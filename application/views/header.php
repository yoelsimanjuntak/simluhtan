
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/themes/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte/plugins/modernizr/modernizr.js"></script>

    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/bootstrap/css/bootstrap.min.css">
    <!-- font Awesome -->
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">

    <!-- Upload file -->
    <link href="<?=base_url()?>assets/css/uploadfile.css" rel="stylesheet" type="text/css" />

    <link href="<?=base_url()?>assets/css/my.css" rel="stylesheet" type="text/css" />
    <!--<link href="--><?//=base_url()?><!--assets/tbs/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />-->

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/plugins/iCheck/all.css">

    <!-- Select 2 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/plugins/select2/select2.min.css">

    <!-- Bootstrap select -->
    <!--<link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap-select.css">-->

    <!-- datatable css -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">

    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/ColReorderWithResize.js"></script>

    <!-- datatable buttons ext + resp + print -->
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <link href="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.html5.min.js"></script>

    <!-- WYSIHTML5 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <!-- daterange picker -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/plugins/daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/plugins/datepicker/datepicker3.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
    <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>
</head>
<!-- Preloader Style -->
<style>
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(<?=base_url()?>assets/preloader/images/<?=$this->setting_web_preloader?>) center no-repeat #fff;
    }
    .navbar-nav b {
        color: #ffff00;
    }
    .main-header .sidebar-toggle:before {
        font-family: 'fontAwesome';
        margin-right: 2px;
    }

    @media (max-width: 767px) {
        .sidebar-toggle {
            font-size: 3vw !important;
        }
    }

</style>
<!-- /.preloader style -->

<!-- Preloader Script -->
<script>
    // Wait for window load
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    });
</script>
<!-- /.preloader script -->

<body class="<?=$this->setting_web_skin_class?> fixed layout-boxed sidebar-mini sidebar-collapse">
<!-- preloader -->
<div class="se-pre-con"></div>
<!-- /.preloader -->

<div class="wrapper">
    <?php
    $ruser = GetLoggedUser();
    $displayname = $ruser ? $ruser[COL_NAME] : "Guest";
    $displaypicture = MY_IMAGEURL.'user.jpg';
    if($ruser) {
        $displaypicture = $ruser[COL_IMAGEFILENAME] ? MY_UPLOADURL.$ruser[COL_IMAGEFILENAME] : MY_IMAGEURL.'user.jpg';
    }
    ?>
    <header class="main-header">

        <!-- Logo -->
        <a href="<?=site_url()?>" class="logo">
            <span class="logo-mini">
                <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" style="width: 36px" alt="Logo">
            </span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg" style="font-size: 12pt;">
                <?=$this->setting_web_name?>
            </span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" style="font-family: inherit !important;">
                <?=$this->setting_web_desc?> <sup>ver <?=$this->setting_web_version?></sup>
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!--<div class="navbar-custom-menu" style="float: none;">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="<?=site_url()?>" class="dropdown-toggle" style="font-size: 14pt">
                            <?=$this->setting_web_desc?>
                            <sup>ver <?=$this->setting_web_version?></sup>
                        </a>
                    </li>
                </ul>
            </div>-->
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?=$displaypicture?>" class="user-image" alt="Your Profile Image">
                            <span class="hidden-xs"><?=$displayname?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?=$displaypicture?>" class="img-circle" alt="User Image">

                                <p>
                                    <?=$displayname?>
                                    <small>Terdaftar sejak <?=date('M Y', strtotime(($ruser[COL_REGISTEREDDATE])))?></small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <!--<div class="pull-left">
                                    <a href="<?= site_url("user/profile") ?>" class="btn btn-info"><i class="fa fa-gear"></i> Profil</a>
                                </div>-->
                                <div class="pull-right">
                                    <a href="<?= site_url("user/logout") ?>" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Keluar</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?=$displaypicture?>" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p><?=$displayname?></p>
                    <a href="#"><?=date("d M Y")?></a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MENU UTAMA</li>
                <li class="treeview">
                    <a href="<?=site_url('user/dashboard')?>">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>

                <?php
                if($ruser[COL_ROLEID] == ROLEADMIN) {
                    ?>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-anchor"></i> <span>Master Data</span>
                                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=site_url('master/kecamatan-index')?>"><i class="fa fa-circle-o"></i> Kecamatan</a></li>
                            <li><a href="<?=site_url('master/kelurahan-index')?>"><i class="fa fa-circle-o"></i> Kelurahan</a></li>
                            <li><a href="<?=site_url('master/kegiatan-index')?>"><i class="fa fa-circle-o"></i> Jenis Bantuan</a></li>
                            <li><a href="<?=site_url('master/komoditas-index')?>"><i class="fa fa-circle-o"></i> Komoditas</a></li>
                            <li><a href="<?=site_url('master/pupuk-index')?>"><i class="fa fa-circle-o"></i> Pupuk</a></li>
                            <li><a href="<?=site_url('master/pestisida-index')?>"><i class="fa fa-circle-o"></i> Pestisida</a></li>
                        </ul>
                    </li>
                <?php
                }
                ?>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-male"></i> <span>Penyuluh</span>
                                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <?php
                        if($ruser[COL_ROLEID] == ROLEADMIN) {
                            ?>
                            <li><a href="<?=site_url('master/penyuluh-lapangan-index')?>"><i class="fa fa-circle-o"></i> Penyuluh Lapangan</a></li>
                        <?php
                        }
                        ?>
                        <?php
                        if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEPPL) {
                            ?>
                            <li><a href="<?=site_url('master/penyuluh-swadaya-index')?>"><i class="fa fa-circle-o"></i> Penyuluh Swadaya</a></li>
                        <?php
                        }
                        ?>
                        <li><a href="<?=site_url('kegiatan/index')?>"><i class="fa fa-circle-o"></i> Kegiatan</a></li>
                        <?php
                        if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEPPS) {
                            ?>
                            <li><a href="<?=site_url('kinerja/index')?>"><i class="fa fa-circle-o"></i> Kinerja</a></li>
                        <?php
                        }
                        ?>
                    </ul>
                </li>

                <?php
                if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEPPL || $ruser[COL_ROLEID] == ROLEPPS) {
                    ?>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-tree"></i> <span>Kelompok Tani</span>
                                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=site_url('master/kelompok-tani-index')?>"><i class="fa fa-circle-o"></i> Kelompok Tani</a></li>
                            <li><a href="<?=site_url('master/kelompok-tani-anggota')?>"><i class="fa fa-circle-o"></i> Anggota</a></li>
                            <li><a href="<?=site_url('kelompok-tani/bantuan-index')?>"><i class="fa fa-circle-o"></i> Bantuan</a></li>
                        </ul>
                    </li>
                    <?php
                }
                ?>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-book"></i> <span>Laporan</span>
                                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?=site_url('report/poktan')?>"><i class="fa fa-circle-o"></i> Kelompok Tani</a></li>
                        <li><a href="<?=site_url('report/poktan-anggota')?>"><i class="fa fa-circle-o"></i> Anggota Kelompok Tani</a></li>
                        <li><a href="<?=site_url('report/poktan-komoditas')?>"><i class="fa fa-circle-o"></i> Komoditas</a></li>
                        <li><a href="<?=site_url('report/bantuan')?>"><i class="fa fa-circle-o"></i> Bantuan</a></li>
                        <li><a href="<?=site_url('report/kegiatan')?>"><i class="fa fa-circle-o"></i> Kegiatan</a></li>
                    </ul>
                </li>

                <?php
                if($ruser[COL_ROLEID] == ROLEADMIN) {
                    ?>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-users"></i> <span>Pengguna</span>
                                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=site_url('user/index')?>"><i class="fa fa-circle-o"></i> Data</a></li>
                            <li><a href="<?=site_url('user/add')?>"><i class="fa fa-circle-o"></i> Tambah Pengguna</a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-newspaper-o"></i> <span>Post</span>
                                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=site_url('post/index')?>"><i class="fa fa-circle-o"></i> Data</a></li>
                            <li><a href="<?=site_url('post/add')?>"><i class="fa fa-circle-o"></i> Tambah Post</a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-cogs"></i> <span>Setting</span>
                                <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?=site_url('setting/common')?>"><i class="fa fa-circle-o"></i> Umum</a></li>
                            <li><a href="<?=site_url('setting/web')?>"><i class="fa fa-circle-o"></i> Konfigurasi</a></li>
                        </ul>
                    </li>
                <?php
                }
                ?>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-user"></i> <span>Akun</span>
                            <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <!--<li><a href="<?=site_url('user/profile')?>"><i class="fa fa-circle-o"></i> Profil</a></li>-->
                        <li><a href="<?=site_url('user/changepassword')?>"><i class="fa fa-circle-o"></i> Ubah Password</a></li>
                    </ul>
                </li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">