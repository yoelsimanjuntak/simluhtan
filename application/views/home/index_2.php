<?php $this->load->view('frontend/header') ?>
<div class="ibox">
    <div class="ibox-title">
        <h5 class="text-navy">Website Resmi Badan Kesatuan Bangsa Politik dan Perlindungan Masyarakat</h5>
    </div>
    <div class="ibox-content no-padding">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="<?=base_url()?>assets/themes/inspinia/img/p_big1.jpg" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">
                        <p>This is simple caption 1</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="<?=base_url()?>assets/themes/inspinia/img/p_big3.jpg" alt="Second slide">
                    <div class="carousel-caption d-none d-md-block">
                        <p>This is simple caption 2</p>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="<?=base_url()?>assets/themes/inspinia/img/p_big2.jpg" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                        <p>This is simple caption 3</p>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>

<div class="col-md-12 m-b-lg text-center">
    <div class="navy-line"></div>
    <h3>Berita Terkini</h3>

    <div class="slick-news">
        <?php
        if(count($news) > 0) {
            foreach($news as $n) {
                ?>
                <div>
                    <div class="ibox-content">
                        <h2><?=$n[COL_POSTTITLE]?></h2>
                        <?php
                        $strippedcontent = strip_tags($n[COL_POSTCONTENT]);
                        ?>
                        <p style="text-align: justify">
                            <?=strlen($strippedcontent) > 200 ? substr($strippedcontent, 0, 200) . "..." : $strippedcontent ?>
                        </p>
                        <a href="<?=site_url('post/view/'.$n[COL_POSTSLUG])?>" class="btn btn-outline btn-primary btn-flat btn-block">Lihat selengkapnya <i class="fa fa-arrow-right"></i> </a>
                    </div>
                </div>
            <?php
            }
        }
        else {
            ?>
            <div>
                <div class="ibox-content">
                    <h2>Tidak Ada Data</h2>
                    <p class="font-italic">
                        Tidak ada data untuk ditampilkan.
                    </p>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
</div>

<div class="col-md-12 m-b-lg text-center">
    <div class="navy-line"></div>
    <h3>Galeri</h3>

    <div class="ibox">
        <div class="ibox-content">
            <div class="lightBoxGallery">
                <?php
                if(count($gallery) > 0) {
                    foreach($gallery as $n) {
                        ?>
                        <a href="<?=MY_UPLOADURL.$n[COL_FILENAME]?>" title="<?=$n[COL_POSTTITLE]?>" data-gallery=""><img src="<?=MY_UPLOADURL.$n[COL_FILENAME]?>" height="100px"></a>
                    <?php
                    }
                }
                else {
                    ?>
                    <p class="font-italic">
                        Tidak ada data untuk ditampilkan.
                    </p>
                <?php
                }
                ?>
                <div id="blueimp-gallery" class="blueimp-gallery">
                    <div class="slides"></div>
                    <h3 class="title"></h3>
                    <a class="prev">‹</a>
                    <a class="next">›</a>
                    <a class="close">×</a>
                    <a class="play-pause"></a>
                    <ol class="indicator"></ol>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('frontend/footer') ?>