<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 9/22/2019
 * Time: 6:12 PM
 */
$this->load->view('header');
$ruser = GetLoggedUser();
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('kegiatan/index')?>"> <?=$title?></a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
            <input type="hidden" name="<?=COL_KD_KEGIATAN?>" value="<?=$edit?$data[COL_KD_KEGIATAN]:''?>" />
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?php if(validation_errors()){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <ul>
                                    <?= validation_errors() ?>
                                </ul>

                            </div>
                        <?php } ?>

                        <?php if(!empty($errormess)){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <?= $errormess ?>
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('success')){ ?>
                            <div class="form-group alert alert-success alert-dismissible">
                                <i class="fa fa-check"></i>
                                Berhasil.
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('error')){ ?>
                            <div class="form-group alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                Gagal mengupdate data, silahkan coba kembali
                            </div>
                        <?php } ?>
                        <div class="col-sm-6">
                            <?php
                            if($ruser[COL_ROLEID] != ROLEPPL && $ruser[COL_ROLEID] != ROLEPPS) {
                                ?>
                                <div class="form-group">
                                    <label  class="control-label col-sm-4">PPL</label>
                                    <div class="col-sm-8">
                                        <select name="<?=COL_KD_PPL?>" class="form-control" <?=$ruser[COL_ROLEID]==ROLEPPL?'disabled':''?>>
                                            <option value="">-- Pilih --</option>
                                            <?=GetCombobox("SELECT * FROM mppl ORDER BY Nm_PPL", COL_KD_PPL, COL_NM_PPL, !empty($data[COL_KD_PPL]) ? $data[COL_KD_PPL] : null)?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label  class="control-label col-sm-4">PPS</label>
                                    <div class="col-sm-8">
                                        <select name="<?=COL_KD_PPS?>" class="form-control" <?=$ruser[COL_ROLEID]==ROLEPPL?'disabled':''?>>
                                            <option value="">-- Pilih --</option>
                                            <?=GetCombobox("SELECT * FROM mpps ORDER BY Nm_PPS", COL_KD_PPS, COL_NM_PPS, !empty($data[COL_KD_PPS]) ? $data[COL_KD_PPS] : null)?>
                                        </select>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>

                            <div class="form-group">
                                <label  class="control-label col-sm-4">Kegiatan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_KEGIATAN?>" value="<?=!empty($data[COL_NM_KEGIATAN]) ? $data[COL_NM_KEGIATAN] : ''?>" required <?=$ruser[COL_ROLEID]==ROLEPPL?'disabled':''?> />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Lokasi</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_LOKASI?>" value="<?=!empty($data[COL_NM_LOKASI]) ? $data[COL_NM_LOKASI] : ''?>" required <?=$ruser[COL_ROLEID]==ROLEPPL?'disabled':''?> />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Tanggal</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control datepicker" name="<?=COL_NM_TANGGAL?>" value="<?=!empty($data[COL_NM_TANGGAL]) ? date('Y-m-d', strtotime($data[COL_NM_TANGGAL])) : ''?>" required <?=$ruser[COL_ROLEID]==ROLEPPL?'disabled':''?> />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Lampiran</label>
                                <div class="col-sm-8">
                                    <?php
                                    if(!empty($data) && !empty($data[COL_NM_FILE])) {
                                        if(file_exists(MY_UPLOADPATH.$data[COL_NM_FILE])) {
                                            ?>
                                            <object id="preview-data" data="<?=MY_UPLOADURL.$data[COL_NM_FILE]?>" type="<?=mime_content_type(MY_UPLOADPATH.$data[COL_NM_FILE])?>" width="100%" height="400">
                                            </object>
                                            <?php
                                        }
                                        ?>
                                    <?php
                                    }
                                    ?>
                                    <input name="userfile" type="file" <?=$ruser[COL_ROLEID]==ROLEPPL?'disabled':''?> />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Permasalahan</label>
                                <div class="col-sm-8">
                                    <textarea rows="3" class="form-control" name="<?=COL_NM_PERMASALAHAN?>" <?=$ruser[COL_ROLEID]==ROLEPPL?'disabled':''?>><?=!empty($data[COL_NM_PERMASALAHAN]) ? $data[COL_NM_PERMASALAHAN] : ''?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Solusi</label>
                                <div class="col-sm-8">
                                    <textarea rows="3" class="form-control" name="<?=COL_NM_SOLUSI?>" <?=$ruser[COL_ROLEID]==ROLEPPL?'disabled':''?>><?=!empty($data[COL_NM_SOLUSI]) ? $data[COL_NM_SOLUSI] : ''?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-12" style="text-align: right">
                            <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                            <a href="<?=site_url('kegiatan/index')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                        </div>
                    </div>
                </div>
                <?=form_close()?>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
<script>
    $(document).ready(function() {
        $("[name=Kd_PPL],[name=Kd_PPS]").change(function() {
            if($("[name=Kd_PPL]").val() != "") {
                $("[name=Kd_PPS]").val("");
            } else if($("[name=Kd_PPS]").val() != "") {
                $("[name=Kd_PPL]").val("");
            }
        }).trigger("change");
    });
</script>
<?php $this->load->view('footer') ?>