<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/19/2019
 * Time: 10:26 AM
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>

    <script src="<?=base_url()?>assets/themes/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte/plugins/modernizr/modernizr.js"></script>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?=base_url()?>assets/css/my.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>
    <link href="<?=base_url()?>assets/themes/inspinia/css/plugins/blueimp/css/blueimp-gallery.min.css" rel="stylesheet">

    <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">

    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/ColReorderWithResize.js"></script>
</head>
<style>
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(<?=base_url()?>assets/preloader/images/<?=$this->setting_web_preloader?>) center no-repeat #fff;
    }
</style>
<script>
    // Wait for window load
    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");
    });
</script>
<body class="<?=$this->setting_web_skin_class?> layout-top-nav">
<div class="se-pre-con"></div>
<div class="wrapper">
    <header class="main-header">
        <nav class="navbar navbar-static-top" style="min-height: 10px !important; background-color: greenyellow">
            <div class="container">
                <div class="pull-right">
                    <ul class="nav navbar-nav">
                        <?php
                        if(!IsLogin()) {
                            ?>
                            <li><a href="<?=site_url('user/login')?>" style="padding: 2px 10px; color: #000"><i class="fa fa-sign-in"></i>&nbsp;&nbsp;Login</a></li>
                        <?php
                        } else {
                            $ruser_ = GetLoggedUser();
                            ?>
                            <li><a href="<?=site_url('user/dashboard')?>" style="padding: 2px 10px; color: #000"><i class="fa fa-dashboard"></i>&nbsp;&nbsp;Dashboard</a></li>
                            <li><a href="<?=site_url('user/logout')?>" style="padding: 2px 10px; color: #000"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;Logout (<?=strtoupper($ruser_[COL_NAME])?>)</a></li>
                        <?php
                        }
                        ?>

                    </ul>
                </div>

            </div>
        </nav>
        <nav class="navbar navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <a href="<?=site_url()?>" class="navbar-brand" style="padding: 10px 15px;">
                        <img class="img-circle" src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" style="display: inline !important; width: 32px" />&nbsp;&nbsp;
                        <span>
                           <b><?=$this->setting_web_name?></b> <small style="font-style: italic"><?=$this->setting_web_desc?></small>
                        </span>
                    </a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse pull-right" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="<?=site_url()?>">Beranda</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tautan <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?=site_url('post/archive')?>">Berita</a></li>
                                <li><a href="<?=site_url('post/gallery')?>">Galeri</a></li>
                            </ul>
                        </li>
                        <li><a href="<?=site_url('post/view/hubungi-kami')?>">Hubungi Kami</a></li>
                    </ul>
                    <form class="navbar-form navbar-left no-padding" role="search">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" id="navbar-search-input" placeholder="Kata Kunci">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-default btn-flat"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.navbar-collapse -->

            </div>
            <!-- /.container-fluid -->
        </nav>
    </header>
    <!-- Full Width Column -->
    <div class="content-wrapper">
        <div class="container">
            <section class="content">
