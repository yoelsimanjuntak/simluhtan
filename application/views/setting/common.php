<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 20/07/2019
 * Time: 09:21
 */
$this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?=$title?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?php if(validation_errors()){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <ul>
                                    <?= validation_errors() ?>
                                </ul>

                            </div>
                        <?php } ?>

                        <?php if(!empty($errormess)){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <?= $errormess ?>
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('success')){ ?>
                            <div class="form-group alert alert-success alert-dismissible">
                                <i class="fa fa-check"></i>
                                Berhasil.
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('error')){ ?>
                            <div class="form-group alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                Gagal mengupdate data, silahkan coba kembali
                            </div>
                        <?php } ?>

                        <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label  class="control-label col-sm-3">Nama Organisasi</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="<?=SETTING_ORG_NAME?>" value="<?=!empty($data[SETTING_ORG_NAME]) ? $data[SETTING_ORG_NAME] : $this->setting_org_name?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-3">Alamat</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" name="<?=SETTING_ORG_ADDRESS?>" rows="5" required><?=!empty($data[SETTING_ORG_ADDRESS]) ? $data[SETTING_ORG_ADDRESS] : $this->setting_org_address?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-3">Telepon</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" name="<?=SETTING_ORG_PHONE?>" value="<?=!empty($data[SETTING_ORG_PHONE]) ? $data[SETTING_ORG_PHONE] : $this->setting_org_phone?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-3">Fax</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" name="<?=SETTING_ORG_FAX?>" value="<?=!empty($data[SETTING_ORG_FAX]) ? $data[SETTING_ORG_FAX] : $this->setting_org_fax?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-3">Email</label>
                                <div class="col-sm-3">
                                    <input type="text" class="form-control" name="<?=SETTING_ORG_MAIL?>" value="<?=!empty($data[SETTING_ORG_MAIL]) ? $data[SETTING_ORG_MAIL] : $this->setting_org_mail?>" required />
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr />
                        <div class="col-sm-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-flat pull-right">Simpan</button>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
<?php $this->load->view('footer') ?>