<?php
$this->load->view('header');
$ruser = GetLoggedUser();
?>
<!-- Content Header (Page header) -->
<style>
    /* Set the size of the div element that contains the map */
    #map {
        height: 64vh;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
    }
    #map td {
        padding: 2px;
    }
    .info-box {
        min-height: 70px !important;
    }
    .info-box-icon {
        height: 70px !important;
        line-height: 70px !important;
    }
    .info-box-icon-right {
        border-top-right-radius: 2px;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 2px;
        display: block;
        float: right;
        height: 70px !important;
        width: 24px;
        text-align: center;
        font-size: 12px;
        line-height: 70px;
        background: rgba(0,0,0,0.1);
    }
    .info-box-icon-right:hover {
        background: rgba(0,0,0,0.15);
    }
    .info-box-icon-right>.small-box-footer {
        color: rgba(255,255,255,0.8);
    }
    .info-box-icon-right>.small-box-footer:hover {
        color: #fff;
    }
    .box-widget .icon {
        -webkit-transition: all .3s linear;
        -o-transition: all .3s linear;
        transition: all .3s linear;
        position: absolute;
        top: -10px;
        right: 10px;
        z-index: 0;
        font-size: 90px;
        color: rgba(0,0,0,0.15);
    }
    .widget-user-header:hover .icon {
        font-size: 95px;
    }
</style>
<section class="content-header" style="box-shadow: 1px 1px 2px #dd4b39">
    <h1>
        Dashboard <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<section class="content">
    <?php
    if($ruser[COL_ROLEID] == ROLEADMIN) {
        ?>
        <div class="col-sm-3">
            <div class="info-box bg-purple">
                <span class="info-box-icon">
                    <i class="fa fa-map-signs"></i>
                </span>
                <span class="info-box-icon-right">
                    <a href="<?=site_url('master/kecamatan-index')?>" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Kecamatan</span>
                    <span class="info-box-number"><?=number_format($count_kecamatan, 0)?></span>
                </div>
            </div>
            <div class="info-box bg-blue">
                <span class="info-box-icon">
                    <i class="fa fa-map-marker"></i>
                </span>
                <span class="info-box-icon-right">
                    <a href="<?=site_url('master/kelurahan-index')?>" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Kelurahan</span>
                    <span class="info-box-number"><?=number_format($count_kelurahan, 0)?></span>
                </div>
            </div>
            <div class="info-box bg-teal">
                <span class="info-box-icon">
                    <i class="fa fa-users"></i>
                </span>
                <span class="info-box-icon-right">
                    <a href="<?=site_url('master/penyuluh-lapangan-index')?>" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Penyuluh Lapangan</span>
                    <span class="info-box-number"><?=number_format($count_ppl, 0)?></span>
                </div>
            </div>
            <div class="info-box bg-maroon">
                <span class="info-box-icon">
                    <i class="fa fa-users"></i>
                </span>
                <span class="info-box-icon-right">
                    <a href="<?=site_url('master/penyuluh-swadaya-index')?>" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Penyuluh Swadaya</span>
                    <span class="info-box-number"><?=number_format($count_pps, 0)?></span>
                </div>
            </div>
            <div class="info-box bg-orange">
                <span class="info-box-icon">
                    <i class="fa fa-users"></i>
                </span>
                <span class="info-box-icon-right">
                    <a href="<?=site_url('master/kelompok-tani-index')?>" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Kelompok Tani</span>
                    <span class="info-box-number"><?=number_format($count_keltan, 0)?></span>
                </div>
            </div>
        </div>
        <?php
    } else if($ruser[COL_ROLEID] == ROLEPPL) {
        $rwkpp = $this->db
            ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MPPL.".".COL_KD_KECAMATAN,"left")
            ->where(TBL_MPPL.".".COL_KD_PPL, $ruser[COL_COMPANYID])
            ->get(TBL_MPPL)
            ->row_array();
        ?>
        <div class="col-sm-3">
            <div class="info-box bg-purple">
                <span class="info-box-icon">
                    <i class="fa fa-map-signs"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Kecamatan</span>
                    <span class="progress-description"><strong><?=!empty($rwkpp)?$rwkpp[COL_NM_KECAMATAN]:"-"?></strong></span>
                </div>
            </div>
            <div class="info-box bg-blue">
                <span class="info-box-icon">
                    <i class="fa fa-map-marker"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Kelurahan</span>
                    <span class="info-box-number"><?=number_format($count_kelurahan, 0)?></span>
                </div>
            </div>
            <div class="info-box bg-teal">
                <span class="info-box-icon">
                    <i class="fa fa-users"></i>
                </span>
                <span class="info-box-icon-right">
                    <a href="<?=site_url('master/penyuluh-swadaya-index')?>" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Penyuluh Swadaya</span>
                    <span class="info-box-number"><?=number_format($count_pps, 0)?></span>
                </div>
            </div>
            <div class="info-box bg-maroon">
                <span class="info-box-icon">
                    <i class="fa fa-users"></i>
                </span>
                <span class="info-box-icon-right">
                    <a href="<?=site_url('master/kelompok-tani-index')?>" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Kelompok Tani</span>
                    <span class="info-box-number"><?=number_format($count_keltan, 0)?></span>
                </div>
            </div>
            <div class="info-box bg-orange">
                <span class="info-box-icon">
                    <i class="fa fa-clipboard"></i>
                </span>
                <span class="info-box-icon-right">
                    <a href="<?=site_url('kegiatan/index')?>" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Kegiatan TH. <?=date("Y")?></span>
                    <span class="info-box-number"><?=number_format($count_kegiatan, 0)?></span>
                </div>
            </div>
        </div>
        <?php
    } else if($ruser[COL_ROLEID] == ROLEPPS) {
        $rwkpp = $this->db
            ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KD_KELURAHAN." = ".TBL_MPPS.".".COL_KD_KELURAHAN,"left")
            ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MKELURAHAN.".".COL_KD_KECAMATAN,"left")
            ->join(TBL_MPPL,TBL_MPPL.'.'.COL_KD_PPL." = ".TBL_MPPS.".".COL_KD_PPL,"left")
            ->where(TBL_MPPS.".".COL_KD_PPS, $ruser[COL_COMPANYID])
            ->get(TBL_MPPS)
            ->row_array();
        ?>
        <div class="col-sm-3">
            <div class="info-box bg-purple">
                <span class="info-box-icon">
                    <i class="fa fa-map-signs"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Kecamatan</span>
                    <span class="progress-description"><strong><?=!empty($rwkpp)?$rwkpp[COL_NM_KECAMATAN]:"-"?></strong></span>
                </div>
            </div>
            <div class="info-box bg-blue">
                <span class="info-box-icon">
                    <i class="fa fa-map-marker"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Kelurahan</span>
                    <span class="progress-description"><strong><?=!empty($rwkpp)?$rwkpp[COL_NM_KELURAHAN]:"-"?></strong></span>
                </div>
            </div>
            <div class="info-box bg-teal">
                <span class="info-box-icon">
                    <i class="fa fa-user"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Penyuluh Lapangan</span>
                    <span class="progress-description"><strong><?=!empty($rwkpp)?$rwkpp[COL_NM_PPL]:"-"?></strong></span>
                </div>
            </div>
            <div class="info-box bg-maroon">
                <span class="info-box-icon">
                    <i class="fa fa-users"></i>
                </span>
                <span class="info-box-icon-right">
                    <a href="<?=site_url('master/kelompok-tani-index')?>" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Kelompok Tani</span>
                    <span class="info-box-number"><?=number_format($count_keltan, 0)?></span>
                </div>
            </div>
            <div class="info-box bg-orange">
                <span class="info-box-icon">
                    <i class="fa fa-clipboard"></i>
                </span>
                <span class="info-box-icon-right">
                    <a href="<?=site_url('kegiatan/index')?>" class="small-box-footer"><i class="fa fa-arrow-circle-right"></i></a>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Kegiatan TH. <?=date("Y")?></span>
                    <span class="info-box-number"><?=number_format($count_kegiatan, 0)?></span>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
    <div class="col-sm-9">
        <div class="box box-default color-palette-box">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-info"></i>&nbsp;&nbsp;Rekapitulasi</h3>
            </div>
            <div class="box-body">
                <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
                <div class="form-group">
                    <?php
                    $rppl = null;
                    $rpps = null;
                    if($ruser[COL_ROLEID] == ROLEPPL) {
                        $rppl = $this->db->where(COL_KD_PPL, $ruser[COL_COMPANYID])->get(TBL_MPPL)->row_array();
                    }
                    else if($ruser[COL_ROLEID] == ROLEPPS) {
                        $rpps = $this->db->where(COL_KD_PPS, $ruser[COL_COMPANYID])->get(TBL_MPPS)->row_array();
                        if(!empty($rpps)) {
                            $rppl = $this->db->where(COL_KD_PPL, $rpps[COL_KD_PPL])->get(TBL_MPPL)->row_array();
                        }
                    }
                    ?>
                    <label  class="control-label col-sm-2">Filter</label>
                    <div class="col-sm-3">
                        <select name="<?=COL_KD_KECAMATAN?>" class="form-control" <?=!empty($rppl) ? "disabled" : ""?>>
                            <option value="">-- Semua Kecamatan --</option>
                            <?=GetCombobox("SELECT * FROM mkecamatan ORDER BY Nm_Kecamatan", COL_KD_KECAMATAN, COL_NM_KECAMATAN, (!empty($rppl) ? $rppl[COL_KD_KECAMATAN] : null))?>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="<?=COL_KD_KELURAHAN?>" class="form-control" <?=!empty($rpps) ? "disabled" : ""?>>
                            <option value="">-- Semua Kelurahan --</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="<?=COL_KD_KELOMPOKTANI?>" class="form-control">
                            <option value="">-- Semua Poktan --</option>
                        </select>
                    </div>

                    <!--<div class="col-sm-2">
                        <button type="submit" class="btn btn-success btn-flat btn-block"><i class="fa fa-filter"></i></button>
                    </div>-->
                </div>
                <?=form_close()?>
            </div>
            <div class="overlay" style="display: none">
                <i class="fa fa-spinner fa-spin"></i>
            </div>
            <!--<div class="box-footer">
                <div id="summary">
                    <div class="col-sm-3">
                        <div class="callout callout-warning">
                            <h4><i class="icon fa fa-warning"></i> Alert!</h4>
                            Warning alert preview. This alert is dismissable.
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
        <!--<div id="main-result">
            <div class="preloader" style="text-align: center">
                <span style="font-size: 36pt; color: rgba(0,0,0,0.2)"><i class="fa fa-spin fa-spinner"></i></span>
            </div>
        </div>-->
        <div class="col-sm-6" style="padding-left: 0px !important;">
            <div class="box box-widget widget-user box-dashboard-1">
                <div class="overlay" style="display: none;">
                    <i class="fa fa-spinner fa-spin"></i>
                </div>
            </div>
        </div>
        <div class="col-sm-6" style="padding-right: 0px !important;">
            <div class="box box-widget widget-user box-dashboard-2">
                <div class="overlay" style="display: none;">
                    <i class="fa fa-spinner fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

</section>
<?php $this->load->view('loadjs')?>
<script>

    $(document).ready(function() {
        $("#main-form").validate({
            submitHandler : function(form) {
                var kdKecamatan = $("[name=Kd_Kecamatan]", form).val();
                var kdKelurahan = $("[name=Kd_Kelurahan]", form).val();
                var kdKeltan = $("[name=Kd_KelompokTani]", form).val();

                $(".overlay", form.closest(".box")).show();
                $(".overlay", $(".box-dashboard-1")).show();
                $(".overlay", $(".box-dashboard-2")).show();
                $(".box-dashboard-1").load("<?=site_url("ajax/get-dashboard-partial")?>", {Kd_Kec: kdKecamatan, Kd_Kel: kdKelurahan, Kd_Keltan: kdKeltan, opt: 1}, function () {
                    (".overlay", $(".box-dashboard-1")).show();
                    $(".box-dashboard-2").load("<?=site_url("ajax/get-dashboard-partial")?>", {Kd_Kec: kdKecamatan, Kd_Kel: kdKelurahan, Kd_Keltan: kdKeltan, opt: 2}, function () {
                        (".overlay", $(".box-dashboard-2")).show();
                        $(".overlay", form.closest(".box")).hide();
                    });
                });
                return false;
            }
        });
        $("button[type=submit]", $("#main-form")).click();

        $("[name=Kd_Kecamatan],[name=Kd_Kelurahan],[name=Kd_KelompokTani]").change(function() {
            $("#main-form").submit();
        });

        $("[name=Kd_Kecamatan]").change(function() {
            if($(this).val()) {
                $("[name^=Kd_Kelurahan]").html('<option value="">Loading...</option>');
                $("[name^=Kd_Kelurahan]").load("<?=site_url("ajax/get-opt-kelurahan")?>", {Kd_Kec: $(this).val()}, function () {
                    $("[name^=Kd_Kelurahan]").prepend('<option value="">-- Semua Kelurahan --</option>');
                    $("[name^=Kd_Kelurahan]").val("");
                    $("[name^=Kd_Kelurahan]").select2({ width: 'resolve' });

                    if('<?=!empty($rpps)?$rpps[COL_KD_KELURAHAN]:''?>' != '') {
                        $("[name^=Kd_Kelurahan]").val(<?=$rpps[COL_KD_KELURAHAN]?>).trigger("change");
                    }
                });
            }
        }).trigger("change");

        $("[name=Kd_Kelurahan]").change(function() {
            if($(this).val()) {
                $("[name^=Kd_KelompokTani]").html('<option value="">Loading...</option>');
                $("[name^=Kd_KelompokTani]").load("<?=site_url("ajax/get-opt-keltan")?>", {Kd_Kel: $(this).val()}, function () {
                    $("[name^=Kd_KelompokTani]").prepend('<option value="">-- Semua Poktan --</option>');
                    $("[name^=Kd_KelompokTani]").val("");
                    $("[name^=Kd_KelompokTani]").select2({ width: 'resolve' });
                });
            }
        }).trigger("change");
    });

//    //-------------
//    //- PIE CHART -
//    //-------------
//    // Get context with jQuery - using jQuery's .get() method.
//    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
//    var pieChart = new Chart(pieChartCanvas);
//    var PieData = <?//=json_encode($posts)?>//;
//    var pieOptions = {
//        //Boolean - Whether we should show a stroke on each segment
//        segmentShowStroke: true,
//        //String - The colour of each segment stroke
//        segmentStrokeColor: "#fff",
//        //Number - The width of each segment stroke
//        segmentStrokeWidth: 1,
//        //Number - The percentage of the chart that we cut out of the middle
//        percentageInnerCutout: 50, // This is 0 for Pie charts
//        //Number - Amount of animation steps
//        animationSteps: 100,
//        //String - Animation easing effect
//        animationEasing: "easeOutBounce",
//        //Boolean - Whether we animate the rotation of the Doughnut
//        animateRotate: true,
//        //Boolean - Whether we animate scaling the Doughnut from the centre
//        animateScale: false,
//        //Boolean - whether to make the chart responsive to window resizing
//        responsive: true,
//        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
//        maintainAspectRatio: false,
//        //String - A legend template
//        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
//        //String - A tooltip template
//        tooltipTemplate: "<%=value %> <%=label%> users"
//    };
//    //Create pie or douhnut chart
//    // You can switch between pie and douhnut using the method below.
//    pieChart.Doughnut(PieData, pieOptions);
//    //-----------------
//    //- END PIE CHART -
//    //-----------------
</script>
<?php $this->load->view('footer') ?>
