<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 21/07/2019
 * Time: 04:48
 */
$this->load->view('header');
$ruser = GetLoggedUser();
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('master/kelompok-tani-anggota')?>"> <?=$title?></a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
            <input type="hidden" name="<?=COL_KD_ANGGOTA?>" value="<?=$edit?$data[COL_KD_ANGGOTA]:''?>" />
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?php if(validation_errors()){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <ul>
                                    <?= validation_errors() ?>
                                </ul>

                            </div>
                        <?php } ?>

                        <?php if(!empty($errormess)){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <?= $errormess ?>
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('success')){ ?>
                            <div class="form-group alert alert-success alert-dismissible">
                                <i class="fa fa-check"></i>
                                Berhasil.
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('error')){ ?>
                            <div class="form-group alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                Gagal mengupdate data, silahkan coba kembali
                            </div>
                        <?php } ?>
                        <div class="col-sm-7">
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Kelompok Tani</label>
                                <div class="col-sm-8">
                                    <?php
                                    $arrkel = array();
                                    if($ruser[COL_ROLEID] == ROLEPPL) {
                                        $rkel = $this->db->where(COL_KD_PPL, $ruser[COL_COMPANYID])->get(TBL_MPPL_KELURAHAN)->result_array();

                                        foreach($rkel as $k) {
                                            $arrkel[] = $k[COL_KD_KELURAHAN];
                                        }
                                    }
                                    if($ruser[COL_ROLEID] == ROLEPPS) {
                                        $rpps = $this->db->where(COL_KD_PPS, $ruser[COL_COMPANYID])->get(TBL_MPPS)->row_array();
                                        if(!empty($rpps)) {
                                            $arrkel[] = $rpps[COL_KD_KELURAHAN];
                                        }
                                    }
                                    ?>
                                    <select name="<?=COL_KD_KELOMPOKTANI?>" class="form-control" required>
                                        <?=GetCombobox("SELECT * FROM mkeltan ".(count($arrkel) > 0 ? "where Kd_Kelurahan in (".join(",", $arrkel).")" : "")." ORDER BY Nm_KelompokTani", COL_KD_KELOMPOKTANI  , COL_NM_KELOMPOKTANI, (!empty($data[COL_KD_KELOMPOKTANI]) ? $data[COL_KD_KELOMPOKTANI] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Nama</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_ANGGOTA?>" value="<?=!empty($data[COL_NM_ANGGOTA]) ? $data[COL_NM_ANGGOTA] : ''?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">NIK</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_ANGGOTA_NIK?>" value="<?=!empty($data[COL_NM_ANGGOTA_NIK]) ? $data[COL_NM_ANGGOTA_NIK] : ''?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Alamat</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_ALAMAT?>" value="<?=!empty($data[COL_NM_ALAMAT]) ? $data[COL_NM_ALAMAT] : ''?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">No. Telepon</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_NOTELEPON?>" value="<?=!empty($data[COL_NM_NOTELEPON]) ? $data[COL_NM_NOTELEPON] : ''?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Email</label>
                                <div class="col-sm-8">
                                    <input type="email" class="form-control" name="<?=COL_EMAIL?>" value="<?=!empty($data[COL_EMAIL]) ? $data[COL_EMAIL] : ''?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Tgl. Lahir</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control datepicker" name="<?=COL_NM_TANGGALLAHIR?>" value="<?=!empty($data[COL_NM_TANGGALLAHIR]) ? $data[COL_NM_TANGGALLAHIR] : ''?>" placeholder="Tanggal Lahir" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Status</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_NM_STATUSANGGOTA?>" class="form-control" required>
                                        <option value="Sekretaris" <?=!empty($data[COL_NM_STATUSANGGOTA]) && $data[COL_NM_STATUSANGGOTA] == "Sekretaris" ? "selected" : ""?>>Sekretaris</option>
                                        <option value="Bendahara" <?=!empty($data[COL_NM_STATUSANGGOTA]) && $data[COL_NM_STATUSANGGOTA] == "Bendahara" ? "selected" : ""?>>Bendahara</option>
                                        <option value="Anggota" <?=!empty($data[COL_NM_STATUSANGGOTA]) && $data[COL_NM_STATUSANGGOTA] == "Anggota" ? "selected" : ""?>>Anggota</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="box box-default">
                    <div class="box-header">
                        <h4 class="box-title">Komoditas</h4>
                    </div>
                    <div class="box-body">
                        <input type="hidden" name="DataPupuk" />
                        <input type="hidden" name="DataPestisida" />
                        <table class="table table-bordered" id="tbl-det">
                            <thead>
                            <tr>
                                <th>Komoditas</th>
                                <th width="100px">Volume</th>
                                <th width="100px">Musim Tanam</th>
                                <th width="100px">Sumber Benih</th>
                                <th>Pupuk / Jlh</th>
                                <th>Pestisida</th>
                                <th style="width: 40px"><button type="button" id="btn-add-komoditas" class="btn btn-default btn-flat"><i class="fa fa-plus"></i></button></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="tr-blueprint-komoditas" style="display: none">
                                <td>
                                    <select name="<?=COL_KD_KOMODITAS?>[]" class="form-control no-select2" disabled style="width: 240px">
                                        <?=GetCombobox("SELECT * FROM mkomoditas ORDER BY Nm_Komoditas", COL_KD_KOMODITAS, array(COL_NM_KOMODITAS, COL_NM_VARIETAS))?>
                                    </select>
                                </td>
                                <td><input type="text" name="<?=COL_VOLUME?>[]" class="form-control money" placeholder="rante / ekor / unit" disabled /></td>
                                <td><input type="text" name="<?=COL_MUSIM_TANAM?>[]" class="form-control money" disabled /></td>
                                <td>
                                    <select name="<?=COL_NM_SUMBERBENIH?>[]" class="form-control no-select2" disabled style="width: 200px">
                                        <option value="Bantuan">Bantuan</option>
                                        <option value="Swadaya">Swadaya</option>
                                        <option value="Lainnya">Lainnya</option>
                                    </select>
                                </td>
                                <td>
                                    <div class="div-pupuk-blueprint" style="display: none; margin-top: 10px">
                                        <div class="col-sm-6">
                                            <select class="form-control no-select2 sel-pupuk" disabled style="width: 200px">
                                                <?=GetCombobox("SELECT * FROM mpupuk ORDER BY Nm_Pupuk", COL_KD_PUPUK, COL_NM_PUPUK)?>
                                            </select>
                                        </div>
                                        <div class="col-sm-6" style="padding-left: 0px">
                                            <div class="input-group">
                                                <input type="text" class="form-control money text-jlhpupuk" placeholder="Kg" disabled />
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-success btn-flat btn-del-pupuk"><i class="fa fa-minus"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="div-pupuk">
                                        <div class="col-sm-6">
                                            <select class="form-control no-select2 sel-pupuk" disabled style="width: 200px">
                                                <?=GetCombobox("SELECT * FROM mpupuk ORDER BY Nm_Pupuk", COL_KD_PUPUK, COL_NM_PUPUK)?>
                                            </select>
                                        </div>
                                        <div class="col-sm-6" style="padding-left: 0px">
                                            <div class="input-group">
                                                <input type="text" class="form-control money text-jlhpupuk" placeholder="Kg" disabled />
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-success btn-flat btn-add-pupuk"><i class="fa fa-plus"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </td>
                                <td>
                                    <div class="div-pestisida-blueprint" style="display: none; margin-top: 10px">
                                        <div class="col-sm-6">
                                            <select class="form-control no-select2 sel-pupuk" disabled style="width: 200px">
                                                <?=GetCombobox("SELECT * FROM mpestisida ORDER BY Nm_Pestisida", COL_KD_PESTISIDA, COL_NM_PESTISIDA)?>
                                            </select>
                                        </div>
                                        <div class="col-sm-6" style="padding-left: 0px">
                                            <div class="input-group">
                                                <input type="text" class="form-control money text-jlhpestisida" placeholder="-" disabled />
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-success btn-flat btn-del-pestisida"><i class="fa fa-minus"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="div-pestisida">
                                        <div class="col-sm-6">
                                            <select class="form-control no-select2 sel-pestisida" disabled style="width: 200px">
                                                <?=GetCombobox("SELECT * FROM mpestisida ORDER BY Nm_Pestisida", COL_KD_PESTISIDA, COL_NM_PESTISIDA)?>
                                            </select>
                                        </div>
                                        <div class="col-sm-6" style="padding-left: 0px">
                                            <div class="input-group">
                                                <input type="text" class="form-control money text-jlhpestisida" placeholder="-" disabled />
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-success btn-flat btn-add-pestisida"><i class="fa fa-plus"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-default btn-flat btn-del-komoditas"><i class="fa fa-minus"></i></button>
                                </td>
                            </tr>
                            <?php
                            $det = $this->db
                                ->where(COL_KD_ANGGOTA, ($edit?$data[COL_KD_ANGGOTA]:-999))
                                ->get(TBL_MKELTAN_KOMODITAS)->result_array();
                            foreach($det as $m) {
                                ?>
                                <tr>
                                    <td>
                                        <select name="<?=COL_KD_KOMODITAS?>[]" class="form-control" style="width: 240px">
                                            <?=GetCombobox("SELECT * FROM mkomoditas ORDER BY Nm_Komoditas", COL_KD_KOMODITAS, array(COL_NM_KOMODITAS, COL_NM_VARIETAS), $m[COL_KD_KOMODITAS])?>
                                        </select>
                                    </td>
                                    <td><input type="text" name="<?=COL_VOLUME?>[]" value="<?=$m[COL_VOLUME]?>" placeholder="rante / ekor / unit" class="form-control money" /></td>
                                    <td><input type="text" name="<?=COL_MUSIM_TANAM?>[]" value="<?=$m[COL_MUSIM_TANAM]?>" class="form-control money" /></td>
                                    <td>
                                        <select name="<?=COL_NM_SUMBERBENIH?>[]" class="form-control" style="width: 200px">
                                            <option value="Bantuan" <?=$m[COL_NM_SUMBERBENIH] == "Bantuan" ? "selected" : ""?>>Bantuan</option>
                                            <option value="Swadaya" <?=$m[COL_NM_SUMBERBENIH] == "Swadaya" ? "selected" : ""?>>Swadaya</option>
                                            <option value="Lainnya" <?=$m[COL_NM_SUMBERBENIH] == "Lainnya" ? "selected" : ""?>>Lainnya</option>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="div-pupuk-blueprint" style="display: none; margin-top: 10px">
                                            <div class="col-sm-6">
                                                <select class="form-control no-select2 sel-pupuk" disabled style="width: 200px">
                                                    <?=GetCombobox("SELECT * FROM mpupuk ORDER BY Nm_Pupuk", COL_KD_PUPUK, COL_NM_PUPUK)?>
                                                </select>
                                            </div>
                                            <div class="col-sm-6" style="padding-left: 0px">
                                                <div class="input-group">
                                                    <input type="text" class="form-control money text-jlhpupuk" placeholder="Kg" disabled />
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-success btn-flat btn-del-pupuk"><i class="fa fa-minus"></i></button>
                                                </span>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <?php
                                        $pupuk = $this->db
                                            ->where(COL_KD_ANGGOTA, $m[COL_KD_ANGGOTA])
                                            ->where(COL_KD_KOMODITAS, $m[COL_KD_KOMODITAS])
                                            ->get(TBL_MKELTAN_PUPUK)->result_array();

                                        if(count($pupuk) > 0) {
                                            for($i=0; $i<count($pupuk); $i++) {
                                                $btnclass = $i==0?"btn-add-pupuk":"btn-del-pupuk";
                                                $btnicon = $i==0?"fa-plus":"fa-minus";
                                                ?>
                                                <div class="div-pupuk" <?=$i==0?"":"style='margin-top: 10px'"?>>
                                                    <div class="col-sm-6">
                                                        <select class="form-control sel-pupuk" style="width: 200px">
                                                            <?=GetCombobox("SELECT * FROM mpupuk ORDER BY Nm_Pupuk", COL_KD_PUPUK, COL_NM_PUPUK, $pupuk[$i][COL_KD_PUPUK])?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6" style="padding-left: 0px">
                                                        <div class="input-group">
                                                            <input type="text" value="<?=$pupuk[$i][COL_JLH_PUPUK]?>" class="form-control money text-jlhpupuk" placeholder="Kg" />
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-success btn-flat <?=$btnclass?>"><i class="fa <?=$btnicon?>"></i></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <div class="div-pupuk">
                                                <div class="col-sm-6">
                                                    <select class="form-control sel-pupuk" style="width: 200px">
                                                        <?=GetCombobox("SELECT * FROM mpupuk ORDER BY Nm_Pupuk", COL_KD_PUPUK, COL_NM_PUPUK)?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6" style="padding-left: 0px">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control money text-jlhpupuk" placeholder="Kg" />
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-success btn-flat btn-add-pupuk"><i class="fa fa-plus"></i></button>
                                                            </span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <div class="div-pestisida-blueprint" style="display: none; margin-top: 10px">
                                            <div class="col-sm-6">
                                                <select class="form-control no-select2 sel-pestisida" disabled style="width: 200px">
                                                    <?=GetCombobox("SELECT * FROM mpestisida ORDER BY Nm_Pestisida", COL_KD_PESTISIDA, COL_NM_PESTISIDA)?>
                                                </select>
                                            </div>
                                            <div class="col-sm-6" style="padding-left: 0px">
                                                <div class="input-group">
                                                    <input type="text" class="form-control money text-jlhpestisida" placeholder="-" disabled />
                                                <span class="input-group-btn">
                                                    <button type="button" class="btn btn-success btn-flat btn-del-pestisida"><i class="fa fa-minus"></i></button>
                                                </span>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <?php
                                        $pestisida = $this->db
                                            ->where(COL_KD_ANGGOTA, $m[COL_KD_ANGGOTA])
                                            ->where(COL_KD_KOMODITAS, $m[COL_KD_KOMODITAS])
                                            ->get(TBL_MKELTAN_PESTISIDA)->result_array();

                                        if(count($pestisida) > 0) {
                                            for($i=0; $i<count($pestisida); $i++) {
                                                $btnclass = $i==0?"btn-add-pestisida":"btn-del-pestisida";
                                                $btnicon = $i==0?"fa-plus":"fa-minus";
                                                ?>
                                                <div class="div-pestisida" <?=$i==0?"":"style='margin-top: 10px'"?>>
                                                    <div class="col-sm-6">
                                                        <select class="form-control sel-pupuk" style="width: 200px">
                                                            <?=GetCombobox("SELECT * FROM mpestisida ORDER BY Nm_Pestisida", COL_KD_PESTISIDA, COL_NM_PESTISIDA, $pestisida[$i][COL_KD_PESTISIDA])?>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-6" style="padding-left: 0px">
                                                        <div class="input-group">
                                                            <input type="text" value="<?=$pestisida[$i][COL_JLH_PESTISIDA]?>" class="form-control money text-jlhpestisida" placeholder="-" />
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-success btn-flat <?=$btnclass?>"><i class="fa <?=$btnicon?>"></i></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            <?php
                                            }
                                        } else {
                                            ?>
                                            <div class="div-pestisida">
                                                <div class="col-sm-6">
                                                    <select class="form-control sel-pestisida" style="width: 200px">
                                                        <?=GetCombobox("SELECT * FROM mpestisida ORDER BY Nm_Pestisida", COL_KD_PESTISIDA, COL_NM_PESTISIDA)?>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6" style="padding-left: 0px">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control money text-jlhpestisida" placeholder="-" />
                                                            <span class="input-group-btn">
                                                                <button type="button" class="btn btn-success btn-flat btn-add-pestisida"><i class="fa fa-plus"></i></button>
                                                            </span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat btn-del-komoditas"><i class="fa fa-minus"></i></button>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>

                            <?php
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                        <a href="<?=site_url('master/kelompok-tani-anggota')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                    </div>
                </div>
                <?=form_close()?>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">

        $("#main-form").validate({
            submitHandler : function(form) {
                var arr_pupuk = [];
                var tr_komoditi = $("#tbl-det").find("tr").not(".tr-blueprint-komoditas");
                for (var i=0; i<tr_komoditi.length; i++) {
                    var el_komoditi = $(tr_komoditi[i]).find("[name^=Kd_Komoditas]");
                    if(el_komoditi) {
                        var div_pupuk = $(tr_komoditi[i]).find(".div-pupuk").not(".div-pupuk-blueprint");
                        for (var n=0; n<div_pupuk.length; n++) {
                            var kdPupuk = $(".sel-pupuk", $(div_pupuk[n])).val();
                            var jlPupuk = $(".text-jlhpupuk", $(div_pupuk[n])).val();
                            if(kdPupuk && jlPupuk) {
                                arr_pupuk.push({Kd_Komoditas: el_komoditi.val(), Kd_Pupuk: kdPupuk, Jlh_Pupuk: jlPupuk});
                            }
                        }
                    }
                }
                $("[name=DataPupuk]", form).val(JSON.stringify(arr_pupuk));

                var arr_pestisida = [];
                var tr_komoditi = $("#tbl-det").find("tr").not(".tr-blueprint-komoditas");
                for (var i=0; i<tr_komoditi.length; i++) {
                    var el_komoditi = $(tr_komoditi[i]).find("[name^=Kd_Komoditas]");
                    if(el_komoditi) {
                        var div_pestisida = $(tr_komoditi[i]).find(".div-pestisida").not(".div-pestisida-blueprint");
                        for (var n=0; n<div_pupuk.length; n++) {
                            var kdPestisida = $(".sel-pestisida", $(div_pestisida[n])).val();
                            var jlPestisida = $(".text-jlhpestisida", $(div_pestisida[n])).val();
                            if(kdPestisida && jlPestisida) {
                                arr_pestisida.push({Kd_Komoditas: el_komoditi.val(), Kd_Pestisida: kdPestisida, Jlh_Pestisida: jlPestisida});
                            }
                        }
                    }
                }
                $("[name=DataPestisida]", form).val(JSON.stringify(arr_pestisida));
                form.submit();
            }
        });


        $(".btn-del-komoditas").click(function () {
            var row = $(this).closest("tr");
            row.remove();
        });

        $("#btn-add-komoditas").click(function () {
            var tbl = $(this).closest("table");
            var blueprint = tbl.find(".tr-blueprint-komoditas").first().clone();

            blueprint.appendTo(tbl).removeClass("tr-blueprint-komoditas").show();
            $("input, select", blueprint).attr('disabled', false);
            $("select", blueprint).not($(".div-pupuk-blueprint, .div-pestisida-blueprint").find('select')).select2({ width: 'resolve' });
            $(".btn-del-komoditas", blueprint).click(function () {
                var row = $(this).closest("tr");
                row.remove();
            });

            $(".btn-add-pupuk", blueprint).click(function () {
                var td = $(this).closest("td");
                var div = td.find(".div-pupuk-blueprint").first().clone();

                div.appendTo(td).removeClass("div-pupuk-blueprint").addClass("div-pupuk").show();
                $("input, select", div).attr('disabled', false);
                $(".btn-add-pupuk", div).addClass('btn-del-pupuk').removeClass("btn-add-pupuk");
                $(".fa-plus", div).addClass('fa-minus').removeClass("fa-plus");
                $("select", div).select2({ width: 'resolve' });
                $(".btn-del-pupuk", div).click(function () {
                    var row = $(this).closest(".div-pupuk");
                    row.remove();
                });
            });

            $(".btn-add-pestisida", blueprint).click(function () {
                var td = $(this).closest("td");
                var div = td.find(".div-pestisida-blueprint").first().clone();

                div.appendTo(td).removeClass("div-pestisida-blueprint").addClass("div-pestisida").show();
                $("input, select", div).attr('disabled', false);
                $(".btn-add-pestisida", div).addClass('btn-del-pestisida').removeClass("btn-add-pestisida");
                $(".fa-plus", div).addClass('fa-minus').removeClass("fa-plus");
                $("select", div).select2({ width: 'resolve' });
                $(".btn-del-pestisida", div).click(function () {
                    var row = $(this).closest(".div-pestisida");
                    row.remove();
                });
            });
        });

        $(".btn-add-pupuk").click(function () {
            var td = $(this).closest("td");
            var div = td.find(".div-pupuk-blueprint").first().clone();

            div.appendTo(td).removeClass("div-pupuk-blueprint").addClass("div-pupuk").show();
            $("input, select", div).attr('disabled', false);
            $(".btn-add-pupuk", div).addClass('btn-del-pupuk').removeClass("btn-add-pupuk");
            $(".fa-plus", div).addClass('fa-minus').removeClass("fa-plus");
            $("select", div).select2({ width: 'resolve' });
            $(".btn-del-pupuk", div).click(function () {
                var row = $(this).closest(".div-pupuk");
                row.remove();
            });
        });

        $(".btn-add-pestisida").click(function () {
            var td = $(this).closest("td");
            var div = td.find(".div-pestisida-blueprint").first().clone();

            div.appendTo(td).removeClass("div-pestisida-blueprint").addClass("div-pestisida").show();
            $("input, select", div).attr('disabled', false);
            $(".btn-add-pestisida", div).addClass('btn-del-pestisida').removeClass("btn-add-pestisida");
            $(".fa-plus", div).addClass('fa-minus').removeClass("fa-plus");
            $("select", div).select2({ width: 'resolve' });
            $(".btn-del-pestisida", div).click(function () {
                var row = $(this).closest(".div-pestisida");
                row.remove();
            });
        });

    </script>
<?php $this->load->view('footer') ?>