<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 21/07/2019
 * Time: 04:03
 */
$this->load->view('header');
$ruser = GetLoggedUser();
?>
    <style>
        /* Set the size of the div element that contains the map */
        #map {
            height: 300px;  /* The height is 400 pixels */
            width: 100%;  /* The width is the width of the web page */
        }
    </style>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('master/kelompok-tani-index')?>"> <?=$title?></a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
            <input type="hidden" name="<?=COL_KD_KELOMPOKTANI?>" value="<?=$edit?$data[COL_KD_KELOMPOKTANI]:''?>" />
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?php if(validation_errors()){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <ul>
                                    <?= validation_errors() ?>
                                </ul>

                            </div>
                        <?php } ?>

                        <?php if(!empty($errormess)){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <?= $errormess ?>
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('success')){ ?>
                            <div class="form-group alert alert-success alert-dismissible">
                                <i class="fa fa-check"></i>
                                Berhasil.
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('error')){ ?>
                            <div class="form-group alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                Gagal mengupdate data, silahkan coba kembali
                            </div>
                        <?php } ?>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Kecamatan</label>
                                <div class="col-sm-8">
                                    <?php
                                    $rppl = null;
                                    $rpps = null;
                                    if($ruser[COL_ROLEID] == ROLEPPL) {
                                        $rppl = $this->db->where(COL_KD_PPL, $ruser[COL_COMPANYID])->get(TBL_MPPL)->row_array();
                                    }
                                    else if($ruser[COL_ROLEID] == ROLEPPS) {
                                        $rpps = $this->db->where(COL_KD_PPS, $ruser[COL_COMPANYID])->get(TBL_MPPS)->row_array();
                                        if(!empty($rpps)) {
                                            $rppl = $this->db->where(COL_KD_PPL, $rpps[COL_KD_PPL])->get(TBL_MPPL)->row_array();
                                        }
                                    }
                                    ?>
                                    <select name="<?=COL_KD_KECAMATAN?>" class="form-control" required <?=!empty($rppl) ? "disabled" : ""?>>
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mkecamatan ORDER BY Nm_Kecamatan", COL_KD_KECAMATAN, COL_NM_KECAMATAN, (!empty($data[COL_KD_KECAMATAN]) ? $data[COL_KD_KECAMATAN] : (!empty($rppl) ? $rppl[COL_KD_KECAMATAN] : null)))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Kelurahan</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_KD_KELURAHAN?>" class="form-control" required <?=!empty($rpps) ? "disabled" : ""?>>
                                        <?=GetCombobox("SELECT * FROM mkelurahan ".($edit?"WHERE Kd_Kecamatan=".$data[COL_KD_KECAMATAN]:(!empty($data[COL_KD_KECAMATAN]) ? $data[COL_KD_KECAMATAN] : (!empty($rppl) ? "WHERE Kd_Kelurahan in (select Kd_Kelurahan from mppl_kelurahan where Kd_PPL = ".$rppl[COL_KD_PPL].")" : "WHERE 1<>1")))." ORDER BY Nm_Kelurahan", COL_KD_KELURAHAN, COL_NM_KELURAHAN, (!empty($data[COL_KD_KELURAHAN]) ? $data[COL_KD_KELURAHAN] : (!empty($rpps) ? $rpps[COL_KD_KELURAHAN] : null)))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Nama</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_KELOMPOKTANI?>" value="<?=!empty($data[COL_NM_KELOMPOKTANI]) ? $data[COL_NM_KELOMPOKTANI] : ''?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Ketua</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_KETUA?>" value="<?=!empty($data[COL_NM_KETUA]) ? $data[COL_NM_KETUA] : ''?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">NIK Ketua</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_KETUA_NIK?>" value="<?=!empty($data[COL_NM_KETUA_NIK]) ? $data[COL_NM_KETUA_NIK] : ''?>" />
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <label  class="control-label col-sm-4">Kepala Desa</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_KEPALADESA?>" value="<?=!empty($data[COL_NM_KEPALADESA]) ? $data[COL_NM_KEPALADESA] : ''?>" required />
                                </div>
                            </div>-->
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Jenis Kelompok Tani</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_NM_JENISKELOMPOK?>" class="form-control" required>
                                        <option value="Pemula" <?=!empty($data[COL_NM_JENISKELOMPOK]) && $data[COL_NM_JENISKELOMPOK] == "Pemula" ? "selected" : ""?>>Pemula</option>
                                        <option value="Lanjut" <?=!empty($data[COL_NM_JENISKELOMPOK]) && $data[COL_NM_JENISKELOMPOK] == "Lanjut" ? "selected" : ""?>>Lanjut</option>
                                        <option value="Madya" <?=!empty($data[COL_NM_JENISKELOMPOK]) && $data[COL_NM_JENISKELOMPOK] == "Madya" ? "selected" : ""?>>Madya</option>
                                        <option value="Utama" <?=!empty($data[COL_NM_JENISKELOMPOK]) && $data[COL_NM_JENISKELOMPOK] == "Pemula" ? "selected" : ""?>>Utama</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">No. SK</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_NOSK?>" value="<?=!empty($data[COL_NM_NOSK]) ? $data[COL_NM_NOSK] : ''?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Tahun Pembentukan</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" name="<?=COL_KD_TAHUNPEMBENTUKAN?>" value="<?=!empty($data[COL_KD_TAHUNPEMBENTUKAN]) ? $data[COL_KD_TAHUNPEMBENTUKAN] : ''?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">No. Akta Notaris</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_NOAKTANOTARIS?>" value="<?=!empty($data[COL_NM_NOAKTANOTARIS]) ? $data[COL_NM_NOAKTANOTARIS] : ''?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Status</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_STATUS?>" value="<?=!empty($data[COL_NM_STATUS]) ? $data[COL_NM_STATUS] : ''?>" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Alamat</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" rows="6" name="<?=COL_NM_ALAMAT?>"><?=!empty($data[COL_NM_ALAMAT]) ? $data[COL_NM_ALAMAT] : ''?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Latitude</label>
                                <div class="col-sm-8">
                                    <input type="text" id="lat" class="form-control" name="<?=COL_LATITUDE?>" value="<?=!empty($data[COL_LATITUDE]) ? $data[COL_LATITUDE] : ''?>" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Longitude</label>
                                <div class="col-sm-8">
                                    <input type="text" id="lng" class="form-control" name="<?=COL_LONGITUDE?>" value="<?=!empty($data[COL_LONGITUDE]) ? $data[COL_LONGITUDE] : ''?>" readonly />
                                </div>
                            </div>
                            <div class="col-sm-12" style="border: 1px solid #dedede; margin-bottom: 10px; padding-bottom: 10px;">
                                <p class="font-italic"><span class="text-red">*)</span> Klik pada map untuk menandai lokasi</p>
                                <div id="map"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-12" style="text-align: right">
                            <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                            <a href="<?=site_url('master/kelompok-tani-index')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                        </div>
                    </div>
                </div>
                <?=form_close()?>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        var map; //Will contain map object.
        var marker = false; ////Has the user plotted their location marker?

        //Function called to initialize / create the map.
        //This is called when the page has loaded.
        function initMap() {
            var currLat = $("#lat").val();
            var currLng = $("#lng").val();

            //The center location of our map.
            var centerOfMap = new google.maps.LatLng(2.2587974,98.7436999);
            if(currLat && currLng) {
                centerOfMap = new google.maps.LatLng(parseFloat(currLat), parseFloat(currLng));
            }

            //Map options.
            var options = {
                center: centerOfMap, //Set center.
                zoom: 13 //The zoom value.
            };

            //Create the map object.
            map = new google.maps.Map(document.getElementById('map'), options);
            console.log(map);

            if(currLat && currLng) {
                var currLatLng = {lat: parseFloat(currLat),lng: parseFloat(currLng)};
                var currLocation = new google.maps.Marker({
                    position: currLatLng,
                    map: map,
                    title: 'Lokasi sekarang',
                    icon: {
                        url: "http://maps.google.com/mapfiles/ms/icons/green-dot.png"
                    }
                });
            }


            //Listen for any clicks on the map.
            google.maps.event.addListener(map, 'click', function(event) {
                //Get the location that the user clicked.
                var clickedLocation = event.latLng;
                //If the marker hasn't been added.
                if(marker === false){
                    //Create the marker.
                    marker = new google.maps.Marker({
                        position: clickedLocation,
                        map: map,
                        draggable: true //make it draggable
                    });
                    //Listen for drag events!
                    google.maps.event.addListener(marker, 'dragend', function(event){
                        markerLocation();
                    });
                } else{
                    //Marker has already been added, so just change its location.
                    marker.setPosition(clickedLocation);
                }
                //Get the marker's location.
                markerLocation();
            });
        }

        //This function will get the marker's current location and then add the lat/long
        //values to our textfields so that we can save the location.
        function markerLocation(){
            //Get location.
            var currentLocation = marker.getPosition();
            //Add lat and lng values to a field that we can save.
            document.getElementById('lat').value = currentLocation.lat(); //latitude
            document.getElementById('lng').value = currentLocation.lng(); //longitude
        }

        $("[name=Kd_Kecamatan]").change(function() {
            $("[name=Kd_Kelurahan]").load("<?=site_url("ajax/get-opt-kelurahan")?>", {Kd_Kec: $(this).val()}, function () {
                $("[name=Kd_Kelurahan]").select2({ width: 'resolve' });
            });
        });
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDzvhEg4efwXcYvdMvRLDGUEau4vjh8klg&callback=initMap">
    </script>
<?php $this->load->view('footer') ?>