<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 21/07/2019
 * Time: 03:59
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_KD_KELOMPOKTANI] . '" />',
        anchor('master/kelompok-tani-edit/'.$d[COL_KD_KELOMPOKTANI],$d[COL_NM_KELOMPOKTANI]),
        $d[COL_NM_KECAMATAN],
        $d[COL_NM_KELURAHAN],
        $d[COL_KD_TAHUNPEMBENTUKAN],
        $d[COL_NM_STATUS],
    );
    $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>

<?php $this->load->view('header')
?>
    <section class="content-header">
        <h1><?= $title ?>  <small>Data</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                <?=$title?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <p>
            <?=anchor('master/kelompok-tani-delete','<i class="fa fa-trash-o"></i> Hapus',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))?>
            <?=anchor('master/kelompok-tani-add','<i class="fa fa-plus"></i> Data Baru',array('class'=>'btn btn-primary btn-sm'))?>
        </p>
        <div class="box box-default">
            <div class="box-header with-border">
                <h4 class="box-title">Filter</h4>
            </div>
            <div class="box-body">
                <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'filter-form','class'=>'form-horizontal','method'=>'get'))?>
                <div class="form-group">
                    <label  class="control-label col-sm-2">Kecamatan</label>
                    <div class="col-sm-3">
                        <select name="<?=COL_KD_KECAMATAN?>" class="form-control" required>
                            <option value="">-- Semua --</option>
                            <?=GetCombobox("SELECT *, (select count(*) from mkeltan left join mkelurahan kel on kel.Kd_Kelurahan = mkeltan.Kd_Kelurahan where kel.Kd_Kecamatan = mkecamatan.Kd_Kecamatan) as JLH_POKTAN FROM mkecamatan ORDER BY Nm_Kecamatan", COL_KD_KECAMATAN, array(COL_NM_KECAMATAN, "JLH_POKTAN"))?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label  class="control-label col-sm-2">Kelurahan</label>
                        <div class="col-sm-3">
                            <select name="<?=COL_KD_KELURAHAN?>" class="form-control" required>
                                <option value="">-- Semua --</option>
                            </select>
                        </div>
                    </div>
                </div>
                <?=form_close()?>
            </div>
        </div>
        <div id="box-result" class="box box-solid">
            <div class="box-body">

            </div>
        </div>
    </section>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
                //"sDom": "Rlfrtip",
                "aaData": <?=$data?>,
                //"bJQueryUI": true,
                //"aaSorting" : [[5,'desc']],
                "scrollY" : '40vh',
                "scrollX": "120%",
                "iDisplayLength": 100,
                "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
                "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
                "order": [[ 1, "asc" ]],
                "aoColumns": [
                    {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />",bSortable:false, "width": "10px"},
                    {"sTitle": "Kecamatan"},
                    {"sTitle": "Kelurahan"},
                    {"sTitle": "Nama"},
                    {"sTitle": "Tahun Pembentukan"},
                    {"sTitle": "Status"}
                ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });
            $("[name=Kd_Kecamatan]").change(function() {
                $("[name=Kd_Kelurahan]").load("<?=site_url("ajax/get-opt-kelurahan-poktan")?>", {Kd_Kec: $(this).val()}, function () {
                    $("[name=Kd_Kelurahan]").select2({ width: 'resolve' });
                });
            });

            $("[name=Kd_Kecamatan], [name=Kd_Kelurahan]").change(function() {
                $("#box-result").append('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
                $(".box-body", $("#box-result")).load("<?=site_url("master/kelompok-tani-index/1")?>", {Kd_Kec: $("[name=Kd_Kecamatan]").val(), Kd_Kel: $("[name=Kd_Kelurahan]").val()}, function () {
                    $("#box-result").find('.overlay').remove();
                });
            }).trigger("change");
        });
    </script>

<?php $this->load->view('footer')?>