<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 20/07/2019
 * Time: 13:36
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_KD_KEGIATAN] . '" />',
        anchor('master/kegiatan-edit/'.$d[COL_KD_KEGIATAN],$d[COL_NM_KEGIATAN],array('class' => 'modal-popup-edit', 'data-name' => $d[COL_NM_KEGIATAN], 'data-satuan' => $d[COL_NM_SATUAN])),
        $d[COL_NM_SATUAN]
    );
    $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>

<?php $this->load->view('header')
?>
    <section class="content-header">
        <h1><?= $title ?>  <small>Data</small></h1>
        <ol class="breadcrumb">
            <li>
                <a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">
                <?=$title?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <p>
            <?=anchor('master/kegiatan-delete','<i class="fa fa-trash-o"></i> Hapus',array('class'=>'cekboxaction btn btn-danger btn-sm','confirm'=>'Apa anda yakin?'))?>
            <?=anchor('master/kegiatan-add','<i class="fa fa-plus"></i> Data Baru',array('class'=>'modal-popup btn btn-primary btn-sm'))?>
        </p>
        <div class="box box-default" style="margin-bottom: 0px !important;">
            <div class="box-body">
                <form id="dataform" method="post" action="#">
                    <table id="datalist" class="table table-bordered table-hover">

                    </table>
                </form>
            </div>
        </div>
        <div class="modal fade" id="modal-editor" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-close"></i></span></button>
                        <h4 class="modal-title">Editor</h4>
                    </div>
                    <div class="modal-body">
                        <p class="text-danger error-message"></p>
                        <form id="form-editor" method="post" action="#">
                            <div class="form-group">
                                <label>Jenis Bantuan</label>
                                <input type="text" class="form-control" name="<?=COL_NM_KEGIATAN?>" required>
                            </div>
                            <div class="form-group">
                                <label>Satuan</label>
                                <input type="text" class="form-control" name="<?=COL_NM_SATUAN?>" required>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary btn-flat btn-ok">Simpan</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </section>

<?php $this->load->view('loadjs')?>
    <script type="text/javascript">
        $(document).ready(function() {
            var dataTable = $('#datalist').dataTable({
                //"sDom": "Rlfrtip",
                "aaData": <?=$data?>,
                //"bJQueryUI": true,
                //"aaSorting" : [[5,'desc']],
                "scrollY" : '44vh',
                "scrollX": "120%",
                "iDisplayLength": 100,
                "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
                "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
                "order": [[ 1, "asc" ]],
                "aoColumns": [
                    {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />", "width": "10px","bSortable":false},
                    {"sTitle": "Jenis Bantuan"},
                    {"sTitle": "Satuan"}

                ]
            });
            $('#cekbox').click(function(){
                if($(this).is(':checked')){
                    $('.cekbox').prop('checked',true);
                    console.log('clicked');
                }else{
                    $('.cekbox').prop('checked',false);
                }
            });

            $('.modal-popup, .modal-popup-edit').click(function(){
                var a = $(this);
                var name = $(this).data('name');
                var satuan = $(this).data('satuan');
                var editor = $("#modal-editor");

                $('[name=<?=COL_NM_KEGIATAN?>]', editor).val(name);
                $('[name=<?=COL_NM_SATUAN?>]', editor).val(satuan);
                editor.modal("show");
                $(".btn-ok", editor).unbind('click').click(function() {
                    $(this).html("Loading...").attr("disabled", true);
                    $('#form-editor').ajaxSubmit({
                        dataType: 'json',
                        url : a.attr('href'),
                        success : function(data){
                            if(data.error==0){
                                window.location.reload();
                            }else{
                                $(".error-message", editor).html(data.error);
                            }
                        }
                    });
                });
                return false;
            });
        });
    </script>

<?php $this->load->view('footer')?>