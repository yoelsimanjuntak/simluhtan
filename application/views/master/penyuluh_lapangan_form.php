<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 20/07/2019
 * Time: 15:03
 */
$this->load->view('header') ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('master/penyuluh-lapangan-index')?>"> <?=$title?></a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
            <input type="hidden" name="<?=COL_KD_PPL?>" value="<?=$edit?$data[COL_KD_PPL]:''?>" />
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?php if(validation_errors()){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <ul>
                                    <?= validation_errors() ?>
                                </ul>

                            </div>
                        <?php } ?>

                        <?php if(!empty($errormess)){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <?= $errormess ?>
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('success')){ ?>
                            <div class="form-group alert alert-success alert-dismissible">
                                <i class="fa fa-check"></i>
                                Berhasil.
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('error')){ ?>
                            <div class="form-group alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                Gagal mengupdate data, silahkan coba kembali
                            </div>
                        <?php } ?>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Status Penyuluh</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_KD_STATUSPENYULUH?>" class="form-control" required>
                                        <option value="PNS" <?=!empty($data[COL_KD_STATUSPENYULUH]) && $data[COL_KD_STATUSPENYULUH] == 'PNS' ? 'selected' : ''?>>PNS</option>
                                        <option value="THL" <?=!empty($data[COL_KD_STATUSPENYULUH]) && $data[COL_KD_STATUSPENYULUH] == 'THL' ? 'selected' : ''?>>THL</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Nama</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_PPL?>" value="<?=!empty($data[COL_NM_PPL]) ? $data[COL_NM_PPL] : ''?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">NIK</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_PPL_NIK?>" value="<?=!empty($data[COL_NM_PPL_NIK]) ? $data[COL_NM_PPL_NIK] : ''?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Alamat</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_PPL_ALAMAT?>" value="<?=!empty($data[COL_NM_PPL_ALAMAT]) ? $data[COL_NM_PPL_ALAMAT] : ''?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Tempat / Tgl. Lahir</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="<?=COL_NM_PPL_TEMPATLAHIR?>" value="<?=!empty($data[COL_NM_PPL_TEMPATLAHIR]) ? $data[COL_NM_PPL_TEMPATLAHIR] : ''?>" placeholder="Tempat Lahir" />
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control datepicker" name="<?=COL_NM_PPL_TANGGALLAHIR?>" value="<?=!empty($data[COL_NM_PPL_TANGGALLAHIR]) ? $data[COL_NM_PPL_TANGGALLAHIR] : ''?>" placeholder="Tanggal Lahir" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Jenis Kelamin</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_KD_JENISKELAMIN?>" class="form-control" required>
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mgender ORDER BY Nm_JenisKelamin", COL_KD_JENISKELAMIN, COL_NM_JENISKELAMIN, (!empty($data[COL_KD_JENISKELAMIN]) ? $data[COL_KD_JENISKELAMIN] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Agama</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_KD_AGAMA?>" class="form-control" required>
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mreligion ORDER BY Nm_Agama", COL_KD_AGAMA  , COL_NM_AGAMA, (!empty($data[COL_KD_AGAMA]) ? $data[COL_KD_AGAMA] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Pendidikan</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_KD_PENDIDIKAN?>" class="form-control" required>
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mpendidikan ORDER BY Nm_Pendidikan", COL_KD_PENDIDIKAN, COL_NM_PENDIDIKAN, (!empty($data[COL_KD_PENDIDIKAN]) ? $data[COL_KD_PENDIDIKAN] : null))?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label  class="control-label col-sm-4">No. SK PNS</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_PPL_SKCPNS?>" value="<?=!empty($data[COL_NM_PPL_SKCPNS]) ? $data[COL_NM_PPL_SKCPNS] : ''?>" />
                                </div>
                            </div>
                            <div class="if-pns">
                                <div class="form-group">
                                    <label  class="control-label col-sm-4">Jabatan</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="<?=COL_NM_PPL_JABATAN?>" value="<?=!empty($data[COL_NM_PPL_JABATAN]) ? $data[COL_NM_PPL_JABATAN] : ''?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label  class="control-label col-sm-4">No. Kontrak / SK Terakhir</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="<?=COL_NM_PPL_SKTERAKHIR?>" value="<?=!empty($data[COL_NM_PPL_SKTERAKHIR]) ? $data[COL_NM_PPL_SKTERAKHIR] : ''?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="if-thl">
                                <div class="form-group">
                                    <label  class="control-label col-sm-4">Tahun Kontrak</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control" name="<?=COL_NM_PPL_TAHUNKONTRAK?>" value="<?=!empty($data[COL_NM_PPL_TAHUNKONTRAK]) ? $data[COL_NM_PPL_TAHUNKONTRAK] : ''?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">No. Telepon</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="<?=COL_NM_NOTELEPON?>" value="<?=!empty($data[COL_NM_NOTELEPON]) ? $data[COL_NM_NOTELEPON] : ''?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Email</label>
                                <div class="col-sm-8">
                                    <input type="email" class="form-control" name="<?=COL_EMAIL?>" value="<?=!empty($data[COL_EMAIL]) ? $data[COL_EMAIL] : ''?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">Unit Kerja</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_KD_KECAMATAN?>" class="form-control" required>
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mkecamatan ORDER BY Nm_Kecamatan", COL_KD_KECAMATAN, COL_NM_KECAMATAN, (!empty($data[COL_KD_KECAMATAN]) ? $data[COL_KD_KECAMATAN] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4">Wilayah Kerja</label>
                                <div class="col-sm-8">
                                    <table class="table table-bordered" id="tbl-kelurahan">
                                        <thead>
                                        <tr>
                                            <th style="vertical-align: middle">Kelurahan</th>
                                            <th style="width: 40px"><button type="button" id="btn-add-kelurahan" class="btn btn-default btn-flat btn-sm"><i class="fa fa-plus"></i></button></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="tr-blueprint-kelurahan" style="display: none">
                                            <td><select name="<?=COL_KD_KELURAHAN?>[]" class="form-control no-select2" disabled></select></td>
                                            <td>
                                                <button type="button" class="btn btn-default btn-flat btn-del-kelurahan  btn-sm"><i class="fa fa-minus"></i></button>
                                            </td>
                                        </tr>
                                        <?php
                                        $kel = $this->db->where(COL_KD_PPL, ($edit?$data[COL_KD_PPL]:-999))->get(TBL_MPPL_KELURAHAN)->result_array();
                                        foreach($kel as $m) {
                                            ?>
                                            <tr>
                                                <td>
                                                    <select name="<?=COL_KD_KELURAHAN?>[]" class="form-control">
                                                        <option value="">-- Pilih --</option>
                                                        <?=GetCombobox("SELECT * FROM mkelurahan WHERE Kd_Kecamatan = ".($edit?$data[COL_KD_KECAMATAN]:-999)." ORDER BY Nm_Kelurahan", COL_KD_KELURAHAN, COL_NM_KELURAHAN, $m[COL_KD_KELURAHAN])?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <button type="button" class="btn btn-default btn-flat btn-del-kelurahan"><i class="fa fa-minus"></i></button>
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                        ?>

                                        <?php
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="box box-default">
                    <div class="box-header">
                        <h4 class="box-title">Pelatihan</h4>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered" id="tbl-det">
                            <thead>
                            <tr>
                                <th width="140px">Tahun</th>
                                <th>Nama Pelatihan</th>
                                <th style="width: 40px"><button type="button" id="btn-add-pelatihan" class="btn btn-default btn-flat"><i class="fa fa-plus"></i></button></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="tr-blueprint-pelatihan" style="display: none">
                                <td width="140px"><input type="number" name="<?=COL_KD_TAHUN?>[]" class="form-control" style="text-align: right" disabled /></td>
                                <td><input type="text" name="<?=COL_NM_PELATIHAN?>[]" class="form-control" disabled /></td>
                                <td>
                                    <button type="button" class="btn btn-default btn-flat btn-del-pelatihan"><i class="fa fa-minus"></i></button>
                                </td>
                            </tr>
                            <?php
                            $det = $this->db
                                ->where(COL_KD_PPL, ($edit?$data[COL_KD_PPL]:-999))
                                ->get(TBL_MPPL_PELATIHAN)->result_array();
                            foreach($det as $m) {
                                ?>
                                <tr>
                                    <td width="140px"><input type="number" name="<?=COL_KD_TAHUN?>[]" value="<?=$m[COL_KD_TAHUN]?>" class="form-control" style="text-align: right" /></td>
                                    <td><input type="text" name="<?=COL_NM_PELATIHAN?>[]"" value="<?=$m[COL_NM_PELATIHAN]?>" class="form-control" /></td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-flat btn-del-pelatihan"><i class="fa fa-minus"></i></button>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>

                            <?php
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                        <a href="<?=site_url('master/penyuluh-lapangan-index')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                    </div>
                </div>
                <?=form_close()?>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
    <script type="text/javascript">
        $(".btn-del-kelurahan").click(function () {
            var row = $(this).closest("tr");
            row.remove();
        });

        $("#btn-add-kelurahan").click(function () {
            var tbl = $(this).closest("table");
            var blueprint = tbl.find(".tr-blueprint-kelurahan").first().clone();

            blueprint.appendTo(tbl).removeClass("tr-blueprint-kelurahan").show();
            $("input, select", blueprint).attr('disabled', false);
            $("select", blueprint).select2({ width: 'resolve' });
            $(".btn-del-kelurahan", blueprint).click(function () {
                var row = $(this).closest("tr");
                row.remove();
            });
        });

        $("[name=Kd_Kecamatan]").change(function() {
            if($(this).val()) {
                $("[name^=Kd_Kelurahan]").load("<?=site_url("ajax/get-opt-kelurahan")?>", {Kd_Kec: $(this).val()}, function () {
                    $("[name^=Kd_Kelurahan]").val("").trigger("change");
                });
            }
        });
        $("[name^=Kd_Kelurahan].no-select2").load("<?=site_url("ajax/get-opt-kelurahan")?>", {Kd_Kec: $("[name=Kd_Kecamatan]").val()}, function () {

        });

        $("[name=Kd_StatusPenyuluh]").change(function() {
            if($(this).val() == "PNS") {
                $(".if-pns").show();
                $(".if-thl").hide();
                $("input", $(".if-thl")).val("");
            } else if($(this).val() == "THL") {
                $(".if-pns").hide();
                $(".if-thl").show();
                $("input", $(".if-pns")).val("");
            }
        }).trigger("change");

        $("#btn-add-pelatihan").click(function () {
            var tbl = $(this).closest("table");
            var blueprint = tbl.find(".tr-blueprint-pelatihan").first().clone();

            blueprint.appendTo(tbl).removeClass("tr-blueprint-pelatihan").show();
            $("input, select", blueprint).attr('disabled', false);
            $(".btn-del-pelatihan", blueprint).click(function () {
                var row = $(this).closest("tr");
                row.remove();
            });
        });
    </script>
<?php $this->load->view('footer') ?>