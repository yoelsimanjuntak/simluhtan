<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 10/21/2019
 * Time: 9:31 PM
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_KD_KELOMPOKTANI] . '" />',
        $d[COL_NM_KECAMATAN],
        $d[COL_NM_KELURAHAN],
        anchor('master/kelompok-tani-edit/'.$d[COL_KD_KELOMPOKTANI],$d[COL_NM_KELOMPOKTANI]),
        $d[COL_NM_KETUA],
        $d[COL_NM_ALAMAT],
        $d["JLH_ANGGOTA"],
        anchor('master/kelompok-tani-anggota/'.$d[COL_KD_KELOMPOKTANI], 'Daftar anggota')."&nbsp;|&nbsp;".anchor('kelompok-tani/bantuan-index/'.$d[COL_KD_KELOMPOKTANI], 'Daftar bantuan')
    );
    $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>
<form id="dataform" method="post" action="#">
    <table id="datalist" class="table table-bordered table-hover">

    </table>
</form>
<?php $this->load->view('loadjs')?>
<script type="text/javascript">
    $(document).ready(function() {
        var dataTable = $('#datalist').dataTable({
            //"sDom": "Rlfrtip",
            "aaData": <?=$data?>,
            //"bJQueryUI": true,
            //"aaSorting" : [[5,'desc']],
            "scrollY" : '40vh',
            "scrollX": "120%",
            "iDisplayLength": 100,
            "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
            "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
            "order": [[ 1, "asc" ]],
            "aoColumns": [
                {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />",bSortable:false, "width": "10px"},
                {"sTitle": "Kecamatan"},
                {"sTitle": "Kelurahan"},
                {"sTitle": "Nama"},
                {"sTitle": "Ketua"},
                {"sTitle": "Alamat"},
                {"sTitle": "Jlh. Anggota"},
                {"sTitle": "#"}
            ]
        });
        $('#cekbox').click(function(){
            if($(this).is(':checked')){
                $('.cekbox').prop('checked',true);
                console.log('clicked');
            }else{
                $('.cekbox').prop('checked',false);
            }
        });
    });
</script>