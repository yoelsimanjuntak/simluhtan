<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 9/23/2019
 * Time: 9:17 PM
 */
$ruser = GetLoggedUser();
$rpenyuluh = array();
if($ruser[COL_ROLEID] == ROLEPPL) {
    $rpenyuluh = $this->db
        ->select("Nm_PPL as Nm_Penyuluh, mkecamatan.Nm_Kecamatan, mkecamatan.Nm_Pejabat, CONCAT('Kec. ',Nm_Kecamatan) as WKPP")
        ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MPPL.".".COL_KD_KECAMATAN,"left")
        ->where(COL_KD_PPL, $ruser[COL_COMPANYID])
        ->get(TBL_MPPL)
        ->row_array();
}
else if($ruser[COL_ROLEID] == ROLEPPS) {
    $rpenyuluh = $this->db
        ->select("Nm_PPS as Nm_Penyuluh, mkecamatan.Nm_Kecamatan, mkecamatan.Nm_Pejabat, CONCAT('Desa / Kel. ',Nm_Kelurahan) as WKPP")
        ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KD_KELURAHAN." = ".TBL_MPPS.".".COL_KD_KELURAHAN,"left")
        ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MKELURAHAN.".".COL_KD_KECAMATAN,"left")
        ->where(COL_KD_PPS, $ruser[COL_COMPANYID])
        ->get(TBL_MPPS)
        ->row_array();
} else {
    if(!empty($data[COL_KD_PPL])) {
        $rpenyuluh = $this->db
            ->select("Nm_PPL as Nm_Penyuluh, mkecamatan.Nm_Kecamatan, mkecamatan.Nm_Pejabat, CONCAT('Kec. ',Nm_Kecamatan) as WKPP")
            ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MPPL.".".COL_KD_KECAMATAN,"left")
            ->where(COL_KD_PPL, $data[COL_KD_PPL])
            ->get(TBL_MPPL)
            ->row_array();
    } else {
        $rpenyuluh = $this->db
            ->select("Nm_PPS as Nm_Penyuluh, mkecamatan.Nm_Kecamatan, mkecamatan.Nm_Pejabat, CONCAT('Desa / Kel. ',Nm_Kelurahan) as WKPP")
            ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KD_KELURAHAN." = ".TBL_MPPS.".".COL_KD_KELURAHAN,"left")
            ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MKELURAHAN.".".COL_KD_KECAMATAN,"left")
            ->where(COL_KD_PPS, $data[COL_KD_PPS])
            ->get(TBL_MPPS)
            ->row_array();
    }
}
?>
<style>
    body {
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
    }
    th, td {
        padding: 5px;
    }
</style>
<?php
if(!$cetak) {
    ?>
    <?=form_open(current_url(),array('role'=>'form','class'=>'form-horizontal', 'method'=> 'get', 'target'=>'_blank'))?>
    <input type="hidden" name="<?=COL_KD_PPL?>" value="<?=$data[COL_KD_PPL]?>" />
    <input type="hidden" name="<?=COL_KD_PPS?>" value="<?=$data[COL_KD_PPS]?>" />
    <input type="hidden" name="TanggalFrom" value="<?=$data["TanggalFrom"]?>" />
    <input type="hidden" name="TanggalTo" value="<?=$data["TanggalTo"]?>" />
    <div class="form-group">
        <div class="col-sm-12" style="text-align: right">
            <button type="submit" class="btn btn-success btn-flat" title="Cetak" name="cetak" value="1"><i class="fa fa-print"></i> Cetak</button>
        </div>
    </div>
    <?=form_close()?>
    <?php
}
?>
<table width="100%">
    <tr>
        <td colspan="3" style="text-align: center">
            <img class="user-image" src="<?=MY_IMAGEURL?>logo.png" style="width: 60px" alt="Logo">
        </td>
    </tr>
    <tr>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: center; vertical-align: middle">
            <h4>LAPORAN PELAKSANAAN TUGAS PENYULUH </h4>
        </td>
    </tr>
    <tr>
        <td style="text-align: right">Nama</td>
        <td width="10px">:</td>
        <td><?=$rpenyuluh["Nm_Penyuluh"]?></td>
    </tr>
    <tr>
        <td style="text-align: right">WKPP</td>
        <td width="10px">:</td>
        <td><?=$rpenyuluh["WKPP"]?></td>
    </tr>
    <tr>
        <td style="text-align: right">Periode</td>
        <td width="10px">:</td>
        <td><?=date("d/m/Y", strtotime($data["TanggalFrom"]))?> - <?=date("d/m/Y", strtotime($data["TanggalTo"]))?></td>
    </tr>
</table>
<br />
<table class="table table-bordered" width="100%" style="border: 1px solid #000; border-spacing: 0" border="1">
    <thead>
    <tr>
        <th>No.</th>
        <th>Tanggal</th>
        <th>Kegiatan</th>
        <th>Lokasi</th>
        <th>Permasalahan</th>
        <th>Solusi</th>
        <th>TTD</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $no = 1;
    foreach($kegiatan as $k) {
        ?>
        <tr>
            <td style="text-align: center"><?=$no?></td>
            <td><?=date('d/m/Y', strtotime($k[COL_NM_TANGGAL]))?></td>
            <td><?=$k[COL_NM_KEGIATAN]?></td>
            <td><?=$k[COL_NM_LOKASI]?></td>
            <td><?=$k[COL_NM_PERMASALAHAN]?></td>
            <td><?=$k[COL_NM_SOLUSI]?></td>
            <td style="text-align: center"><?=!empty($k[COL_NM_FILE])?'&#10004':'&#10006'?></td>
        </tr>
        <?php
        $no++;
    }
    ?>
    </tbody>
</table>
<br />
<br />
<table>
    <tr>
        <td width="25%" style="text-align: center">
            Mengetahui,<br /><br />
            Camat <?=$rpenyuluh[COL_NM_KECAMATAN]?>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            (<?=!empty($rpenyuluh[COL_NM_PEJABAT])?$rpenyuluh[COL_NM_PEJABAT]:"________________________________"?>)
        </td>
        <td width="50%"></td>
        <td width="25%" style="text-align: center">
            <?=$rpenyuluh[COL_NM_KECAMATAN]?>, <?=date("d-m-Y")?><br /><br />
            <?=$ruser[COL_ROLEID]==ROLEPPL?"PPL":"PPS"?> <?=$rpenyuluh["WKPP"]?>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            (<?=!empty($rpenyuluh["Nm_Penyuluh"])?$rpenyuluh["Nm_Penyuluh"]:"________________________________"?>)
        </td>
    </tr>
</table>