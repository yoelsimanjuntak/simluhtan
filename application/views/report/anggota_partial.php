<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 10/24/2019
 * Time: 12:14 AM
 */
$ruser = GetLoggedUser();
$kec = "Semua";
$kel = "Semua";
$poktan = "Semua";
if(!empty($data[COL_KD_KECAMATAN])) {
    $rkecamatan = $this->db->where(COL_KD_KECAMATAN, $data[COL_KD_KECAMATAN])->get(TBL_MKECAMATAN)->row_array();
    if(!empty($rkecamatan)) {
        $kec = $rkecamatan[COL_NM_KECAMATAN];
    }
}
if(!empty($data[COL_KD_KELURAHAN])) {
    $rkelurahan = $this->db->where(COL_KD_KELURAHAN, $data[COL_KD_KELURAHAN])->get(TBL_MKELURAHAN)->row_array();
    if(!empty($rkelurahan)) {
        $kel = $rkelurahan[COL_NM_KELURAHAN];
    }
}
if(!empty($data[COL_KD_KELOMPOKTANI])) {
    $rpoktan = $this->db->where(COL_KD_KELOMPOKTANI, $data[COL_KD_KELOMPOKTANI])->get(TBL_MKELTAN)->row_array();
    if(!empty($rpoktan)) {
        $poktan = $rpoktan[COL_NM_KELOMPOKTANI];
    }
}
?>
<style>
    body {
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
    }
    th, td {
        padding: 5px;
    }
</style>
<?php
if(!$cetak) {
    ?>
    <?=form_open(current_url(),array('role'=>'form','class'=>'form-horizontal', 'method'=> 'get', 'target'=>'_blank'))?>
    <input type="hidden" name="<?=COL_KD_KECAMATAN?>" value="<?=$data[COL_KD_KECAMATAN]?>" />
    <input type="hidden" name="<?=COL_KD_KELURAHAN?>" value="<?=$data[COL_KD_KELURAHAN]?>" />
    <div class="form-group">
        <div class="col-sm-12" style="text-align: right">
            <button type="submit" class="btn btn-success btn-flat" title="Cetak" name="cetak" value="1"><i class="fa fa-print"></i> Cetak</button>
        </div>
    </div>
    <?=form_close()?>
<?php
}
?>
<table width="100%">
    <tr>
        <td colspan="3" style="text-align: center">
            <img class="user-image" src="<?=MY_IMAGEURL?>logo.png" style="width: 60px" alt="Logo">
        </td>
    </tr>
    <tr>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: center; vertical-align: middle">
            <h4>Rekapitulasi Anggota Kelompok Tani</h4>
        </td>
    </tr>
    <tr>
        <td width="49%" style="text-align: right">Kecamatan</td>
        <td width="1%">:</td>
        <td><?=$kec?></td>
    </tr>
    <tr>
        <td width="49%" style="text-align: right">Kelurahan</td>
        <td width="1%">:</td>
        <td><?=$kel?></td>
    </tr>
    <tr>
        <td width="49%" style="text-align: right">Kelompok Tani</td>
        <td width="1%">:</td>
        <td><?=$poktan?></td>
    </tr>
</table>
<br />
<table class="table table-bordered" width="100%" style="border: 1px solid #000; border-spacing: 0" border="1">
    <thead>
    <tr>
        <th>No.</th>
        <th>Kecamatan</th>
        <th>Desa / Kelurahan</th>
        <th>Nama Poktan</th>
        <th>Nama Anggota</th>
        <th>NIK</th>
        <th>Status Keanggotaan</th>
        <th>Komoditas</th>
        <th>Volume</th>
        <th>Musim Tanam</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $no = 1;
    foreach($anggota as $p) {
        $rkomoditas = $this->db
            ->join(TBL_MKOMODITAS,TBL_MKOMODITAS.'.'.COL_KD_KOMODITAS." = ".TBL_MKELTAN_KOMODITAS.".".COL_KD_KOMODITAS,"left")
            ->where(COL_KD_ANGGOTA, $p[COL_KD_ANGGOTA])
            ->get(TBL_MKELTAN_KOMODITAS)
            ->result_array();
        $rowspan = count($rkomoditas)>1?'rowspan="'.count($rkomoditas).'"':'';
        ?>
        <tr>
            <td <?=$rowspan?> style="text-align: center"><?=$no?></td>
            <td <?=$rowspan?>><?=$p[COL_NM_KECAMATAN]?></td>
            <td <?=$rowspan?>><?=$p[COL_NM_KELURAHAN]?></td>
            <td <?=$rowspan?>><?=$p[COL_NM_KELOMPOKTANI]?></td>
            <td <?=$rowspan?>><?=$p[COL_NM_ANGGOTA]?></td>
            <td <?=$rowspan?>><?=$p[COL_NM_ANGGOTA_NIK]?></td>
            <td <?=$rowspan?>><?=$p[COL_NM_STATUSANGGOTA]?></td>
            <?php
            if(count($rkomoditas) > 0) {
                ?>
                <td><?=$rkomoditas[0][COL_NM_KOMODITAS]?></td>
                <td style="text-align: right"><?=number_format($rkomoditas[0][COL_VOLUME], 0)?></td>
                <td style="text-align: right"><?=$rkomoditas[0][COL_MUSIM_TANAM]?></td>
                <?php
            } else {
                echo '<td>-</td><td>-</td><td>-</td>';
            }
            ?>
        </tr>
        <?php
        if(count($rkomoditas)>1) {
            for($n=1;$n<count($rkomoditas);$n++) {
                ?>
                <tr>
                    <td><?=$rkomoditas[$n][COL_NM_KOMODITAS]?></td>
                    <td style="text-align: right"><?=number_format($rkomoditas[$n][COL_VOLUME], 0)?></td>
                    <td style="text-align: right"><?=$rkomoditas[$n][COL_MUSIM_TANAM]?></td>
                </tr>
                <?php
            }
        }
        ?>
        <?php
        $no++;
    }
    ?>
    </tbody>
</table>