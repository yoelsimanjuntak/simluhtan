<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 10/24/2019
 * Time: 12:14 AM
 */

$this->load->view('header');
$ruser = GetLoggedUser();
?>
    <section class="content-header">
        <h1> <?= $title ?> <small> Generate</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Laporan Anggota Kelompok Tani</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-default">
                    <div class="box-body">
                        <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal', 'method'=> 'get'))?>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Kecamatan</label>
                            <div class="col-sm-4">
                                <select name="<?=COL_KD_KECAMATAN?>" class="form-control">
                                    <option value="">-- Semua --</option>
                                    <?=GetCombobox("SELECT * FROM mkecamatan ORDER BY Nm_Kecamatan", COL_KD_KECAMATAN, COL_NM_KECAMATAN, !empty($data)?$data[COL_KD_KECAMATAN]:null)?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Kelurahan</label>
                            <div class="col-sm-4">
                                <select name="<?=COL_KD_KELURAHAN?>" class="form-control">
                                    <option value="">-- Semua --</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Kelompok Tani</label>
                            <div class="col-sm-4">
                                <select name="<?=COL_KD_KELOMPOKTANI?>" class="form-control">
                                    <option value="">-- Semua --</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12" style="text-align: right">
                                <button type="submit" class="btn btn-default btn-flat" title="Lihat"><i class="fa fa-search"></i> Lihat</button>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
                <?php
                if(!empty($anggota)) {
                    ?>
                    <div class="box box-solid">
                        <div class="box-body">
                            <?php
                            $this->load->view('report/anggota_partial', array('data'=>$data, 'anggota'=>$anggota));
                            ?>
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>
    </section>
<?php $this->load->view('loadjs') ?>
    <script>
        $(document).ready(function() {
            $("select.readonly").select2({ width: 'resolve', disabled: 'readonly' });
            $("[name=Kd_Kecamatan]").change(function() {
                $("[name=Kd_Kelurahan]").load("<?=site_url("ajax/get-opt-kelurahan")?>", {Kd_Kec: $(this).val(), IgnoreExist: 1}, function () {
                    <?php
                    if(!empty($data[COL_KD_KELURAHAN])) {
                    ?>
                    $("option[value=<?=$data[COL_KD_KELURAHAN]?>]", $("[name=Kd_Kelurahan]")).attr("selected", true);
                    <?php
                    }
                    ?>
                    $("[name=Kd_Kelurahan]:not([type=hidden])").select2({ width: 'resolve' });
                    $("[name=Kd_Kelurahan]").change(function() {
                        $("[name=Kd_KelompokTani]").load("<?=site_url("ajax/get-opt-poktan")?>", {Kd_Kel: $(this).val()}, function () {
                            <?php
                            if(!empty($data[COL_KD_KELOMPOKTANI])) {
                            ?>
                            $("option[value=<?=$data[COL_KD_KELOMPOKTANI]?>]", $("[name=Kd_KelompokTani]")).attr("selected", true);
                            <?php
                            }
                            ?>
                            $("[name=Kd_KelompokTani]:not([type=hidden])").select2({ width: 'resolve' });
                        });
                    }).trigger("change");
                });
            }).trigger("change");
        });
    </script>
<?php $this->load->view('footer') ?>