<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 10/24/2019
 * Time: 12:14 AM
 */
$ruser = GetLoggedUser();
?>
<style>
    body {
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
    }
    th, td {
        padding: 5px;
    }
</style>
<?php
if(!$cetak) {
    $this->load->view('header');
    ?>
    <section class="content-header">
        <h1> <?= $title ?> <small> Generate</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Laporan Komoditas</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-solid">
                    <div class="box-body">
                        <?=form_open(current_url(),array('role'=>'form','class'=>'form-horizontal', 'method'=> 'get', 'target'=>'_blank'))?>
                        <div class="form-group">
                            <div class="col-sm-12" style="text-align: right">
                                <button type="submit" class="btn btn-success btn-flat" title="Cetak" name="cetak" value="1"><i class="fa fa-print"></i> Cetak</button>
                            </div>
                        </div>
                        <?=form_close()?>
<?php
}
?>
<table width="100%">
    <tr>
        <td colspan="3" style="text-align: center">
            <img class="user-image" src="<?=MY_IMAGEURL?>logo.png" style="width: 60px" alt="Logo">
        </td>
    </tr>
    <tr>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: center; vertical-align: middle">
            <h4>Rekapitulasi Komoditas</h4>
        </td>
    </tr>
</table>
<br />
<table class="table table-bordered" width="100%" style="border: 1px solid #000; border-spacing: 0" border="1">
    <thead>
    <tr>
        <th>No.</th>
        <th>Nama Kecamatan</th>
        <th>Jlh. Poktan</th>
        <th>Desa</th>
        <th>Komoditas</th>
        <th>Luas Tanam</th>
        <th>Musim Tanam</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $no = 1;
    foreach($res as $r) {
        ?>
        <tr>
            <td style="text-align: center"><?=$no?></td>
            <td><?=$r[COL_NM_KECAMATAN]?></td>
            <td><?=$r["JLH_POKTAN"]?></td>
            <td><?=$r[COL_NM_KELURAHAN]?></td>
            <td><?=$r[COL_NM_KOMODITAS]?></td>
            <td><?=$r["JLH_VOLUME"]?></td>
            <td><?=$r[COL_MUSIM_TANAM]?></td>
        </tr>
        <?php
        $no++;
    }
    ?>
    </tbody>
</table>
<?php
if(!$cetak) {
    ?>
    </div>
    </div>
    </div>
    </div>
    </section>
    <?php
    $this->load->view('loadjs');
    $this->load->view('footer');

}
?>