<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 9/23/2019
 * Time: 9:13 PM
 */
$this->load->view('header');
$ruser = GetLoggedUser();
?>
    <section class="content-header">
        <h1> <?= $title ?> <small> Generate</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Laporan Kegiatan</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-default">
                    <div class="box-body">
                        <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal', 'method'=> 'get'))?>
                        <?php
                        if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEPPL) {
                            ?>
                            <div class="form-group">
                                <label  class="control-label col-sm-3">PPL</label>
                                <div class="col-sm-6">
                                    <select name="<?=COL_KD_PPL?>" class="form-control <?=($ruser[COL_ROLEID]==ROLEPPL?"no-select2 readonly":null)?>">
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mppl ORDER BY Nm_PPL", COL_KD_PPL, COL_NM_PPL, !empty($data[COL_KD_PPL]) ? $data[COL_KD_PPL] : ($ruser[COL_ROLEID]==ROLEPPL?$ruser[COL_COMPANYID]:null))?>
                                    </select>
                                </div>
                            </div>
                        <?php
                        } if($ruser[COL_ROLEID] == ROLEADMIN || $ruser[COL_ROLEID] == ROLEPPS) {
                            ?>
                            <div class="form-group">
                                <label  class="control-label col-sm-3">PPS</label>
                                <div class="col-sm-6">
                                    <select name="<?=COL_KD_PPS?>" class="form-control <?=($ruser[COL_ROLEID]==ROLEPPS?"no-select2 readonly":null)?>">
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mpps ORDER BY Nm_PPS", COL_KD_PPS, COL_NM_PPS, !empty($data[COL_KD_PPS]) ? $data[COL_KD_PPS] : ($ruser[COL_ROLEID]==ROLEPPS?$ruser[COL_COMPANYID]:null))?>
                                    </select>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="form-group">
                            <label class="control-label col-sm-3">Tanggal</label>
                            <div class="col-sm-3">
                                <input name="TanggalFrom" class="form-control datepicker" placeholder="Dari" required="required" value="<?=!empty($data["TanggalFrom"]) ? $data["TanggalFrom"] : ""?>" />
                            </div>
                            <div class="col-sm-3">
                                <input name="TanggalTo" class="form-control datepicker" placeholder="Sampai" required="required" value="<?=!empty($data["TanggalTo"]) ? $data["TanggalTo"] : ""?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12" style="text-align: right">
                                <button type="submit" class="btn btn-default btn-flat" title="Lihat"><i class="fa fa-search"></i> Lihat</button>
                            </div>
                        </div>
                        <?=form_close()?>
                    </div>
                </div>
                <?php
                if(!empty($kegiatan)) {
                    ?>
                    <div class="box box-solid">
                        <div class="box-body">
                            <?php
                            $this->load->view('report/kegiatan_partial', array('data'=>$data, 'kegiatan'=>$kegiatan));
                            ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </section>
<?php $this->load->view('loadjs') ?>
<script>
    $(document).ready(function() {
        $("select.readonly").select2({ width: 'resolve', disabled: 'readonly' });
    });
</script>
<?php $this->load->view('footer') ?>