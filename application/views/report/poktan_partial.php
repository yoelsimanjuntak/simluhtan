<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 10/24/2019
 * Time: 12:14 AM
 */
$ruser = GetLoggedUser();
$kec = "Semua";
$kel = "Semua";
if(!empty($data[COL_KD_KECAMATAN])) {
    $rkecamatan = $this->db->where(COL_KD_KECAMATAN, $data[COL_KD_KECAMATAN])->get(TBL_MKECAMATAN)->row_array();
    if(!empty($rkecamatan)) {
        $kec = $rkecamatan[COL_NM_KECAMATAN];
    }
}
if(!empty($data[COL_KD_KELURAHAN])) {
    $rkelurahan = $this->db->where(COL_KD_KELURAHAN, $data[COL_KD_KELURAHAN])->get(TBL_MKELURAHAN)->row_array();
    if(!empty($rkelurahan)) {
        $kel = $rkelurahan[COL_NM_KELURAHAN];
    }
}
?>
<style>
    body {
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
    }
    th, td {
        padding: 5px;
    }
</style>
<?php
if(!$cetak) {
    ?>
    <?=form_open(current_url(),array('role'=>'form','class'=>'form-horizontal', 'method'=> 'get', 'target'=>'_blank'))?>
    <input type="hidden" name="<?=COL_KD_KECAMATAN?>" value="<?=$data[COL_KD_KECAMATAN]?>" />
    <input type="hidden" name="<?=COL_KD_KELURAHAN?>" value="<?=$data[COL_KD_KELURAHAN]?>" />
    <div class="form-group">
        <div class="col-sm-12" style="text-align: right">
            <button type="submit" class="btn btn-success btn-flat" title="Cetak" name="cetak" value="1"><i class="fa fa-print"></i> Cetak</button>
        </div>
    </div>
    <?=form_close()?>
<?php
}
?>
<table width="100%">
    <tr>
        <td colspan="3" style="text-align: center">
            <img class="user-image" src="<?=MY_IMAGEURL?>logo.png" style="width: 60px" alt="Logo">
        </td>
    </tr>
    <tr>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: center; vertical-align: middle">
            <h4>Rekapitulasi Kelompok Tani</h4>
        </td>
    </tr>
    <tr>
        <td width="49%" style="text-align: right">Kecamatan</td>
        <td width="1%">:</td>
        <td><?=$kec?></td>
    </tr>
    <tr>
        <td width="49%" style="text-align: right">Kelurahan</td>
        <td width="1%">:</td>
        <td><?=$kel?></td>
    </tr>
</table>
<br />
<table class="table table-bordered" width="100%" style="border: 1px solid #000; border-spacing: 0" border="1">
    <thead>
    <tr>
        <th>No.</th>
        <th>Nama PPL</th>
        <th>Nama PPS</th>
        <th>Kecamatan</th>
        <th>Desa / Kelurahan</th>
        <th>Nama Poktan</th>
        <th>Nama Ketua</th>
        <th>Alamat Sekretariat</th>
        <th>Jumlah Anggota</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $no = 1;
    foreach($poktan as $p) {
        ?>
        <tr>
            <td style="text-align: center"><?=$no?></td>
            <td><?=!empty($p[COL_NM_PPL])?$p[COL_NM_PPL]:"-"?></td>
            <td><?=!empty($p[COL_NM_PPS])?$p[COL_NM_PPS]:"-"?></td>
            <td><?=$p[COL_NM_KECAMATAN]?></td>
            <td><?=$p[COL_NM_KELURAHAN]?></td>
            <td><?=$p[COL_NM_KELOMPOKTANI]?></td>
            <td><?=$p[COL_NM_KETUA]?></td>
            <td><?=$p[COL_NM_ALAMAT]?></td>
            <td style="text-align: right"><?=number_format($p["JLH_ANGGOTA"], 0)?></td>
        </tr>
        <?php
        $no++;
    }
    ?>
    </tbody>
</table>