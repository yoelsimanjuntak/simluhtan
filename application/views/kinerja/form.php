<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 9/28/2019
 * Time: 1:45 PM
 */
?>
<?php $this->load->view('header');
$ruser = GetLoggedUser();
?>

<section class="content-header">
    <h1> <?= $title ?> <small> Form</small></h1>
    <ol class="breadcrumb">
        <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?=site_url('kinerja/index')?>"> <?=$title?></a></li>
        <li class="active"><?=$edit?'Edit':'Add'?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
        <input type="hidden" name="<?=COL_KD_KINERJA?>" value="<?=$edit?$data[COL_KD_KINERJA]:''?>" />
        <div class="col-sm-12">
            <div class="box box-primary" style="border-top-color: transparent">
                <div class="box-body">
                    <?php if(validation_errors()){ ?>
                        <div class="alert alert-danger">
                            <i class="fa fa-ban"></i> PESAN ERROR :
                            <ul>
                                <?= validation_errors() ?>
                            </ul>

                        </div>
                    <?php } ?>

                    <?php if(!empty($errormess)){ ?>
                        <div class="alert alert-danger">
                            <i class="fa fa-ban"></i> PESAN ERROR :
                            <?= $errormess ?>
                        </div>
                    <?php } ?>

                    <?php  if($this->input->get('success')){ ?>
                        <div class="form-group alert alert-success alert-dismissible">
                            <i class="fa fa-check"></i>
                            Berhasil.
                        </div>
                    <?php } ?>

                    <?php  if($this->input->get('error')){ ?>
                        <div class="form-group alert alert-danger alert-dismissible">
                            <i class="fa fa-ban"></i>
                            Gagal mengupdate data, silahkan coba kembali
                        </div>
                    <?php } ?>
                    <div class="col-sm-6">
                        <?php
                        if($ruser[COL_ROLEID] != ROLEPPL && $ruser[COL_ROLEID] != ROLEPPS) {
                            ?>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">PPL</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_KD_PPL?>" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mppl ORDER BY Nm_PPL", COL_KD_PPL, COL_NM_PPL, !empty($data[COL_KD_PPL]) ? $data[COL_KD_PPL] : null)?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-4">PPS</label>
                                <div class="col-sm-8">
                                    <select name="<?=COL_KD_PPS?>" class="form-control">
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mpps ORDER BY Nm_PPS", COL_KD_PPS, COL_NM_PPS, !empty($data[COL_KD_PPS]) ? $data[COL_KD_PPS] : null)?>
                                    </select>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="form-group">
                            <label  class="control-label col-sm-4">Tanggal</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control datepicker" name="<?=COL_NM_TANGGAL?>" value="<?=!empty($data[COL_NM_TANGGAL]) ? date('Y-m-d', strtotime($data[COL_NM_TANGGAL])) : ''?>" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="control-label col-sm-4">Keterangan</label>
                            <div class="col-sm-8">
                                <textarea name="<?=COL_NM_KETERANGAN?>" rows="5" class="form-control" ><?=!empty($data[COL_NM_KETERANGAN]) ? $data[COL_NM_KETERANGAN] : ''?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
            <div class="box box-solid">
                <div class="box-body no-padding">
                    <?php
                    $r_indikator = $this->db->order_by(COL_KD_INDIKATOR, 'asc')->get(TBL_MINDIKATOR)->result_array();
                    $idx_indikator = 1;
                    ?>
                    <table class="table table-bordered">
                        <tbody>
                        <?php
                        foreach($r_indikator as $i) {
                            ?>
                            <tr style="background: #dedede; font-weight: bold; text-transform: uppercase">
                                <td colspan="5"><?=$idx_indikator.". ".$i[COL_NM_INDIKATOR]?></td>
                            </tr>
                            <?php
                            $r_parameter = $this->db
                                ->where(COL_KD_INDIKATOR, $i[COL_KD_INDIKATOR])
                                ->order_by(COL_KD_PARAMETER, 'asc')
                                ->get(TBL_MINDIKATOR_PARAMETER)
                                ->result_array();
                            $idx_parameter = 1;
                            foreach($r_parameter as $prm) {
                                $r_parametersub = $this->db
                                    ->where(COL_KD_PARAMETER, $prm[COL_KD_PARAMETER])
                                    ->order_by(COL_KD_PARAMETERSUB, 'asc')
                                    ->get(TBL_MINDIKATOR_PARAMETERSUB)
                                    ->result_array();
                                $r_parameteropt = $this->db
                                    ->where(COL_KD_PARAMETER, $prm[COL_KD_PARAMETER])
                                    ->order_by(COL_KD_OPT, 'asc')
                                    ->get(TBL_MINDIKATOR_PARAMETEROPT)
                                    ->result_array();
                                $idx_parametersub = "a";

                                if(!empty($data)) {
                                    $rkinerja_prm = $this->db
                                        ->where(COL_KD_KINERJA, $data[COL_KD_KINERJA])
                                        ->where(COL_KD_PARAMETER, $prm[COL_KD_PARAMETER])
                                        ->get(TBL_TKINERJA_PARAMETER)
                                        ->row_array();
                                }
                                ?>
                                <tr>
                                    <td style="font-weight: bold"><?=$idx_indikator.".".$idx_parameter?></td>
                                    <td style="font-weight: bold"><?=$prm[COL_NM_PARAMETER]?></td>
                                    <td>
                                        <?php
                                        if(!empty($data)&&!empty($rkinerja_prm)&&!empty($rkinerja_prm[COL_NM_FILE])) {
                                            ?>
                                            <a href="<?=MY_UPLOADURL.$rkinerja_prm[COL_NM_FILE]?>" target="blank"><i class="fa fa-download"></i> </a>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        if($prm[COL_IS_FILEUPLOAD]==1 && !$edit) {
                                            ?>
                                            <!--<input type="file" name="FileParameter_<?=$prm[COL_KD_PARAMETER]?>" accept="application/pdf" />--->
                                            <div id="FileParameter_<?=$prm[COL_KD_PARAMETER]?>">
                                                Upload
                                            </div>
                                            <input type="hidden" name="FileParameter_<?=$prm[COL_KD_PARAMETER]?>" />
                                        <?php
                                        }
                                        ?>
                                    </td>
                                    <td <?=count($r_parametersub)>1?'rowspan="'.(count($r_parametersub)+1).'"':''?> style="white-space: nowrap">
                                        <?php
                                        foreach($r_parameteropt as $opt) {
                                            ?>
                                            <label>
                                                <input type="radio" name="OptParameter_<?=$prm[COL_KD_PARAMETER]?>" class="minimal" required="true" value="<?=$opt[COL_KD_OPT]?>" <?=!empty($data)&&!empty($rkinerja_prm)&&$rkinerja_prm[COL_KD_OPT]==$opt[COL_KD_OPT]?"checked":""?>>
                                                &nbsp;<?=$opt[COL_NM_OPT]?>
                                            </label>
                                            <br />
                                            <?php
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                                foreach($r_parametersub as $prmsub) {
                                    if(!empty($data)) {
                                        $rkinerja_prmsub = $this->db
                                            ->where(COL_KD_KINERJA, $data[COL_KD_KINERJA])
                                            ->where(COL_KD_PARAMETERSUB, $prmsub[COL_KD_PARAMETERSUB])
                                            ->get(TBL_TKINERJA_PARAMETERSUB)
                                            ->row_array();
                                    }
                                    ?>
                                    <tr>
                                        <td style="text-indent: 1.5em"><?=$idx_parametersub?></td>
                                        <td><?=$prmsub[COL_NM_PARAMETERSUB]?></td>
                                        <td>
                                            <?php
                                            if(!empty($data)&&!empty($rkinerja_prmsub)&&!empty($rkinerja_prmsub[COL_NM_FILE])) {
                                                ?>
                                                <a href="<?=MY_UPLOADURL.$rkinerja_prmsub[COL_NM_FILE]?>" target="blank"><i class="fa fa-download"></i> </a>
                                                <?php
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            if($prmsub[COL_IS_FILEUPLOAD]==1 && !$edit) {
                                                ?>
                                                <!--<input type="file" name="FileParameterSub_<?=$prmsub[COL_KD_PARAMETERSUB]?>" accept="application/pdf" />-->
                                                <div id="FileParameterSub_<?=$prmsub[COL_KD_PARAMETERSUB]?>">
                                                    Upload
                                                </div>
                                                <input type="hidden" name="FileParameterSub_<?=$prmsub[COL_KD_PARAMETERSUB]?>" />
                                            <?php
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $idx_parametersub++;
                                }
                                $idx_parameter++;
                            }
                            $idx_indikator++;
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                    <div class="col-sm-12" style="text-align: center">
                        <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                        <a href="<?=site_url('kinerja/index')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                    </div>
                </div>
            </div>
            <?=form_close()?>
        </div>
    </div>
</section>

<?php $this->load->view('loadjs') ?>
    <script>
        $(document).ready(function() {

            <?=$edit?"$('input,textarea,button[type=submit]').attr('disabled', true);":""?>
            $("[name=Kd_PPL],[name=Kd_PPS]").change(function() {
                if($("[name=Kd_PPL]").val() != "") {
                    $("[name=Kd_PPS]").val("");
                } else if($("[name=Kd_PPS]").val() != "") {
                    $("[name=Kd_PPL]").val("");
                }
            }).trigger("change");

            var el_uploads = $("[id^=FileParameter_");
            for(var i=0; i<el_uploads.length; i++) {
                var el = $(el_uploads[i])[0];
                if(el) {
                    $("#"+el.id).uploadFile({
                        url:"<?=site_url('ajax/media-upload')?>",
                        fileName:"myfile",
                        formData: { id: el.id },
                        dragdropWidth:"280px",
                        statusBarWidth:"280px",
                        showFileCounter: false,
                        returnType: "json",
                        acceptFiles:"application/pdf",
                        showStatusAfterSuccess: false,
                        onSuccess:function(files,data,xhr,pd)
                        {
                            //files: list of files
                            //data: response from server
                            //xhr : jquer xhr object
                            if(data.success == 1) {
                                $("[name="+data.id+"]").val(data.Kd_Media);
                                $("#"+data.id).append('<p class="text-green">File telah diupload : <a href="'+data.url+'" target="_blank">'+data.Nm_File+'</a></p>');
                            }
                        }
                    });
                }

            }

            var el_uploads_sub = $("[id^=FileParameterSub_");
            for(var i=0; i<el_uploads_sub.length; i++) {
                var el = $(el_uploads_sub[i])[0];
                if(el) {
                    $("#"+el.id).uploadFile({
                        url:"<?=site_url('ajax/media-upload')?>",
                        fileName:"myfile",
                        formData: { id: el.id },
                        dragdropWidth:"280px",
                        statusBarWidth:"280px",
                        showFileCounter: false,
                        returnType: "json",
                        acceptFiles:"application/pdf",
                        showStatusAfterSuccess: false,
                        onSuccess:function(files,data,xhr,pd)
                        {
                            //files: list of files
                            //data: response from server
                            //xhr : jquer xhr object
                            if(data.success == 1) {
                                $("[name="+data.id+"]").val(data.Kd_Media);
                                $("#"+data.id).append('<p class="text-green">File telah diupload : <a href="'+data.url+'" target="_blank">'+data.Nm_File+'</a></p>');
                            }
                        }
                    });
                }

            }
        });
    </script>
<?php $this->load->view('footer') ?>