<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/24/2019
 * Time: 10:57 AM
 */
?>
<div class="col-sm-4">
    <div class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Link Terkait</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="http://humbanghasundutankab.go.id/" target="_blank"><i class="fa fa-globe"></i> Website Resmi Kabupaten Humbang Hasundutan</a></li>
                <li><a href="https://diskominfo.humbanghasundutankab.go.id/" target="_blank"><i class="fa fa-globe"></i> Website Diskominfo Humbang Hasundutan</a></li>
            </ul>
        </div>
        <!-- /.box-body -->
    </div>

    <div class="box box-solid">
        <div class="box-body no-padding">
            <script async defer type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>
            <div id="gpr-kominfo-widget-container"></div>
        </div>
        <!-- /.box-body -->
    </div>
</div>