<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 6/15/2019
 * Time: 9:52 AM
 */
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        $i+1,
        //$d["Text"]
        date('d/m/Y',strtotime($d[COL_PURCHASEDATE])),
        '<input type="hidden" name="ID" value="'.$d["ID_"].'" /><input type="hidden" name="Text" value="'.$d[COL_PURCHASENO].'" /><input type="hidden" name="StockLeft" value="'.$d["QtyLeft"].'" />'.$d[COL_PURCHASENO],
        $d[COL_FUNDNAME],
        number_format($d["QtyLeft"])
    );
    $i++;
}
$data = json_encode($res);
?>
<style>
    table.table-picker > tbody > tr > td {
        cursor: pointer;
    }
</style>
<form id="dataform" method="post" action="#">
    <input type="hidden" name="selID" />
    <input type="hidden" name="selStockLeft" />
    <input type="hidden" name="selText" />
    <table id="browseGrid" class="table table-bordered table-hover nowrap table-picker" width="100%" cellspacing="0">

    </table>
</form>

<script>
    console.log(<?=$data?>);
    $(document).ready(function () {
        var dataTable = $('#browseGrid').dataTable({
            //"sDom": "Rlfrtip",
            "aaData": <?=$data?>,
            //"bJQueryUI": true,
            "aaSorting" : [[1,'desc']],
            //"scrollY" : 400,
            //"scrollX": "200%",
            "iDisplayLength": 10,
            "aLengthMenu": [[10, 100, -1], [10, 100, "Semua"]],
            "dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            //"buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
            "aoColumns": [
                {"sTitle": "#",bSortable:false,"sWidth":"10px"},
                {"sTitle": "Tanggal Masuk"},
                {"sTitle": "No. Tracking"},
                {"sTitle": "Sumber Dana"},
                {"sTitle": "Sisa"}
            ],
            "createdRow": function (row, data, index) {
                //var elBrowseTable = $("#browseGrid>tbody");
                $(row).dblclick(function () {
                    var elID = $(row).find("[name=ID][type=hidden]").first();
                    var elText = $(row).find("[name=Text][type=hidden]").first();
                    var elStockLeft = $(row).find("[name=StockLeft][type=hidden]").first();
                    $("[name=selID][type=hidden]").val(elID.val()).change();
                    $("[name=selText][type=hidden]").val(elText.val()).change();
                    $("[name=selStockLeft][type=hidden]").val(elStockLeft.val()).change();
                    $(row).closest(".modal").find("button[data-dismiss=modal]").click();
                    $(row).closest(".modal-body").empty();
                });
            }
        });
    });
</script>
<div class="clearfix"></div>