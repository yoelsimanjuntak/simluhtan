<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 21/07/2019
 * Time: 05:41
 */
$this->load->view('header');
$ruser = GetLoggedUser();
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?= $title ?> <small> Form</small></h1>
        <ol class="breadcrumb">
            <li><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?=site_url('kelompok-tani/bantuan-index')?>"> <?=$title?></a></li>
            <li class="active"><?=$edit?'Edit':'Add'?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
            <input type="hidden" name="<?=COL_KD_BANTUAN?>" value="<?=$edit?$data[COL_KD_BANTUAN]:''?>" />
            <div class="col-sm-12">
                <div class="box box-primary" style="border-top-color: transparent">
                    <div class="box-body">
                        <?php if(validation_errors()){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <ul>
                                    <?= validation_errors() ?>
                                </ul>

                            </div>
                        <?php } ?>

                        <?php if(!empty($errormess)){ ?>
                            <div class="alert alert-danger">
                                <i class="fa fa-ban"></i> PESAN ERROR :
                                <?= $errormess ?>
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('success')){ ?>
                            <div class="form-group alert alert-success alert-dismissible">
                                <i class="fa fa-check"></i>
                                Berhasil.
                            </div>
                        <?php } ?>

                        <?php  if($this->input->get('error')){ ?>
                            <div class="form-group alert alert-danger alert-dismissible">
                                <i class="fa fa-ban"></i>
                                Gagal mengupdate data, silahkan coba kembali
                            </div>
                        <?php } ?>
                        <div class="col-sm-7">
                            <div class="form-group">
                                <label  class="control-label col-sm-5">Kelompok Tani</label>
                                <div class="col-sm-7">
                                    <?php
                                    $arrkel = array();
                                    if($ruser[COL_ROLEID] == ROLEPPL) {
                                        $rkel = $this->db->where(COL_KD_PPL, $ruser[COL_COMPANYID])->get(TBL_MPPL_KELURAHAN)->result_array();

                                        foreach($rkel as $k) {
                                            $arrkel[] = $k[COL_KD_KELURAHAN];
                                        }
                                    }
                                    if($ruser[COL_ROLEID] == ROLEPPS) {
                                        $rpps = $this->db->where(COL_KD_PPS, $ruser[COL_COMPANYID])->get(TBL_MPPS)->row_array();
                                        if(!empty($rpps)) {
                                            $arrkel[] = $rpps[COL_KD_KELURAHAN];
                                        }
                                    }
                                    ?>
                                    <select name="<?=COL_KD_KELOMPOKTANI?>" class="form-control" required>
                                        <option value="">-- Pilih --</option>
                                        <?=GetCombobox("SELECT * FROM mkeltan ".(count($arrkel) > 0 ? "where Kd_Kelurahan in (".join(",", $arrkel).")" : "")." ORDER BY Nm_KelompokTani", COL_KD_KELOMPOKTANI, COL_NM_KELOMPOKTANI, (!empty($data[COL_KD_KELOMPOKTANI]) ? $data[COL_KD_KELOMPOKTANI] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-5">Jenis Bantuan</label>
                                <div class="col-sm-7">
                                    <select name="<?=COL_KD_KEGIATAN?>" class="form-control" required>
                                        <?=GetCombobox("SELECT * FROM mkegiatan ORDER BY Nm_Kegiatan", COL_KD_KEGIATAN, array(COL_NM_KEGIATAN, COL_NM_SATUAN), (!empty($data[COL_KD_KEGIATAN]) ? $data[COL_KD_KEGIATAN] : null))?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-5">Volume</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control uang" name="<?=COL_VOLUME?>" value="<?=!empty($data[COL_VOLUME]) ? $data[COL_VOLUME] : ''?>" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="control-label col-sm-5">Tahun</label>
                                <div class="col-sm-7">
                                    <input type="number" class="form-control" name="<?=COL_KD_TAHUN?>" value="<?=!empty($data[COL_KD_TAHUN]) ? $data[COL_KD_TAHUN] : ''?>" required />
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-12" style="text-align: right">
                            <button type="submit" class="btn btn-primary btn-flat">Simpan</button>
                            <a href="<?=site_url('kelompok-tani/bantuan-index')?>" class="btn btn-default btn-flat">Kembali ke Daftar&nbsp;&nbsp;<i class="fa fa-arrow-right"></i> </a>
                        </div>
                    </div>
                </div>
                <?=form_close()?>
            </div>
        </div>
    </section>

<?php $this->load->view('loadjs') ?>
<?php $this->load->view('footer') ?>