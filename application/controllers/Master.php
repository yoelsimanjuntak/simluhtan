<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 20/07/2019
 * Time: 12:50
 */
class Master extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin()) {
            redirect('user/login');
        }
    }

    function kecamatan_index() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $data['title'] = "Kecamatan";
        $data['res'] = $this->db->get(TBL_MKECAMATAN)->result_array();
        $this->load->view('master/kecamatan_index', $data);
    }

    function kecamatan_add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_KECAMATAN => $this->input->post(COL_NM_KECAMATAN),
                COL_NM_PEJABAT => $this->input->post(COL_NM_PEJABAT),
                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_MKECAMATAN, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function kecamatan_edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_KECAMATAN => $this->input->post(COL_NM_KECAMATAN),
                COL_NM_PEJABAT => $this->input->post(COL_NM_PEJABAT),
                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->where(COL_KD_KECAMATAN, $id)->update(TBL_MKECAMATAN, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function kecamatan_delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            ShowJsonError("Not Authorized.");
            return;
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MKECAMATAN, array(COL_KD_KECAMATAN => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function kelurahan_index() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $data['title'] = "Kelurahan";
        $data['res'] = $this->db
            ->select("mkecamatan.Nm_Kecamatan, mkelurahan.*")
            ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MKELURAHAN.".".COL_KD_KECAMATAN,"left")
            ->get(TBL_MKELURAHAN)->result_array();
        $this->load->view('master/kelurahan_index', $data);
    }

    function kelurahan_add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_KD_KECAMATAN => $this->input->post(COL_KD_KECAMATAN),
                COL_NM_KELURAHAN => $this->input->post(COL_NM_KELURAHAN),
                COL_NM_PEJABAT => $this->input->post(COL_NM_PEJABAT),
                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_MKELURAHAN, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function kelurahan_edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_KD_KECAMATAN => $this->input->post(COL_KD_KECAMATAN),
                COL_NM_KELURAHAN => $this->input->post(COL_NM_KELURAHAN),
                COL_NM_PEJABAT => $this->input->post(COL_NM_PEJABAT),
                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->where(COL_KD_KELURAHAN, $id)->update(TBL_MKELURAHAN, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function kelurahan_delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            ShowJsonError("Not Authorized.");
            return;
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MKELURAHAN, array(COL_KD_KELURAHAN => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function kegiatan_index() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $data['title'] = "Jenis Bantuan";
        $data['res'] = $this->db->get(TBL_MKEGIATAN)->result_array();
        $this->load->view('master/kegiatan_index', $data);
    }

    function kegiatan_add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_KEGIATAN => $this->input->post(COL_NM_KEGIATAN),
                COL_NM_SATUAN => $this->input->post(COL_NM_SATUAN),
                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_MKEGIATAN, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function kegiatan_edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_KEGIATAN => $this->input->post(COL_NM_KEGIATAN),
                COL_NM_SATUAN => $this->input->post(COL_NM_SATUAN),
                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->where(COL_KD_KEGIATAN, $id)->update(TBL_MKEGIATAN, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function kegiatan_delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            ShowJsonError("Not Authorized.");
            return;
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MKEGIATAN, array(COL_KD_KEGIATAN => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function komoditas_index() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $data['title'] = "Komoditas";
        $data['res'] = $this->db
            ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_KD_KATEGORI." = ".TBL_MKOMODITAS.".".COL_KD_KATEGORI,"left")
            ->get(TBL_MKOMODITAS)->result_array();
        $this->load->view('master/komoditas_index', $data);
    }

    function komoditas_add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_KOMODITAS => $this->input->post(COL_NM_KOMODITAS),
                COL_NM_VARIETAS => $this->input->post(COL_NM_VARIETAS),
                COL_KD_KATEGORI => $this->input->post(COL_KD_KATEGORI),
                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_MKOMODITAS, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function komoditas_edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_KOMODITAS => $this->input->post(COL_NM_KOMODITAS),
                COL_NM_VARIETAS => $this->input->post(COL_NM_VARIETAS),
                COL_KD_KATEGORI => $this->input->post(COL_KD_KATEGORI),
                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->where(COL_KD_KOMODITAS, $id)->update(TBL_MKOMODITAS, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function komoditas_delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            ShowJsonError("Not Authorized.");
            return;
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MKOMODITAS, array(COL_KD_KOMODITAS => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function pupuk_index() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $data['title'] = "Pupuk";
        $data['res'] = $this->db->get(TBL_MPUPUK)->result_array();
        $this->load->view('master/pupuk_index', $data);
    }

    function pupuk_add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_PUPUK => $this->input->post(COL_NM_PUPUK),
                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_MPUPUK, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function pupuk_edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_PUPUK => $this->input->post(COL_NM_PUPUK),
                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->where(COL_KD_PUPUK, $id)->update(TBL_MPUPUK, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function pupuk_delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            ShowJsonError("Not Authorized.");
            return;
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MPUPUK, array(COL_KD_PUPUK => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function penyuluh_lapangan_index() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $data['title'] = "Penyuluh Lapangan";
        $data['res'] = $this->db
            ->join(TBL_MGENDER,TBL_MGENDER.'.'.COL_KD_JENISKELAMIN." = ".TBL_MPPL.".".COL_KD_JENISKELAMIN,"left")
            ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MPPL.".".COL_KD_KECAMATAN,"left")
            ->get(TBL_MPPL)->result_array();
        $this->load->view('master/penyuluh_lapangan_index', $data);
    }

    function penyuluh_lapangan_add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        $data['title'] = "Penyuluh Lapangan";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $this->form_validation->set_error_delimiters('<li>', '</li>');
            $this->form_validation->set_rules(array(
                array(
                    'field' => COL_NM_PPL_NIK,
                    'label' => COL_NM_PPL_NIK,
                    'rules' => 'is_unique[mppl.Nm_PPL_NIK]',
                    'errors' => array('is_unique' => 'NIK sudah terdaftar.')
                ),
                /*array(
                    'field' => COL_KD_KECAMATAN,
                    'label' => COL_KD_KECAMATAN,
                    'rules' => 'is_unique[mppl.Kd_Kecamatan]',
                    'errors' => array('is_unique' => 'Kecamatan sudah terdaftar atas nama Penyuluh Lapangan lain.')
                ),*/
                array(
                    'field' => 'Kd_Kelurahan[]',
                    'label' => COL_KD_KELURAHAN,
                    'rules' => 'is_unique[mppl_kelurahan.Kd_Kelurahan]',
                    'errors' => array('is_unique' => 'Kelurahan sudah terdaftar atas nama Penyuluh Lapangan lain.')
                ),
            ));
            if($this->form_validation->run()) {
                $rec = array(
                    COL_KD_STATUSPENYULUH => $this->input->post(COL_KD_STATUSPENYULUH),
                    COL_NM_PPL => $this->input->post(COL_NM_PPL),
                    COL_NM_PPL_NIK => $this->input->post(COL_NM_PPL_NIK),
                    COL_NM_PPL_ALAMAT => $this->input->post(COL_NM_PPL_ALAMAT),
                    COL_NM_PPL_TEMPATLAHIR => $this->input->post(COL_NM_PPL_TEMPATLAHIR),
                    COL_NM_PPL_TANGGALLAHIR => $this->input->post(COL_NM_PPL_TANGGALLAHIR),
                    COL_KD_JENISKELAMIN => $this->input->post(COL_KD_JENISKELAMIN),
                    COL_KD_STATUSPERNIKAHAN => $this->input->post(COL_KD_STATUSPERNIKAHAN),
                    COL_KD_AGAMA => $this->input->post(COL_KD_AGAMA),
                    COL_KD_PENDIDIKAN => $this->input->post(COL_KD_PENDIDIKAN),
                    COL_NM_PPL_SKCPNS => $this->input->post(COL_NM_PPL_SKCPNS),
                    COL_NM_PPL_JABATAN => $this->input->post(COL_NM_PPL_JABATAN),
                    COL_NM_PPL_SKTERAKHIR => $this->input->post(COL_NM_PPL_SKTERAKHIR),
                    COL_NM_PPL_TAHUNKONTRAK => $this->input->post(COL_NM_PPL_TAHUNKONTRAK),
                    COL_KD_KECAMATAN => $this->input->post(COL_KD_KECAMATAN),
                    COL_NM_NOTELEPON => $this->input->post(COL_NM_NOTELEPON),
                    COL_EMAIL => $this->input->post(COL_EMAIL),
                    COL_CREATEDBY => $user[COL_USERNAME],
                    COL_CREATEDON => date('Y-m-d H:i:s')
                );

                $kdKelurahan = $this->input->post("Kd_Kelurahan");
                $nmPelatihan = $this->input->post("Nm_Pelatihan");
                $kdTahunPelatihan = $this->input->post("Kd_Tahun");

                $this->db->trans_begin();
                $res1 = $this->db->insert(TBL_MPPL, $rec);
                $kdPPL = $this->db->insert_id();

                $res2 = true;
                $res3 = true;

                $kel = array();
                $pelatihan = array();
                for($i = 0; $i<count($kdKelurahan); $i++) {
                    $kel[] = array(
                        COL_KD_PPL => $kdPPL,
                        COL_KD_KELURAHAN => $kdKelurahan[$i]
                    );
                }
                for($i = 0; $i<count($nmPelatihan); $i++) {
                    $pelatihan[] = array(
                        COL_KD_PPL => $kdPPL,
                        COL_NM_PELATIHAN => $nmPelatihan[$i],
                        COL_KD_TAHUN => $kdTahunPelatihan[$i],
                    );
                }
                if(count($kel) > 0) $res2 = $this->db->insert_batch(TBL_MPPL_KELURAHAN, $kel);
                if(count($pelatihan) > 0) $res3 = $this->db->insert_batch(TBL_MPPL_PELATIHAN, $pelatihan);

                if($res1 && $res2 && $res3) {
                    $this->db->trans_commit();
                    redirect('master/penyuluh-lapangan-index');
                } else {
                    $this->db->trans_rollback();
                    redirect(current_url()."?error=1");
                }
            }
            else {
                $this->load->view('master/penyuluh_lapangan_form', $data);
            }
        }
        else {
            $this->load->view('master/penyuluh_lapangan_form', $data);
        }
    }

    function penyuluh_lapangan_edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();

        $rdata = $data['data'] = $this->db->where(COL_KD_PPL, $id)->get(TBL_MPPL)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }
        $data['title'] = "Penyuluh Pertanian Lapangan";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rules = array();
            if($rdata[COL_NM_PPL_NIK] != $this->input->post(COL_NM_PPL_NIK)) {
                $rules[] = array(
                    'field' => COL_NM_PPL_NIK,
                    'label' => COL_NM_PPL_NIK,
                    'rules' => "is_unique[mppl.Nm_PPL_NIK]",
                    'errors' => array('is_unique' => 'NIK sudah terdaftar.')
                );
            }
            /*if($rdata[COL_KD_KECAMATAN] != $this->input->post(COL_KD_KECAMATAN)) {
                $rules[] = array(
                    'field' => COL_KD_KECAMATAN,
                    'label' => COL_KD_KECAMATAN,
                    'rules' => "is_unique[mppl.Kd_Kecamatan]",
                    'errors' => array('is_unique' => 'Kecamatan sudah terdaftar atas nama Penyuluh Lapangan lain.')
                );
            }*/
            $this->form_validation->set_error_delimiters('<li>', '</li>');
            $this->form_validation->set_rules($rules);
            if($this->form_validation->run() || count($rules) == 0) {
                $rec = array(
                    COL_KD_STATUSPENYULUH => $this->input->post(COL_KD_STATUSPENYULUH),
                    COL_NM_PPL => $this->input->post(COL_NM_PPL),
                    COL_NM_PPL_NIK => $this->input->post(COL_NM_PPL_NIK),
                    COL_NM_PPL_ALAMAT => $this->input->post(COL_NM_PPL_ALAMAT),
                    COL_NM_PPL_TEMPATLAHIR => $this->input->post(COL_NM_PPL_TEMPATLAHIR),
                    COL_NM_PPL_TANGGALLAHIR => $this->input->post(COL_NM_PPL_TANGGALLAHIR),
                    COL_KD_JENISKELAMIN => $this->input->post(COL_KD_JENISKELAMIN),
                    COL_KD_STATUSPERNIKAHAN => $this->input->post(COL_KD_STATUSPERNIKAHAN),
                    COL_KD_AGAMA => $this->input->post(COL_KD_AGAMA),
                    COL_KD_PENDIDIKAN => $this->input->post(COL_KD_PENDIDIKAN),
                    COL_NM_PPL_SKCPNS => $this->input->post(COL_NM_PPL_SKCPNS),
                    COL_NM_PPL_JABATAN => $this->input->post(COL_NM_PPL_JABATAN),
                    COL_NM_PPL_SKTERAKHIR => $this->input->post(COL_NM_PPL_SKTERAKHIR),
                    COL_NM_PPL_TAHUNKONTRAK => $this->input->post(COL_NM_PPL_TAHUNKONTRAK),
                    COL_KD_KECAMATAN => $this->input->post(COL_KD_KECAMATAN),
                    COL_NM_NOTELEPON => $this->input->post(COL_NM_NOTELEPON),
                    COL_EMAIL => $this->input->post(COL_EMAIL),
                    COL_UPDATEDBY => $user[COL_USERNAME],
                    COL_UPDATEDON => date('Y-m-d H:i:s')
                );

                $kdKelurahan = $this->input->post("Kd_Kelurahan");
                $nmPelatihan = $this->input->post("Nm_Pelatihan");
                $kdTahunPelatihan = $this->input->post("Kd_Tahun");

                $this->db->trans_begin();
                $res1 = $this->db->where(COL_KD_PPL, $id)->update(TBL_MPPL, $rec);
                $del1 = $this->db->where(COL_KD_PPL, $id)->delete(TBL_MPPL_KELURAHAN);
                $del2 = $this->db->where(COL_KD_PPL, $id)->delete(TBL_MPPL_PELATIHAN);
                $kdPPL = $id;

                $res2 = true;
                $res3 = true;

                $kel = array();
                $pelatihan = array();
                for($i = 0; $i<count($kdKelurahan); $i++) {
                    $kel[] = array(
                        COL_KD_PPL => $kdPPL,
                        COL_KD_KELURAHAN => $kdKelurahan[$i]
                    );
                }
                for($i = 0; $i<count($nmPelatihan); $i++) {
                    $pelatihan[] = array(
                        COL_KD_PPL => $kdPPL,
                        COL_NM_PELATIHAN => $nmPelatihan[$i],
                        COL_KD_TAHUN => $kdTahunPelatihan[$i],
                    );
                }
                if(count($kel) > 0) $res2 = $this->db->insert_batch(TBL_MPPL_KELURAHAN, $kel);
                if(count($pelatihan) > 0) $res3 = $this->db->insert_batch(TBL_MPPL_PELATIHAN, $pelatihan);

                if($res1 && $del1 && $del2 && $res2 && $res3) {
                    $this->db->trans_commit();
                    redirect('master/penyuluh-lapangan-index');
                } else {
                    $this->db->trans_rollback();
                    redirect(current_url()."?error=1");
                }
            }
            else {
                $this->load->view('master/penyuluh_lapangan_form', $data);
            }
        }
        else {
            $this->load->view('master/penyuluh_lapangan_form', $data);
        }
    }

    function penyuluh_lapangan_delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
            ShowJsonError("Not Authorized.");
            return;
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MPPL, array(COL_KD_PPL => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function penyuluh_swadaya_index() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEPPL) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        $data['title'] = "Penyuluh Swadaya";
        $q = $this->db
            ->join(TBL_MGENDER,TBL_MGENDER.'.'.COL_KD_JENISKELAMIN." = ".TBL_MPPS.".".COL_KD_JENISKELAMIN,"left")
            ->join(TBL_MPPL,TBL_MPPL.'.'.COL_KD_PPL." = ".TBL_MPPS.".".COL_KD_PPL,"left")
            ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MPPL.".".COL_KD_KECAMATAN,"left")
            ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KD_KELURAHAN." = ".TBL_MPPS.".".COL_KD_KELURAHAN,"left");

        if($user[COL_ROLEID] == ROLEPPL) {
            $q->where(TBL_MPPS.".".COL_KD_PPL, $user[COL_COMPANYID]);
        }
        $data['res'] = $q->get(TBL_MPPS)->result_array();
        $this->load->view('master/penyuluh_swadaya_index', $data);
    }

    function penyuluh_swadaya_add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEPPL) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        $data['title'] = "Penyuluh Swadaya";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $this->form_validation->set_error_delimiters('<li>', '</li>');
            $this->form_validation->set_rules(array(
                array(
                    'field' => COL_NM_PPS_NIK,
                    'label' => COL_NM_PPS_NIK,
                    'rules' => 'is_unique[mpps.Nm_PPS_NIK]',
                    'errors' => array('is_unique' => 'NIK sudah terdaftar.')
                ),
                array(
                    'field' => COL_KD_KELURAHAN,
                    'label' => COL_KD_KELURAHAN,
                    'rules' => 'is_unique[mpps.Kd_Kelurahan]',
                    'errors' => array('is_unique' => 'Kelurahan sudah terdaftar atas nama Penyuluh Swadaya lain.')
                ),
            ));
            if($this->form_validation->run()) {
                $rec = array(
                    COL_KD_PPL => $user[COL_ROLEID] == ROLEPPL ? $user[COL_COMPANYID] : $this->input->post(COL_KD_PPL),
                    COL_KD_STATUSPENYULUH => $this->input->post(COL_KD_STATUSPENYULUH),
                    COL_NM_PPS => $this->input->post(COL_NM_PPS),
                    COL_NM_PPS_NIK => $this->input->post(COL_NM_PPS_NIK),
                    COL_NM_PPS_ALAMAT => $this->input->post(COL_NM_PPS_ALAMAT),
                    COL_NM_PPS_TEMPATLAHIR => $this->input->post(COL_NM_PPS_TEMPATLAHIR),
                    COL_NM_PPS_TANGGALLAHIR => $this->input->post(COL_NM_PPS_TANGGALLAHIR),
                    COL_KD_JENISKELAMIN => $this->input->post(COL_KD_JENISKELAMIN),
                    COL_KD_STATUSPERNIKAHAN => $this->input->post(COL_KD_STATUSPERNIKAHAN),
                    COL_KD_AGAMA => $this->input->post(COL_KD_AGAMA),
                    COL_KD_PENDIDIKAN => $this->input->post(COL_KD_PENDIDIKAN),
                    COL_NM_PPS_SKPENETAPAN => $this->input->post(COL_NM_PPS_SKPENETAPAN),
                    COL_KD_KELURAHAN => $this->input->post(COL_KD_KELURAHAN),
                    COL_NM_NOTELEPON => $this->input->post(COL_NM_NOTELEPON),
                    COL_EMAIL => $this->input->post(COL_EMAIL),
                    COL_CREATEDBY => $user[COL_USERNAME],
                    COL_CREATEDON => date('Y-m-d H:i:s')
                );
                $nmPelatihan = $this->input->post("Nm_Pelatihan");
                $kdTahunPelatihan = $this->input->post("Kd_Tahun");

                $this->db->trans_begin();
                $res1 = $this->db->insert(TBL_MPPS, $rec);
                $kdPPS = $this->db->insert_id();

                $res2 = true;

                $pelatihan = array();
                for($i = 0; $i<count($nmPelatihan); $i++) {
                    $pelatihan[] = array(
                        COL_KD_PPS => $kdPPS,
                        COL_NM_PELATIHAN => $nmPelatihan[$i],
                        COL_KD_TAHUN => $kdTahunPelatihan[$i],
                    );
                }
                if(count($pelatihan) > 0) $res2 = $this->db->insert_batch(TBL_MPPS_PELATIHAN, $pelatihan);

                if($res1 && $res2) {
                    $this->db->trans_commit();
                    redirect('master/penyuluh-swadaya-index');
                } else {
                    $this->db->trans_rollback();
                    redirect(current_url()."?error=1");
                }
            }
            else {
                $this->load->view('master/penyuluh_swadaya_form', $data);
            }
        }
        else {
            $this->load->view('master/penyuluh_swadaya_form', $data);
        }
    }

    function penyuluh_swadaya_edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEPPL) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();

        $rdata = $data['data'] = $this->db->where(COL_KD_PPS, $id)->get(TBL_MPPS)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }
        $data['title'] = "Penyuluh Pertanian Swadaya";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rules = array();
            if($rdata[COL_NM_PPS_NIK] != $this->input->post(COL_NM_PPS_NIK)) {
                $rules[] = array(
                    'field' => COL_NM_PPS_NIK,
                    'label' => COL_NM_PPS_NIK,
                    'rules' => "is_unique[mpps.Nm_PPS_NIK]",
                    'errors' => array('is_unique' => 'NIK sudah terdaftar.')
                );
            }
            $this->form_validation->set_error_delimiters('<li>', '</li>');
            $this->form_validation->set_rules($rules);
            if($this->form_validation->run() || count($rules) == 0) {
                $rec = array(
                    COL_KD_PPL => $user[COL_ROLEID] == ROLEPPL ? $user[COL_COMPANYID] : $this->input->post(COL_KD_PPL),
                    COL_KD_STATUSPENYULUH => $this->input->post(COL_KD_STATUSPENYULUH),
                    COL_NM_PPS => $this->input->post(COL_NM_PPS),
                    COL_NM_PPS_NIK => $this->input->post(COL_NM_PPS_NIK),
                    COL_NM_PPS_ALAMAT => $this->input->post(COL_NM_PPS_ALAMAT),
                    COL_NM_PPS_TEMPATLAHIR => $this->input->post(COL_NM_PPS_TEMPATLAHIR),
                    COL_NM_PPS_TANGGALLAHIR => $this->input->post(COL_NM_PPS_TANGGALLAHIR),
                    COL_KD_JENISKELAMIN => $this->input->post(COL_KD_JENISKELAMIN),
                    COL_KD_STATUSPERNIKAHAN => $this->input->post(COL_KD_STATUSPERNIKAHAN),
                    COL_KD_AGAMA => $this->input->post(COL_KD_AGAMA),
                    COL_KD_PENDIDIKAN => $this->input->post(COL_KD_PENDIDIKAN),
                    COL_NM_PPS_SKPENETAPAN => $this->input->post(COL_NM_PPS_SKPENETAPAN),
                    COL_KD_KELURAHAN => $this->input->post(COL_KD_KELURAHAN),
                    COL_NM_NOTELEPON => $this->input->post(COL_NM_NOTELEPON),
                    COL_EMAIL => $this->input->post(COL_EMAIL),
                    COL_UPDATEDBY => $user[COL_USERNAME],
                    COL_UPDATEDON => date('Y-m-d H:i:s')
                );
                $nmPelatihan = $this->input->post("Nm_Pelatihan");
                $kdTahunPelatihan = $this->input->post("Kd_Tahun");

                $this->db->trans_begin();
                $res1 = $this->db->where(COL_KD_PPS, $id)->update(TBL_MPPS, $rec);
                $del2 = $this->db->where(COL_KD_PPS, $id)->delete(TBL_MPPS_PELATIHAN);
                $kdPPS = $id;

                $res2 = true;
                $pelatihan = array();
                for($i = 0; $i<count($nmPelatihan); $i++) {
                    $pelatihan[] = array(
                        COL_KD_PPS => $kdPPS,
                        COL_NM_PELATIHAN => $nmPelatihan[$i],
                        COL_KD_TAHUN => $kdTahunPelatihan[$i],
                    );
                }
                if(count($pelatihan) > 0) $res2 = $this->db->insert_batch(TBL_MPPS_PELATIHAN, $pelatihan);

                if($res1 && $del2 && $res2) {
                    $this->db->trans_commit();
                    redirect('master/penyuluh-swadaya-index');
                } else {
                    $this->db->trans_rollback();
                    redirect(current_url()."?error=1");
                }
            }
            else {
                $this->load->view('master/penyuluh_swadaya_form', $data);
            }
        }
        else {
            $this->load->view('master/penyuluh_swadaya_form', $data);
        }
    }

    function penyuluh_swadaya_delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEPPL) {
            ShowJsonError("Not Authorized.");
            return;
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MPPS, array(COL_KD_PPS => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function kelompok_tani_index($id=0) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEPPL && GetLoggedUser()[COL_ROLEID] != ROLEPPS) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        $data['title'] = "Kelompok Tani";

        $arrkel = array();
        if($user[COL_ROLEID] == ROLEPPL) {
            $rkel = $this->db->where(COL_KD_PPL, $user[COL_COMPANYID])->get(TBL_MPPL_KELURAHAN)->result_array();

            foreach($rkel as $k) {
                $arrkel[] = $k[COL_KD_KELURAHAN];
            }
        }
        if($user[COL_ROLEID] == ROLEPPS) {
            $rpps = $this->db->where(COL_KD_PPS, $user[COL_COMPANYID])->get(TBL_MPPS)->row_array();
            if(!empty($rpps)) {
                $arrkel[] = $rpps[COL_KD_KELURAHAN];
            }
        }

        $q = $this->db
            ->select("*,(select count(*) from mkeltan_anggota where mkeltan_anggota.Kd_KelompokTani = mkeltan.Kd_KelompokTani) as JLH_ANGGOTA")
            ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KD_KELURAHAN." = ".TBL_MKELTAN.".".COL_KD_KELURAHAN,"left")
            ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MKELURAHAN.".".COL_KD_KECAMATAN,"left");
        if(count($arrkel) > 0) {
            $q->where_in(TBL_MKELTAN.".".COL_KD_KELURAHAN, $arrkel);
        }

        $kdKec = $this->input->post("Kd_Kec");
        $kdKel = $this->input->post("Kd_Kel");
        if(!empty($kdKec)) {
            $q->where(TBL_MKECAMATAN.".".COL_KD_KECAMATAN, $kdKec);
        }
        if(!empty($kdKel)) {
            $q->where(TBL_MKELURAHAN.".".COL_KD_KELURAHAN, $kdKel);
        }

        $data['res'] = $q->get(TBL_MKELTAN)->result_array();
        if($id==1) $this->load->view('master/kelompok_tani_index_partial', $data);
        else $this->load->view('master/kelompok_tani_index', $data);
    }

    function kelompok_tani_add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEPPL && GetLoggedUser()[COL_ROLEID] != ROLEPPS) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        $data['title'] = "Kelompok Tani";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_KD_KELURAHAN => $this->input->post(COL_KD_KELURAHAN),
                COL_NM_KELOMPOKTANI => $this->input->post(COL_NM_KELOMPOKTANI),
                COL_NM_KETUA => $this->input->post(COL_NM_KETUA),
                COL_NM_KETUA_NIK => $this->input->post(COL_NM_KETUA_NIK),
                COL_NM_ALAMAT => $this->input->post(COL_NM_ALAMAT),
                COL_KD_TAHUNPEMBENTUKAN => $this->input->post(COL_KD_TAHUNPEMBENTUKAN),
                COL_NM_STATUS => $this->input->post(COL_NM_STATUS),
                COL_NM_NOAKTANOTARIS => $this->input->post(COL_NM_NOAKTANOTARIS),
                COL_NM_NOSK => $this->input->post(COL_NM_NOSK),
                COL_NM_JENISKELOMPOK => $this->input->post(COL_NM_JENISKELOMPOK),
                COL_NM_KEPALADESA => $this->input->post(COL_NM_KEPALADESA),
                COL_LATITUDE => $this->input->post(COL_LATITUDE),
                COL_LONGITUDE => $this->input->post(COL_LONGITUDE),

                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );
            if($user[COL_ROLEID] == ROLEPPS) {
                $rpps = $this->db->where(COL_KD_PPS, $user[COL_COMPANYID])->get(TBL_MPPS)->row_array();
                if(!empty($rpps)) {
                    $rec[COL_KD_KELURAHAN] = $rpps[COL_KD_KELURAHAN];
                }
            }
            $res = $this->db->insert(TBL_MKELTAN, $rec);

            if($res) {
                redirect('master/kelompok-tani-index');
            } else {
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('master/kelompok_tani_form', $data);
        }
    }

    function kelompok_tani_edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEPPL && GetLoggedUser()[COL_ROLEID] != ROLEPPS) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();

        $rdata = $data['data'] = $this->db
            ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KD_KELURAHAN." = ".TBL_MKELTAN.".".COL_KD_KELURAHAN,"left")
            ->where(COL_KD_KELOMPOKTANI, $id)
            ->get(TBL_MKELTAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }
        $data['title'] = "Kelompok Tani";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_KD_KELURAHAN => $this->input->post(COL_KD_KELURAHAN),
                COL_NM_KELOMPOKTANI => $this->input->post(COL_NM_KELOMPOKTANI),
                COL_NM_KETUA => $this->input->post(COL_NM_KETUA),
                COL_NM_KETUA_NIK => $this->input->post(COL_NM_KETUA_NIK),
                COL_NM_ALAMAT => $this->input->post(COL_NM_ALAMAT),
                COL_KD_TAHUNPEMBENTUKAN => $this->input->post(COL_KD_TAHUNPEMBENTUKAN),
                COL_NM_STATUS => $this->input->post(COL_NM_STATUS),
                COL_NM_NOAKTANOTARIS => $this->input->post(COL_NM_NOAKTANOTARIS),
                COL_NM_NOSK => $this->input->post(COL_NM_NOSK),
                COL_NM_JENISKELOMPOK => $this->input->post(COL_NM_JENISKELOMPOK),
                COL_NM_KEPALADESA => $this->input->post(COL_NM_KEPALADESA),
                COL_LATITUDE => $this->input->post(COL_LATITUDE),
                COL_LONGITUDE => $this->input->post(COL_LONGITUDE),

                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );
            $res = $this->db->where(COL_KD_KELOMPOKTANI, $id)->update(TBL_MKELTAN, $rec);
            if($res) {
                redirect('master/kelompok-tani-index');
            } else {
                $this->db->trans_rollback();
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('master/kelompok_tani_form', $data);
        }
    }

    function kelompok_tani_delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEPPL && GetLoggedUser()[COL_ROLEID] != ROLEPPS) {
            ShowJsonError("Not Authorized.");
            return;
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MKELTAN, array(COL_KD_KELOMPOKTANI => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function kelompok_tani_anggota($id=0) {
        $data['title'] = "Anggota Kelompok Tani";
        $user = GetLoggedUser();

        $arrkel = array();
        if($user[COL_ROLEID] == ROLEPPL) {
            $rkel = $this->db->where(COL_KD_PPL, $user[COL_COMPANYID])->get(TBL_MPPL_KELURAHAN)->result_array();

            foreach($rkel as $k) {
                $arrkel[] = $k[COL_KD_KELURAHAN];
            }
        }
        if($user[COL_ROLEID] == ROLEPPS) {
            $rpps = $this->db->where(COL_KD_PPS, $user[COL_COMPANYID])->get(TBL_MPPS)->row_array();
            if(!empty($rpps)) {
                $arrkel[] = $rpps[COL_KD_KELURAHAN];
            }
        }

        $q = $this->db
            ->join(TBL_MGENDER,TBL_MGENDER.'.'.COL_KD_JENISKELAMIN." = ".TBL_MKELTAN_ANGGOTA.".".COL_KD_JENISKELAMIN,"left")
            ->join(TBL_MKELTAN,TBL_MKELTAN.'.'.COL_KD_KELOMPOKTANI." = ".TBL_MKELTAN_ANGGOTA.".".COL_KD_KELOMPOKTANI,"left");
        if(count($arrkel) > 0) {
            $q->where_in(TBL_MKELTAN.".".COL_KD_KELURAHAN, $arrkel);
        }
        if($id>0) {
            $q->where(TBL_MKELTAN.".".COL_KD_KELOMPOKTANI, $id);
        }

        $data['res'] = $q->get(TBL_MKELTAN_ANGGOTA)->result_array();
        $this->load->view('master/kelompok_tani_anggota', $data);
    }

    function kelompok_tani_anggota_add() {
        $user = GetLoggedUser();
        $data['title'] = "Anggota Kelompok Tani";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_KD_KELOMPOKTANI => $this->input->post(COL_KD_KELOMPOKTANI),
                COL_NM_ANGGOTA => $this->input->post(COL_NM_ANGGOTA),
                COL_NM_ANGGOTA_NIK => $this->input->post(COL_NM_ANGGOTA_NIK),
                COL_NM_TANGGALLAHIR => $this->input->post(COL_NM_TANGGALLAHIR),
                COL_KD_JENISKELAMIN => $this->input->post(COL_KD_JENISKELAMIN),
                COL_NM_ALAMAT => $this->input->post(COL_NM_ALAMAT),
                COL_NM_STATUSANGGOTA => $this->input->post(COL_NM_STATUSANGGOTA),
                COL_NM_NOTELEPON => $this->input->post(COL_NM_NOTELEPON),
                COL_EMAIL => $this->input->post(COL_EMAIL),
                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );
            $kdKomoditas = $this->input->post("Kd_Komoditas");
            $volume = $this->input->post("Volume");
            $musim = $this->input->post("Musim_Tanam");
            $sumber = $this->input->post("Nm_SumberBenih");
            $kdPupuk = $this->input->post("Kd_Pupuk");
            $jlhPupuk = $this->input->post("Jlh_Pupuk");

            $this->db->trans_begin();
            $res1 = $this->db->insert(TBL_MKELTAN_ANGGOTA, $rec);
            $kdAnggota = $this->db->insert_id();

            $res2 = true;
            $komoditas = array();
            for($i = 0; $i<count($kdKomoditas); $i++) {
                $komoditas[] = array(
                    COL_KD_ANGGOTA => $kdAnggota,
                    COL_KD_KOMODITAS => $kdKomoditas[$i],
                    COL_VOLUME => toNum($volume[$i]),
                    COL_MUSIM_TANAM => $musim[$i],
                    COL_NM_SUMBERBENIH => $sumber[$i],
                    COL_KD_PUPUK => $kdPupuk[$i],
                    COL_JLH_PUPUK => toNum($jlhPupuk[$i]),
                );
            }
            if(count($komoditas) > 0) $res2 = $this->db->insert_batch(TBL_MKELTAN_KOMODITAS, $komoditas);

            $res3 = true;
            $dataPupuk = $this->input->post("DataPupuk");
            $pupuk = array();
            if($dataPupuk) {
                $arrPupuk = json_decode($dataPupuk, true);
                foreach($arrPupuk as $p) {
                    $pupuk[] = array(
                        COL_KD_ANGGOTA => $kdAnggota,
                        COL_KD_KOMODITAS => $p[COL_KD_KOMODITAS],
                        COL_KD_PUPUK => $p[COL_KD_PUPUK],
                        COL_JLH_PUPUK => toNum($p[COL_JLH_PUPUK])
                    );
                }
            }
            if(count($pupuk) > 0) $res3 = $this->db->insert_batch(TBL_MKELTAN_PUPUK, $pupuk);

            $res4 = true;
            $dataPestisida = $this->input->post("DataPestisida");
            $pestisida = array();
            if($dataPestisida) {
                $arrPestisida = json_decode($dataPestisida, true);
                foreach($arrPestisida as $p) {
                    $pestisida[] = array(
                        COL_KD_ANGGOTA => $kdAnggota,
                        COL_KD_KOMODITAS => $p[COL_KD_KOMODITAS],
                        COL_KD_PESTISIDA => $p[COL_KD_PESTISIDA],
                        COL_JLH_PESTISIDA => toNum($p[COL_JLH_PESTISIDA])
                    );
                }
            }
            if(count($pestisida) > 0) $res4 = $this->db->insert_batch(TBL_MKELTAN_PESTISIDA, $pestisida);

            if($res1 && $res2 && $res3 && $res4) {
                $this->db->trans_commit();
                redirect('master/kelompok-tani-anggota');
            } else {
                $this->db->trans_rollback();
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('master/kelompok_tani_anggota_form', $data);
        }
    }

    function kelompok_tani_anggota_edit($id) {
        $user = GetLoggedUser();

        $rdata = $data['data'] = $this->db->where(COL_KD_ANGGOTA, $id)->get(TBL_MKELTAN_ANGGOTA)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }
        $data['title'] = "Anggota Kelompok Tani";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_KD_KELOMPOKTANI => $this->input->post(COL_KD_KELOMPOKTANI),
                COL_NM_ANGGOTA => $this->input->post(COL_NM_ANGGOTA),
                COL_NM_ANGGOTA_NIK => $this->input->post(COL_NM_ANGGOTA_NIK),
                COL_NM_TANGGALLAHIR => $this->input->post(COL_NM_TANGGALLAHIR),
                COL_KD_JENISKELAMIN => $this->input->post(COL_KD_JENISKELAMIN),
                COL_NM_ALAMAT => $this->input->post(COL_NM_ALAMAT),
                COL_NM_STATUSANGGOTA => $this->input->post(COL_NM_STATUSANGGOTA),
                COL_NM_NOTELEPON => $this->input->post(COL_NM_NOTELEPON),
                COL_EMAIL => $this->input->post(COL_EMAIL),
                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );
            $kdKomoditas = $this->input->post("Kd_Komoditas");
            $volume = $this->input->post("Volume");
            $musim = $this->input->post("Musim_Tanam");
            $sumber = $this->input->post("Nm_SumberBenih");
            $kdPupuk = $this->input->post("Kd_Pupuk");
            $jlhPupuk = $this->input->post("Jlh_Pupuk");

            $this->db->trans_begin();
            $res1 = $this->db->where(COL_KD_ANGGOTA, $id)->update(TBL_MKELTAN_ANGGOTA, $rec);
            $del1 = $this->db->where(COL_KD_ANGGOTA, $id)->delete(TBL_MKELTAN_KOMODITAS);
            $del2 = $this->db->where(COL_KD_ANGGOTA, $id)->delete(TBL_MKELTAN_PUPUK);
            $del3 = $this->db->where(COL_KD_ANGGOTA, $id)->delete(TBL_MKELTAN_KOMODITAS);
            $kdAnggota = $id;

            $res2 = true;
            $komoditas = array();
            for($i = 0; $i<count($kdKomoditas); $i++) {
                $komoditas[] = array(
                    COL_KD_ANGGOTA => $kdAnggota,
                    COL_KD_KOMODITAS => $kdKomoditas[$i],
                    COL_VOLUME => toNum($volume[$i]),
                    COL_MUSIM_TANAM => $musim[$i],
                    COL_NM_SUMBERBENIH => $sumber[$i],
                    COL_KD_PUPUK => $kdPupuk[$i],
                    COL_JLH_PUPUK => toNum($jlhPupuk[$i]),
                );
            }
            if(count($komoditas) > 0) $res2 = $this->db->insert_batch(TBL_MKELTAN_KOMODITAS, $komoditas);

            $dataPupuk = $this->input->post("DataPupuk");
            $dataPestisida = $this->input->post("DataPestisida");
            $pupuk = array();
            $pestisida = array();

            $res3 = true;
            if($dataPupuk) {
                $arrPupuk = json_decode($dataPupuk, true);
                foreach($arrPupuk as $p) {
                    $pupuk[] = array(
                        COL_KD_ANGGOTA => $kdAnggota,
                        COL_KD_KOMODITAS => $p[COL_KD_KOMODITAS],
                        COL_KD_PUPUK => $p[COL_KD_PUPUK],
                        COL_JLH_PUPUK => toNum($p[COL_JLH_PUPUK])
                    );
                }
            }
            if(count($pupuk) > 0) $res3 = $this->db->insert_batch(TBL_MKELTAN_PUPUK, $pupuk);

            $res4 = true;
            if($dataPestisida) {
                $arrPestisida = json_decode($dataPestisida, true);
                foreach($arrPestisida as $p) {
                    $pestisida[] = array(
                        COL_KD_ANGGOTA => $kdAnggota,
                        COL_KD_KOMODITAS => $p[COL_KD_KOMODITAS],
                        COL_KD_PESTISIDA => $p[COL_KD_PESTISIDA],
                        COL_JLH_PESTISIDA => toNum($p[COL_JLH_PESTISIDA])
                    );
                }
            }
            if(count($pestisida) > 0) $res4 = $this->db->insert_batch(TBL_MKELTAN_PESTISIDA, $pestisida);

            if($res1 && $del1 && $res2 && $del2 && $res3 && $del3 && $res4) {
                $this->db->trans_commit();
                redirect('master/kelompok-tani-anggota');
            } else {
                $this->db->trans_rollback();
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('master/kelompok_tani_anggota_form', $data);
        }
    }

    function kelompok_tani_anggota_delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MKELTAN_ANGGOTA, array(COL_KD_ANGGOTA => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }

    function pestisida_index() {
        $data['title'] = "Pestisida";
        $data['res'] = $this->db->get(TBL_MPESTISIDA)->result_array();
        $this->load->view('master/pestisida_index', $data);
    }

    function pestisida_add() {
        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_PESTISIDA => $this->input->post(COL_NM_PESTISIDA),
                COL_NM_SATUAN => $this->input->post(COL_NM_SATUAN),
                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->insert(TBL_MPESTISIDA, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function pestisida_edit($id) {
        $user = GetLoggedUser();
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $data = array(
                COL_NM_PESTISIDA => $this->input->post(COL_NM_PESTISIDA),
                COL_NM_SATUAN => $this->input->post(COL_NM_SATUAN),
                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );

            $res = $this->db->where(COL_KD_PESTISIDA, $id)->update(TBL_MPESTISIDA, $data);
            if($res) {
                ShowJsonSuccess("Berhasil");
            } else {
                ShowJsonError("Gagal");
            }
        }
    }

    function pestisida_delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_MPESTISIDA, array(COL_KD_PESTISIDA => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }
}