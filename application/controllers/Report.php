<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 9/22/2019
 * Time: 5:42 PM
 */
class Report extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin()) {
            redirect('user/login');
        }
    }

    function kegiatan() {
        $user = GetLoggedUser();
        $dat['title'] = "Laporan Kegiatan";
        $dat['edit'] = false;
        $dat['cetak'] = $cetak = $this->input->get("cetak");

        if(!empty($_GET)) {
            $q = $this->db
                ->select(TBL_TKEGIATAN."."."*,COALESCE(Nm_PPL, Nm_PPS) as Nm_Penyuluh")
                ->join(TBL_MPPL,TBL_MPPL.'.'.COL_KD_PPL." = ".TBL_TKEGIATAN.".".COL_KD_PPL,"left")
                ->join(TBL_MPPS,TBL_MPPS.'.'.COL_KD_PPS." = ".TBL_TKEGIATAN.".".COL_KD_PPS,"left")
                ->where(COL_NM_TANGGAL." >= ", $this->input->get("TanggalFrom"))
                ->where(COL_NM_TANGGAL." <= ", $this->input->get("TanggalTo"));
            if($user[COL_ROLEID] == ROLEPPL) {
                $q->where(TBL_TKEGIATAN.".".COL_KD_PPL, $user[COL_COMPANYID]);
            }
            else if($user[COL_ROLEID] == ROLEPPS) {
                $q->where(TBL_TKEGIATAN.".".COL_KD_PPS, $user[COL_COMPANYID]);
            } else {
                $_ppl = $this->input->get(COL_KD_PPL);
                $_pps = $this->input->get(COL_KD_PPS);
                if(!empty($_ppl)) {
                    $q->where(TBL_TKEGIATAN.".".COL_KD_PPL, $_ppl);
                } else if(!empty($_pps)) {
                    $q->where(TBL_TKEGIATAN.".".COL_KD_PPS, $_pps);
                }
            }
            $dat['data'] = $_GET;
            $dat['kegiatan'] = $q->get(TBL_TKEGIATAN)->result_array();
            $dat['edit'] = true;
        } else {
            $dat['data'] = array(
                "TanggalFrom" => date("Y-m")."-01",
                "TanggalTo" => date("Y-m-t", strtotime(date("Y-m-d")))
            );
        }

        if($cetak) {
            $this->load->library('Mypdf');
            $mpdf = new Mypdf();
            $mpdf->pdf->AddPage('L');
            //$mpdf = new Mypdf();

            $html = $this->load->view('report/kegiatan_partial', $dat, TRUE);
            $mpdf->pdf->WriteHTML($html);
            $mpdf->pdf->SetFooter("Dokumen ini dicetak melalui aplikasi <b>Sistem Informasi Manajemen Penyuluhan Pertanian (SIMLUHTAN)</b>: <i>https://simluhtan.humbanghasundutankab.go.id</i>");
            $mpdf->pdf->Output('Laporan Pelaksanaan Tugas Penyuluh.pdf', 'I');
        }
        else $this->load->view('report/kegiatan', $dat);
    }

    function poktan() {
        $user = GetLoggedUser();
        $dat['title'] = "Rekapitulasi Kelompok Tani";
        $dat['edit'] = false;
        $dat['cetak'] = $cetak = $this->input->get("cetak");

        if(!empty($_GET)) {
            $arrkel = array();
            if($user[COL_ROLEID] == ROLEPPL) {
                $rkel = $this->db->where(COL_KD_PPL, $user[COL_COMPANYID])->get(TBL_MPPL_KELURAHAN)->result_array();

                foreach($rkel as $k) {
                    $arrkel[] = $k[COL_KD_KELURAHAN];
                }
            }
            if($user[COL_ROLEID] == ROLEPPS) {
                $rpps = $this->db->where(COL_KD_PPS, $user[COL_COMPANYID])->get(TBL_MPPS)->row_array();
                if(!empty($rpps)) {
                    $arrkel[] = $rpps[COL_KD_KELURAHAN];
                }
            }

            $q = $this->db
                ->select("mkeltan.*, mkelurahan.Nm_Kelurahan, mkecamatan.Nm_Kecamatan, mpps.Nm_PPS, mppl.Nm_PPL,(select count(*) from mkeltan_anggota where mkeltan_anggota.Kd_KelompokTani = mkeltan.Kd_KelompokTani) as JLH_ANGGOTA")
                ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KD_KELURAHAN." = ".TBL_MKELTAN.".".COL_KD_KELURAHAN,"left")
                ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MKELURAHAN.".".COL_KD_KECAMATAN,"left")
                ->join(TBL_MPPS,TBL_MPPS.'.'.COL_KD_KELURAHAN." = ".TBL_MKELTAN.".".COL_KD_KELURAHAN,"left")
                ->join(TBL_MPPL,TBL_MPPL.'.'.COL_KD_PPL." = ".TBL_MPPS.".".COL_KD_PPL,"left");
            if(count($arrkel) > 0) {
                $q->where_in(TBL_MKELTAN.".".COL_KD_KELURAHAN, $arrkel);
            }

            $kdKec = $this->input->get(COL_KD_KECAMATAN);
            $kdKel = $this->input->get(COL_KD_KELURAHAN);
            if(!empty($kdKec)) {
                $q->where(TBL_MKECAMATAN.".".COL_KD_KECAMATAN, $kdKec);
            }
            if(!empty($kdKel)) {
                $q->where(TBL_MKELURAHAN.".".COL_KD_KELURAHAN, $kdKel);
            }
            $dat['data'] = $_GET;
            $dat['poktan'] = $q->get(TBL_MKELTAN)->result_array();
            $dat['edit'] = true;
        }

        if($cetak) {
            $this->load->library('Mypdf');
            $mpdf = new Mypdf();
            $mpdf->pdf->AddPage('L');
            //$mpdf = new Mypdf();

            $html = $this->load->view('report/poktan_partial', $dat, TRUE);
            $mpdf->pdf->WriteHTML($html);
            $mpdf->pdf->SetFooter("Dokumen ini dicetak melalui aplikasi <b>Sistem Informasi Manajemen Penyuluhan Pertanian (SIMLUHTAN)</b>: <i>https://simluhtan.humbanghasundutankab.go.id</i>");
            $mpdf->pdf->Output('Rekapitulasi Kelompok Tani.pdf', 'I');
        }
        else $this->load->view('report/poktan', $dat);
    }

    function poktan_anggota() {
        $user = GetLoggedUser();
        $dat['title'] = "Rekapitulasi Anggota Kelompok Tani";
        $dat['edit'] = false;
        $dat['cetak'] = $cetak = $this->input->get("cetak");

        if(!empty($_GET)) {
            $arrkel = array();
            if($user[COL_ROLEID] == ROLEPPL) {
                $rkel = $this->db->where(COL_KD_PPL, $user[COL_COMPANYID])->get(TBL_MPPL_KELURAHAN)->result_array();

                foreach($rkel as $k) {
                    $arrkel[] = $k[COL_KD_KELURAHAN];
                }
            }
            if($user[COL_ROLEID] == ROLEPPS) {
                $rpps = $this->db->where(COL_KD_PPS, $user[COL_COMPANYID])->get(TBL_MPPS)->row_array();
                if(!empty($rpps)) {
                    $arrkel[] = $rpps[COL_KD_KELURAHAN];
                }
            }

            $q = $this->db
                ->select("mkeltan.*, mkeltan_anggota.*, mkelurahan.Nm_Kelurahan, mkecamatan.Nm_Kecamatan, mpps.Nm_PPS, mppl.Nm_PPL")
                ->join(TBL_MKELTAN,TBL_MKELTAN.'.'.COL_KD_KELOMPOKTANI." = ".TBL_MKELTAN_ANGGOTA.".".COL_KD_KELOMPOKTANI,"left")
                ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KD_KELURAHAN." = ".TBL_MKELTAN.".".COL_KD_KELURAHAN,"left")
                ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MKELURAHAN.".".COL_KD_KECAMATAN,"left")
                ->join(TBL_MPPS,TBL_MPPS.'.'.COL_KD_KELURAHAN." = ".TBL_MKELTAN.".".COL_KD_KELURAHAN,"left")
                ->join(TBL_MPPL,TBL_MPPL.'.'.COL_KD_PPL." = ".TBL_MPPS.".".COL_KD_PPL,"left");
            if(count($arrkel) > 0) {
                $q->where_in(TBL_MKELTAN.".".COL_KD_KELURAHAN, $arrkel);
            }

            $kdKec = $this->input->get(COL_KD_KECAMATAN);
            $kdKel = $this->input->get(COL_KD_KELURAHAN);
            $kdPoktan = $this->input->get(COL_KD_KELOMPOKTANI);
            if(!empty($kdKec)) {
                $q->where(TBL_MKECAMATAN.".".COL_KD_KECAMATAN, $kdKec);
            }
            if(!empty($kdKel)) {
                $q->where(TBL_MKELURAHAN.".".COL_KD_KELURAHAN, $kdKel);
            }
            if(!empty($kdPoktan)) {
                $q->where(TBL_MKELTAN.".".COL_KD_KELOMPOKTANI, $kdPoktan);
            }
            $dat['data'] = $_GET;
            $dat['anggota'] = $q->get(TBL_MKELTAN_ANGGOTA)->result_array();
            $dat['edit'] = true;
        }

        if($cetak) {
            $this->load->library('Mypdf');
            $mpdf = new Mypdf();
            $mpdf->pdf->AddPage('L');
            //$mpdf = new Mypdf();

            $html = $this->load->view('report/anggota_partial', $dat, TRUE);
            $mpdf->pdf->WriteHTML($html);
            $mpdf->pdf->SetFooter("Dokumen ini dicetak melalui aplikasi <b>Sistem Informasi Manajemen Penyuluhan Pertanian (SIMLUHTAN)</b>: <i>https://simluhtan.humbanghasundutankab.go.id</i>");
            $mpdf->pdf->Output('Rekapitulasi Anggota Kelompok Tani.pdf', 'I');
        }
        else $this->load->view('report/anggota', $dat);
    }

    function bantuan() {
        $user = GetLoggedUser();
        $dat['title'] = "Rekapitulasi Bantuan Kelompok Tani";
        $dat['edit'] = false;
        $dat['cetak'] = $cetak = $this->input->get("cetak");

        if(!empty($_GET)) {
            $arrkel = array();
            if($user[COL_ROLEID] == ROLEPPL) {
                $rkel = $this->db->where(COL_KD_PPL, $user[COL_COMPANYID])->get(TBL_MPPL_KELURAHAN)->result_array();

                foreach($rkel as $k) {
                    $arrkel[] = $k[COL_KD_KELURAHAN];
                }
            }
            if($user[COL_ROLEID] == ROLEPPS) {
                $rpps = $this->db->where(COL_KD_PPS, $user[COL_COMPANYID])->get(TBL_MPPS)->row_array();
                if(!empty($rpps)) {
                    $arrkel[] = $rpps[COL_KD_KELURAHAN];
                }
            }

            $q = $this->db
                ->select("tkeltan_bantuan.*, mkeltan.*, mkelurahan.Nm_Kelurahan, mkecamatan.Nm_Kecamatan, mkegiatan.Nm_Kegiatan")
                ->join(TBL_MKEGIATAN,TBL_MKEGIATAN.'.'.COL_KD_KEGIATAN." = ".TBL_TKELTAN_BANTUAN.".".COL_KD_KEGIATAN,"left")
                ->join(TBL_MKELTAN,TBL_MKELTAN.'.'.COL_KD_KELOMPOKTANI." = ".TBL_TKELTAN_BANTUAN.".".COL_KD_KELOMPOKTANI,"left")
                ->join(TBL_MKELURAHAN,TBL_MKELURAHAN.'.'.COL_KD_KELURAHAN." = ".TBL_MKELTAN.".".COL_KD_KELURAHAN,"left")
                ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KD_KECAMATAN." = ".TBL_MKELURAHAN.".".COL_KD_KECAMATAN,"left")
                ->join(TBL_MPPS,TBL_MPPS.'.'.COL_KD_KELURAHAN." = ".TBL_MKELTAN.".".COL_KD_KELURAHAN,"left")
                ->join(TBL_MPPL,TBL_MPPL.'.'.COL_KD_PPL." = ".TBL_MPPS.".".COL_KD_PPL,"left");
            if(count($arrkel) > 0) {
                $q->where_in(TBL_MKELTAN.".".COL_KD_KELURAHAN, $arrkel);
            }

            $kdKec = $this->input->get(COL_KD_KECAMATAN);
            $kdKel = $this->input->get(COL_KD_KELURAHAN);
            if(!empty($kdKec)) {
                $q->where(TBL_MKECAMATAN.".".COL_KD_KECAMATAN, $kdKec);
            }
            if(!empty($kdKel)) {
                $q->where(TBL_MKELURAHAN.".".COL_KD_KELURAHAN, $kdKel);
            }
            $dat['data'] = $_GET;
            $dat['bantuan'] = $q->get(TBL_TKELTAN_BANTUAN)->result_array();
            $dat['edit'] = true;
        }

        if($cetak) {
            $this->load->library('Mypdf');
            $mpdf = new Mypdf();
            $mpdf->pdf->AddPage('L');
            //$mpdf = new Mypdf();

            $html = $this->load->view('report/bantuan_partial', $dat, TRUE);
            $mpdf->pdf->WriteHTML($html);
            $mpdf->pdf->SetFooter("Dokumen ini dicetak melalui aplikasi <b>Sistem Informasi Manajemen Penyuluhan Pertanian (SIMLUHTAN)</b>: <i>https://simluhtan.humbanghasundutankab.go.id</i>");
            $mpdf->pdf->Output('Rekapitulasi Bantuan Kelompok Tani.pdf', 'I');
        }
        else $this->load->view('report/bantuan', $dat);
    }

    function poktan_komoditas() {
        $user = GetLoggedUser();
        $dat['title'] = "Rekapitulasi Komoditas";
        $dat['edit'] = false;
        $dat['cetak'] = $cetak = $this->input->get("cetak");
        $dat['edit'] = true;

        $q = @"
        SELECT DISTINCT
        (SELECT COUNT(DISTINCT kom_.Kd_Komoditas)
            FROM mkeltan_komoditas kom_
            LEFT JOIN mkeltan_anggota a_ ON a_.Kd_Anggota = kom_.Kd_Anggota
            LEFT JOIN mkeltan poktan_ ON poktan_.Kd_KelompokTani = a_.Kd_KelompokTani
            LEFT JOIN mkelurahan kel_ ON kel_.Kd_Kelurahan = poktan_.Kd_Kelurahan
            LEFT JOIN mkecamatan kec_ ON kec_.Kd_Kecamatan = kel_.Kd_Kecamatan
            WHERE kec_.Kd_Kecamatan = kec.Kd_Kecamatan
        ) AS ROWSPAN,
        kec.Nm_Kecamatan,
        GROUP_CONCAT(kel.Nm_Kelurahan SEPARATOR '<br />') AS Nm_Kelurahan,
        GROUP_CONCAT(kel.Nm_Kelurahan SEPARATOR '<br />') AS Nm_Kelurahan,
        mkom.Nm_Komoditas,
        COUNT(poktan.Kd_KelompokTani) AS JLH_POKTAN,
        SUM(kom.Volume) AS JLH_VOLUME,
        GROUP_CONCAT(DISTINCT CONCAT(kom.Musim_Tanam, ' x setahun') ORDER BY kom.Musim_Tanam SEPARATOR '<br />') AS Musim_Tanam
        FROM mkeltan_komoditas kom
        LEFT JOIN mkomoditas mkom ON mkom.Kd_Komoditas = kom.Kd_Komoditas
        LEFT JOIN mkeltan_anggota a ON a.Kd_Anggota = kom.Kd_Anggota
        LEFT JOIN mkeltan poktan ON poktan.Kd_KelompokTani = a.Kd_KelompokTani
        LEFT JOIN mkelurahan kel ON kel.Kd_Kelurahan = poktan.Kd_Kelurahan
        LEFT JOIN mkecamatan kec ON kec.Kd_Kecamatan = kel.Kd_Kecamatan
        GROUP BY kec.Nm_Kecamatan, mkom.Nm_Komoditas
        ORDER BY kec.Nm_Kecamatan, mkom.Nm_Komoditas
        ";
        $dat['res'] = $this->db->query($q)->result_array();

        if($cetak) {
            $this->load->library('Mypdf');
            $mpdf = new Mypdf();
            $mpdf->pdf->AddPage('L');
            //$mpdf = new Mypdf();

            $html = $this->load->view('report/komoditas', $dat, TRUE);
            $mpdf->pdf->WriteHTML($html);
            $mpdf->pdf->SetFooter("Dokumen ini dicetak melalui aplikasi <b>Sistem Informasi Manajemen Penyuluhan Pertanian (SIMLUHTAN)</b>: <i>https://simluhtan.humbanghasundutankab.go.id</i>");
            $mpdf->pdf->Output('Rekapitulasi Komoditas.pdf', 'I');
        }
        else $this->load->view('report/komoditas', $dat);
    }
}