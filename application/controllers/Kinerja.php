<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 9/28/2019
 * Time: 12:59 PM
 */
class Kinerja extends MY_Controller {
    function __construct() {
        parent::__construct();
        ini_set('post_max_size', '50000M');
        if(!IsLogin()) {
            redirect('user/login');
        }
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEPPS && GetLoggedUser()[COL_ROLEID] != ROLEPPL) {
            redirect('user/dashboard');
        }
    }

    function index() {
        $data['title'] = "Kinerja Penyuluh";
        $user = GetLoggedUser();
        $q = $this->db
            ->select(TBL_TKINERJA."."."*,COALESCE(Nm_PPL, Nm_PPS) as Nm_Penyuluh")
            ->join(TBL_MPPL,TBL_MPPL.'.'.COL_KD_PPL." = ".TBL_TKINERJA.".".COL_KD_PPL,"left")
            ->join(TBL_MPPS,TBL_MPPS.'.'.COL_KD_PPS." = ".TBL_TKINERJA.".".COL_KD_PPS,"left");

        if($user[COL_ROLEID] == ROLEPPL) {
            $q->where(TBL_TKINERJA.".".COL_KD_PPL, $user[COL_COMPANYID]);
        }
        if($user[COL_ROLEID] == ROLEPPS) {
            $q->where(TBL_TKINERJA.".".COL_KD_PPS, $user[COL_COMPANYID]);
        }
        $data['res'] = $q->get(TBL_TKINERJA)->result_array();

        $this->load->view('kinerja/index', $data);
    }

    function add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEPPL && GetLoggedUser()[COL_ROLEID] != ROLEPPS) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        $data['title'] = "Kinerja Penyuluh";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_KD_PPL => $this->input->post(COL_KD_PPL),
                COL_KD_PPS => $this->input->post(COL_KD_PPS),
                COL_NM_TANGGAL => date('Y-m-d', strtotime($this->input->post(COL_NM_TANGGAL))),
                COL_NM_KETERANGAN => $this->input->post(COL_NM_KETERANGAN),

                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            if($user[COL_ROLEID] == ROLEPPL) {
                $rec[COL_KD_PPL] = $user[COL_COMPANYID];
                $rec[COL_KD_PPS] = null;
            }
            else if($user[COL_ROLEID] == ROLEPPS) {
                $rec[COL_KD_PPL] = null;
                $rec[COL_KD_PPS] = $user[COL_COMPANYID];
            }

            $this->db->trans_begin();
            try {
                $res = $this->db->insert(TBL_TKINERJA, $rec);
                $KdKinerja = $this->db->insert_id();

                $arrParam = array();
                $arrParamSub = array();
                $r_parameter = $this->db->order_by(COL_KD_PARAMETER, 'asc')->get(TBL_MINDIKATOR_PARAMETER)->result_array();
                foreach($r_parameter as $prm) {
                    $opt = $this->input->post("OptParameter_".$prm[COL_KD_PARAMETER]);
                    $r_parameter_opt = $this->db
                        ->where(COL_KD_PARAMETER, $prm[COL_KD_PARAMETER])
                        ->where(COL_KD_OPT, $opt)
                        ->get(TBL_MINDIKATOR_PARAMETEROPT)
                        ->row_array();

                    $arrParam_tmp = array(
                        COL_KD_KINERJA => $KdKinerja,
                        COL_KD_PARAMETER => $prm[COL_KD_PARAMETER],
                        COL_KD_OPT => $opt,
                        COL_SKOR => !empty($r_parameter_opt)?$r_parameter_opt[COL_POIN]:0,
                        COL_NM_FILE => null
                    );

                    if($prm[COL_IS_FILEUPLOAD] == 1) {
                        $kdMedia = $this->input->post("FileParameter_".$prm[COL_KD_PARAMETER]);
                        if (!empty($kdMedia)) {
                            $rmedia = $this->db->where(COL_KD_MEDIA, $kdMedia)->get(TBL_MEDIA)->row_array();
                            if($rmedia) {
                                $arrParam_tmp[COL_NM_FILE] = $rmedia[COL_NM_FILE];
                            }
                            $this->db->where(COL_KD_MEDIA, $kdMedia)->delete(TBL_MEDIA);
                        }
                    }

                    $r_parameter_sub = $this->db
                        ->where(COL_KD_PARAMETER, $prm[COL_KD_PARAMETER])
                        ->get(TBL_MINDIKATOR_PARAMETERSUB)
                        ->result_array();

                    foreach($r_parameter_sub as $sub) {
                        if($sub[COL_IS_FILEUPLOAD] == 1) {
                            $kdMedia_ = $this->input->post("FileParameterSub_".$sub[COL_KD_PARAMETERSUB]);
                            if (!empty($kdMedia_)) {
                                $rmedia_ = $this->db->where(COL_KD_MEDIA, $kdMedia_)->get(TBL_MEDIA)->row_array();
                                if($rmedia_) {
                                    $arrParamSub[] = array(
                                        COL_KD_KINERJA => $KdKinerja,
                                        COL_KD_PARAMETERSUB => $sub[COL_KD_PARAMETERSUB],
                                        COL_NM_FILE => $rmedia_[COL_NM_FILE]
                                    );
                                }
                                $this->db->where(COL_KD_MEDIA, $kdMedia_)->delete(TBL_MEDIA);
                            }
                        }
                    }

                    $arrParam[] = $arrParam_tmp;
                }

                $res2 = true;
                $res3 = true;
                if(count($arrParam) > 0) {
                    $res2 = $this->db->insert_batch(TBL_TKINERJA_PARAMETER, $arrParam);
                }
                if(count($arrParamSub) > 0) {
                    $res3 = $this->db->insert_batch(TBL_TKINERJA_PARAMETERSUB, $arrParamSub);
                }
                if($res && $res2 && $res3) {
                    $this->db->trans_commit();
                    redirect('kinerja/index');
                } else {
                    $this->db->trans_rollback();
                    redirect(current_url()."?error=1");
                }
            } catch(Exception $e) {
                $this->db->trans_rollback();
                echo 'Caught exception: ',  $e->getMessage(), "\n";
                return;
            }
        }
        else {
            $this->load->view('kinerja/form', $data);
        }
    }

    function edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEPPL && GetLoggedUser()[COL_ROLEID] != ROLEPPS) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();

        $rdata = $data['data'] = $this->db->where(COL_KD_KINERJA, $id)->get(TBL_TKINERJA)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }
        $data['title'] = "Kinerja Penyuluh";
        $data['edit'] = TRUE;
        $this->load->view('kinerja/form', $data);
    }

    function delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEPPL && GetLoggedUser()[COL_ROLEID] != ROLEPPS) {
            ShowJsonError("Not Authorized.");
            return;
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_TKINERJA, array(COL_KD_KINERJA => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }
}