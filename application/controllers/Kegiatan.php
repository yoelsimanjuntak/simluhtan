<?php
/**
 * Created by PhpStorm.
 * User: PTI
 * Date: 9/22/2019
 * Time: 5:42 PM
 */
class Kegiatan extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin()) {
            redirect('user/login');
        }
    }

    function index() {
        $data['title'] = "Kegiatan";
        $user = GetLoggedUser();
        $q = $this->db
            ->select(TBL_TKEGIATAN."."."*,COALESCE(Nm_PPL, Nm_PPS) as Nm_Penyuluh")
            ->join(TBL_MPPL,TBL_MPPL.'.'.COL_KD_PPL." = ".TBL_TKEGIATAN.".".COL_KD_PPL,"left")
            ->join(TBL_MPPS,TBL_MPPS.'.'.COL_KD_PPS." = ".TBL_TKEGIATAN.".".COL_KD_PPS,"left");

        if($user[COL_ROLEID] == ROLEPPL) {
            //$q->where(TBL_TKEGIATAN.".".COL_KD_PPL, $user[COL_COMPANYID]);
            $q->where(TBL_MPPS.".".COL_KD_PPL, $user[COL_COMPANYID]);
        }
        if($user[COL_ROLEID] == ROLEPPS) {
            $q->where(TBL_TKEGIATAN.".".COL_KD_PPS, $user[COL_COMPANYID]);
        }
        $data['res'] = $q->get(TBL_TKEGIATAN)->result_array();

        $this->load->view('kegiatan/index', $data);
    }

    function add() {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEPPL && GetLoggedUser()[COL_ROLEID] != ROLEPPS) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();
        $data['title'] = "Kegiatan";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_KD_PPL => $this->input->post(COL_KD_PPL),
                COL_KD_PPS => $this->input->post(COL_KD_PPS),
                COL_NM_KEGIATAN => $this->input->post(COL_NM_KEGIATAN),
                COL_NM_LOKASI => $this->input->post(COL_NM_LOKASI),
                COL_NM_TANGGAL => date('Y-m-d', strtotime($this->input->post(COL_NM_TANGGAL))),
                COL_NM_PERMASALAHAN => $this->input->post(COL_NM_PERMASALAHAN),
                COL_NM_SOLUSI => $this->input->post(COL_NM_SOLUSI),

                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );

            $config['upload_path'] = MY_UPLOADPATH;
            $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
            $config['max_size']	= 5120000;
            $config['max_width']  = 1600;
            $config['max_height']  = 1600;
            $config['overwrite'] = FALSE;

            $this->load->library('upload', $config);
            if (!empty($_FILES["userfile"]["name"])) {
                if (!$this->upload->do_upload()) {
                    $resp['error'] = $this->upload->display_errors();
                    $resp['success'] = 0;
                    echo json_encode($resp);
                    return;
                }

                $dataupload = $this->upload->data();
                if (!empty($dataupload) && $dataupload['file_name']) {
                    $rec[COL_NM_FILE] = $dataupload['file_name'];
                }
            }

            if($user[COL_ROLEID] == ROLEPPL) {
                $rec[COL_KD_PPL] = $user[COL_COMPANYID];
                $rec[COL_KD_PPS] = null;
            }
            else if($user[COL_ROLEID] == ROLEPPS) {
                $rec[COL_KD_PPL] = null;
                $rec[COL_KD_PPS] = $user[COL_COMPANYID];
            }
            $res = $this->db->insert(TBL_TKEGIATAN, $rec);

            if($res) {
                redirect('kegiatan/index');
            } else {
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('kegiatan/form', $data);
        }
    }

    function edit($id) {
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEPPL && GetLoggedUser()[COL_ROLEID] != ROLEPPS) {
            redirect('user/dashboard');
        }

        $user = GetLoggedUser();

        $rdata = $data['data'] = $this->db->where(COL_KD_KEGIATAN, $id)->get(TBL_TKEGIATAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }
        $data['title'] = "Kegiatan";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_KD_PPL => $this->input->post(COL_KD_PPL),
                COL_KD_PPS => $this->input->post(COL_KD_PPS),
                COL_NM_KEGIATAN => $this->input->post(COL_NM_KEGIATAN),
                COL_NM_LOKASI => $this->input->post(COL_NM_LOKASI),
                COL_NM_TANGGAL => date('Y-m-d', strtotime($this->input->post(COL_NM_TANGGAL))),
                COL_NM_PERMASALAHAN => $this->input->post(COL_NM_PERMASALAHAN),
                COL_NM_SOLUSI => $this->input->post(COL_NM_SOLUSI),

                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );

            $config['upload_path'] = MY_UPLOADPATH;
            $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
            $config['max_size']	= 5120000;
            $config['max_width']  = 1600;
            $config['max_height']  = 1600;
            $config['overwrite'] = FALSE;

            $this->load->library('upload', $config);
            if (!empty($_FILES["userfile"]["name"])) {
                if (!$this->upload->do_upload()) {
                    $resp['error'] = $this->upload->display_errors();
                    $resp['success'] = 0;
                    echo json_encode($resp);
                    return;
                }

                $dataupload = $this->upload->data();
                if (!empty($dataupload) && $dataupload['file_name']) {
                    $rec[COL_NM_FILE] = $dataupload['file_name'];
                }
            }

            if($user[COL_ROLEID] == ROLEPPL) {
                $rec[COL_KD_PPL] = $user[COL_COMPANYID];
                $rec[COL_KD_PPS] = null;
            }
            else if($user[COL_ROLEID] == ROLEPPS) {
                $rec[COL_KD_PPL] = null;
                $rec[COL_KD_PPS] = $user[COL_COMPANYID];
            }
            $res = $this->db->where(COL_KD_KEGIATAN, $id)->update(TBL_TKEGIATAN, $rec);
            if($res) {
                redirect('kegiatan/index');
            } else {
                $this->db->trans_rollback();
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('kegiatan/form', $data);
        }
    }

    function delete(){
        if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN && GetLoggedUser()[COL_ROLEID] != ROLEPPL && GetLoggedUser()[COL_ROLEID] != ROLEPPS) {
            ShowJsonError("Not Authorized.");
            return;
        }

        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_TKEGIATAN, array(COL_KD_KEGIATAN => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }
}