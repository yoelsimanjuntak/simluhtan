<?php
/**
 * Created by PhpStorm.
 * User: Toshiba
 * Date: 21/07/2019
 * Time: 05:26
 */
class Kelompok_tani extends MY_Controller {
    function __construct() {
        parent::__construct();
        if(!IsLogin()) {
            redirect('user/login');
        }
    }

    function bantuan_index($id=0) {
        $data['title'] = "Bantuan Kelompok Tani";
        $user = GetLoggedUser();

        $arrkel = array();
        if($user[COL_ROLEID] == ROLEPPL) {
            $rkel = $this->db->where(COL_KD_PPL, $user[COL_COMPANYID])->get(TBL_MPPL_KELURAHAN)->result_array();

            foreach($rkel as $k) {
                $arrkel[] = $k[COL_KD_KELURAHAN];
            }
        }
        if($user[COL_ROLEID] == ROLEPPS) {
            $rpps = $this->db->where(COL_KD_PPS, $user[COL_COMPANYID])->get(TBL_MPPS)->row_array();
            if(!empty($rpps)) {
                $arrkel[] = $rpps[COL_KD_KELURAHAN];
            }
        }

        $q = $this->db
            ->join(TBL_MKELTAN,TBL_MKELTAN.'.'.COL_KD_KELOMPOKTANI." = ".TBL_TKELTAN_BANTUAN.".".COL_KD_KELOMPOKTANI,"left")
            ->join(TBL_MKEGIATAN,TBL_MKEGIATAN.'.'.COL_KD_KEGIATAN." = ".TBL_TKELTAN_BANTUAN.".".COL_KD_KEGIATAN,"left");
        if(count($arrkel) > 0) {
            $q->where_in(TBL_MKELTAN.".".COL_KD_KELURAHAN, $arrkel);
        }
        if($id>0) {
            $q->where(TBL_MKELTAN.".".COL_KD_KELOMPOKTANI, $id);
        }
        $data['res'] = $q->get(TBL_TKELTAN_BANTUAN)->result_array();
        $this->load->view('kelompok_tani/bantuan_index', $data);
    }

    function bantuan_add() {
        $user = GetLoggedUser();
        $data['title'] = "Bantuan Kelompok Tani";
        $data['edit'] = FALSE;

        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_KD_KELOMPOKTANI => $this->input->post(COL_KD_KELOMPOKTANI),
                COL_KD_KEGIATAN => $this->input->post(COL_KD_KEGIATAN),
                COL_VOLUME => toNum($this->input->post(COL_VOLUME)),
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),

                COL_CREATEDBY => $user[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
            );
            $res = $this->db->insert(TBL_TKELTAN_BANTUAN, $rec);

            if($res) {
                redirect('kelompok-tani/bantuan-index');
            } else {
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('kelompok_tani/bantuan_form', $data);
        }
    }

    function bantuan_edit($id) {
        $user = GetLoggedUser();

        $rdata = $data['data'] = $this->db
            ->join(TBL_MKELTAN,TBL_MKELTAN.'.'.COL_KD_KELOMPOKTANI." = ".TBL_TKELTAN_BANTUAN.".".COL_KD_KELOMPOKTANI,"left")
            ->where(COL_KD_BANTUAN, $id)
            ->get(TBL_TKELTAN_BANTUAN)->row_array();
        if(empty($rdata)){
            show_404();
            return;
        }
        $data['title'] = "Bantuan Kelompok Tani";
        $data['edit'] = TRUE;
        if(!empty($_POST)){
            $data['data'] = $_POST;
            $rec = array(
                COL_KD_KELOMPOKTANI => $this->input->post(COL_KD_KELOMPOKTANI),
                COL_KD_KEGIATAN => $this->input->post(COL_KD_KEGIATAN),
                COL_VOLUME => toNum($this->input->post(COL_VOLUME)),
                COL_KD_TAHUN => $this->input->post(COL_KD_TAHUN),
                COL_UPDATEDBY => $user[COL_USERNAME],
                COL_UPDATEDON => date('Y-m-d H:i:s')
            );
            $res = $this->db->where(COL_KD_BANTUAN, $id)->update(TBL_TKELTAN_BANTUAN, $rec);
            if($res) {
                redirect('kelompok-tani/bantuan-index');
            } else {
                $this->db->trans_rollback();
                redirect(current_url()."?error=1");
            }
        }
        else {
            $this->load->view('kelompok_tani/bantuan_form', $data);
        }
    }

    function bantuan_delete(){
        $data = $this->input->post('cekbox');
        $deleted = 0;
        foreach ($data as $datum) {
            $this->db->delete(TBL_TKELTAN_BANTUAN, array(COL_KD_BANTUAN => $datum));
            $deleted++;
        }
        if($deleted){
            ShowJsonSuccess($deleted." data dihapus");
        }else{
            ShowJsonError("Tidak ada dihapus");
        }
    }
}