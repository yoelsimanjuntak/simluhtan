<?php
/**
 * Created by PhpStorm.
 * User: Yoel Simanjuntak
 * Date: 15/10/2018
 * Time: 20:20
 */
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><i class="fa fa-close"></i></span>
    </button>
    <h4 class="modal-title">
        <div class="user-block">
            <span class="username" style="margin-left: 0px !important;"><a href="javascript:void(0)" style="text-transform: uppercase"><?=$data[COL_POSTTITLE]?></a></span>
            <span class="description" style="margin-left: 0px !important; font-style: italic">Dibuat oleh <?=$data[COL_NAME]?> pada <?=date('d M Y', strtotime($data[COL_POSTDATE]))?></span>
        </div>
    </h4>
</div>
<div class="modal-body">
    <img class="img-responsive pad" style="margin: auto" src="<?=!empty($data[COL_FILENAME])?MY_UPLOADURL.$data[COL_FILENAME]:MY_NOIMAGEURL?>" alt="<?=$data[COL_POSTTITLE]?>">
</div>
<div class="modal-footer box-comments">
    <div class="box-comment">
        <div class="comment-text" style="text-align: justify; margin-left: 0px !important;">
            <?=$data[COL_POSTCONTENT]?>
        </div>
    </div>
</div>
<div class="clearfix"></div>