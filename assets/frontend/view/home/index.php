<?php $this->load->view('frontend/header') ?>

    <div class="jumbotron text-center" style="padding: 20px; margin-bottom: 1rem;">
        <img src="<?=base_url()?>assets/media/image/logo.png" style="height: 8vh" />
        <h2 style="margin-top: 10px">Badan Kesatuan Bangsa Politik dan Perlindungan Masyarakat Kota Tebing Tinggi</h2>
    </div>

    <div class="ibox">
        <!--<div class="ibox-title">
            <h5 class="text-uppercase">Website Resmi Badan Kesatuan Bangsa Politik dan Perlindungan Masyarakat</h5>
        </div>-->
        <div class="ibox-content no-padding">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="<?=MY_IMAGEURL?>slider/1.jpg" style="height: 400px">
                        <div class="carousel-caption d-none d-md-block">
                            <p style="background-color: rgba(0, 0, 0, 0.72);">Website Resmi Badan Kesatuan Bangsa Politik dan Perlindungan Masyarakat<br />Kota Tebing Tinggi</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?=MY_IMAGEURL?>slider/2.jpg" style="height: 400px">
                        <div class="carousel-caption d-none d-md-block">
                            <p style="background-color: rgba(0, 0, 0, 0.72);">Website Resmi Badan Kesatuan Bangsa Politik dan Perlindungan Masyarakat<br />Kota Tebing Tinggi</p>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

    <div class="col-md-12 m-b-lg text-center">
        <div class="navy-line"></div>
        <h3>Berita Terkini</h3>
    </div>

    <div class="row">
        <div class="col-md-12">

                <?php
                if(count($news) > 0) {
                    ?>
                    <div class="slick-news">
                        <?php
                        foreach($news as $n) {
                            ?>
                            <div>
                                <div class="ibox-content">
                                    <?php
                                    $strippedcontent = strip_tags($n[COL_POSTCONTENT]);
                                    ?>
                                    <h2 style="font-size: 20px; border-bottom: 1px solid #dedede; padding-bottom: 10px"><?=strlen($n[COL_POSTTITLE]) > 60 ? substr($n[COL_POSTTITLE], 0, 60) . "..." : $n[COL_POSTTITLE]?></h2>
                                    <p style="text-align: justify">
                                        <?=strlen($strippedcontent) > 200 ? substr($strippedcontent, 0, 200) . "..." : $strippedcontent ?>
                                    </p>
                                    <a href="<?=site_url('post/view/'.$n[COL_POSTSLUG])?>" class="btn btn-outline btn-primary btn-flat btn-block">Lihat selengkapnya <i class="fa fa-arrow-right"></i> </a>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                else {
                    ?>
                    <div class="ibox-content">
                        <h2>Oops..</h2>
                        <p class="font-italic">
                            Tidak ada data untuk ditampilkan.
                        </p>
                    </div>
                <?php
                }
                ?>
        </div>
    </div>

    <div class="col-md-12 m-b-lg text-center">
        <div class="navy-line"></div>
        <h3>Galeri</h3>

        <div class="ibox">
            <div class="ibox-content">
                <div class="lightBoxGallery">
                    <?php
                    if(count($gallery) > 0) {
                        foreach($gallery as $n) {
                            $files = $this->db->where(COL_POSTID, $n[COL_POSTID])->get(TBL_POSTIMAGES)->result_array();
                            foreach($files as $f) {
                                ?>
                                <a href="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" title="<?=$n[COL_POSTTITLE]?>" data-gallery=""><img src="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" height="100px"></a>
                                <?php
                            }
                            ?>
                        <?php
                        }
                    }
                    else {
                        ?>
                        <p class="font-italic">
                            Tidak ada data untuk ditampilkan.
                        </p>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('frontend/footer') ?>