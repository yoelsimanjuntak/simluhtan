</div>
<div class="col-md-4 animated fadeInRight">
    <div class="ibox">
        <div class="ibox-title">
            <h5>Link Terkait</h5>
        </div>
        <div class="ibox-content">
            <table class="table table-stripped" style="margin-bottom: 0px;">
                <tbody>
                <tr>
                    <td class="no-borders">
                        <i class="fa fa-globe text-navy"></i>
                    </td>
                    <td class="no-borders">
                        <a href="">Website Pemerintah Kota Tebing Tinggi</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <i class="fa fa-facebook text-navy"></i>
                    </td>
                    <td>
                        <a href="">Facebook Resmi BKBPPM</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <i class="fa fa-instagram text-navy"></i>
                    </td>
                    <td>
                        <a href="">Instagram Resmi BKBPPM</a>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
    <div class="ibox">
        <div class="ibox-content no-padding">
            <script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>
            <div id="gpr-kominfo-widget-container"></div>
        </div>
    </div>

</div>
<div id="blueimp-gallery" class="blueimp-gallery">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
</div>
</div>
</div>
<div class="footer">
    <div>
        <strong>Copyright &copy; <?=date('Y')?></strong> Badan Kesatuan Bangsa Politik dan Perlindungan Masyarakat Kota Tebing Tinggi. <strong>Powered by</strong> <a href="https://www.linkedin.com/in/yoelrolas/">Partopi Tao</a>
        <div class="float-right">
            <?php
            if(!IsLogin()) {
                ?>
                <a href="<?=site_url('user/login')?>" class="btn btn-primary btn-outline btn-xs"><i class="fa fa-sign-in"></i> Login</a>
                <?php
            } else {
                $ruser_ = GetLoggedUser();
                ?>
                <a href="<?=site_url('user/dashboard')?>" class="btn btn-primary btn-xs"><i class="fa fa-dashboard"></i> Dashboard</a>
                <a href="<?=site_url('user/logout')?>" class="btn btn-danger btn-xs"><i class="fa fa-sign-out"></i> Logout (<?=$ruser_[COL_NAME]?>)</a>
                <?php
            }
            ?>

        </div>
    </div>
</div>
</div>
</div>

<script src="<?=base_url()?>assets/themes/inspinia/js/jquery-3.1.1.min.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/popper.min.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/bootstrap.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Flot -->
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/flot/jquery.flot.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/flot/jquery.flot.pie.js"></script>

<!-- Peity -->
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/peity/jquery.peity.min.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/demo/peity-demo.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?=base_url()?>assets/themes/inspinia/js/inspinia.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/pace/pace.min.js"></script>
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/wow/wow.min.js"></script>

<!-- jQuery UI -->
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- GITTER -->
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/gritter/jquery.gritter.min.js"></script>

<!-- Sparkline -->
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Sparkline demo data  -->
<script src="<?=base_url()?>assets/themes/inspinia/js/demo/sparkline-demo.js"></script>

<!-- ChartJS-->
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/chartJs/Chart.min.js"></script>

<!-- Toastr -->
<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/toastr/toastr.min.js"></script>

<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/slick/slick.min.js"></script>

<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/masonary/masonry.pkgd.min.js"></script>

<script src="<?=base_url()?>assets/themes/inspinia/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
<script>
    $(document).ready(function() {

        var inslider = $("#inSlider");
        if (inslider.length == 0) {
            $(".navbar-default").addClass('navbar-scroll')
        }
        else {
            $('body').scrollspy({
                target: '#navbar',
                offset: 80
            });
        }

        $('.slick-news').slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            centerMode: true
        });


        // Page scrolling feature
        $('a.page-scroll').bind('click', function(event) {
            var link = $(this);
            $('html, body').stop().animate({
                scrollTop: $(link.attr('href')).offset().top - 50
            }, 500);
            event.preventDefault();
            $("#navbar").collapse('hide');
        });
        $('a[href="<?=current_url()?>"]:not(.no-active)').addClass('active');
        $('a[href="<?=current_url()?>"]:not(.no-active)').parents('li').each(function() {
            $(this).addClass('active');
        });
        $('a[href="<?=current_url()?>"]:not(.no-active').parents('a').each(function() {
            $(this).addClass('in');
        });
    });

    var cbpAnimatedHeader = (function() {
        var docElem = document.documentElement,
            header = document.querySelector( '.navbar-default' ),
            didScroll = false,
            changeHeaderOn = 200;
        function init() {
            window.addEventListener( 'scroll', function( event ) {
                if( !didScroll ) {
                    didScroll = true;
                    setTimeout( scrollPage, 250 );
                }
            }, false );
        }
        function scrollPage() {
            var sy = scrollY();
            if ( sy >= changeHeaderOn ) {
                $(header).addClass('navbar-scroll')
            }
            else {
                $(header).removeClass('navbar-scroll')
            }
            didScroll = false;
        }
        function scrollY() {
            return window.pageYOffset || docElem.scrollTop;
        }
        var inslider = $("#inSlider");
        if (inslider.length > 0) {
            init();
        }


    })();

    // Activate WOW.js plugin for animation on scrol
    new WOW().init();
</script>
</body>
</html>