/*
SQLyog Community v12.09 (64 bit)
MySQL - 10.1.19-MariaDB : Database - db_ppl
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `media` */

DROP TABLE IF EXISTS `media`;

CREATE TABLE `media` (
  `Kd_Media` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_File` varchar(200) NOT NULL,
  `CreatedBy` varchar(50) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Media`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;

/*Table structure for table `mindikator` */

DROP TABLE IF EXISTS `mindikator`;

CREATE TABLE `mindikator` (
  `Kd_Indikator` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_Indikator` varchar(200) NOT NULL,
  PRIMARY KEY (`Kd_Indikator`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `mindikator_parameter` */

DROP TABLE IF EXISTS `mindikator_parameter`;

CREATE TABLE `mindikator_parameter` (
  `Kd_Parameter` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Indikator` bigint(10) NOT NULL,
  `Nm_Parameter` varchar(200) NOT NULL,
  `Is_FileUpload` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Kd_Parameter`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Table structure for table `mindikator_parameteropt` */

DROP TABLE IF EXISTS `mindikator_parameteropt`;

CREATE TABLE `mindikator_parameteropt` (
  `Kd_Opt` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Parameter` bigint(10) NOT NULL,
  `Nm_Opt` varchar(200) NOT NULL,
  `Poin` double NOT NULL,
  PRIMARY KEY (`Kd_Opt`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

/*Table structure for table `mindikator_parametersub` */

DROP TABLE IF EXISTS `mindikator_parametersub`;

CREATE TABLE `mindikator_parametersub` (
  `Kd_ParameterSub` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Parameter` bigint(10) NOT NULL,
  `Nm_ParameterSub` varchar(200) NOT NULL,
  `Is_FileUpload` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Kd_ParameterSub`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Table structure for table `tkinerja` */

DROP TABLE IF EXISTS `tkinerja`;

CREATE TABLE `tkinerja` (
  `Kd_Kinerja` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_PPL` bigint(10) DEFAULT NULL,
  `Kd_PPS` bigint(10) DEFAULT NULL,
  `Nm_Tanggal` date NOT NULL,
  `Nm_Keterangan` varchar(200) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Kinerja`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

/*Table structure for table `tkinerja_parameter` */

DROP TABLE IF EXISTS `tkinerja_parameter`;

CREATE TABLE `tkinerja_parameter` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Kinerja` bigint(10) NOT NULL,
  `Kd_Parameter` bigint(10) NOT NULL,
  `Kd_Opt` bigint(10) NOT NULL,
  `Nm_File` varchar(200) DEFAULT NULL,
  `Skor` double NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_KinerjaParam` (`Kd_Kinerja`),
  CONSTRAINT `FK_KinerjaParam` FOREIGN KEY (`Kd_Kinerja`) REFERENCES `tkinerja` (`Kd_Kinerja`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=latin1;

/*Table structure for table `tkinerja_parametersub` */

DROP TABLE IF EXISTS `tkinerja_parametersub`;

CREATE TABLE `tkinerja_parametersub` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Kinerja` bigint(10) NOT NULL,
  `Kd_ParameterSub` bigint(10) NOT NULL,
  `Nm_File` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_KinerjaParamSub` (`Kd_Kinerja`),
  CONSTRAINT `FK_KinerjaParamSub` FOREIGN KEY (`Kd_Kinerja`) REFERENCES `tkinerja` (`Kd_Kinerja`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
