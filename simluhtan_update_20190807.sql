ALTER TABLE `mppl` ADD `Nm_NoTelepon` VARCHAR(50) NULL AFTER `Kd_Kecamatan`;
ALTER TABLE `mpps` ADD `Nm_NoTelepon` VARCHAR(50) NULL AFTER `Kd_Kelurahan`;
ALTER TABLE `mkeltan_anggota` ADD `Nm_NoTelepon` VARCHAR(50) NULL AFTER `Nm_StatusAnggota`;
ALTER TABLE `mkeltan` ADD `Nm_NoAktaNotaris` VARCHAR(50) NULL AFTER `Nm_Status`;
ALTER TABLE `mkeltan` ADD `Nm_KepalaDesa` VARCHAR(50) NULL AFTER `Nm_NoAktaNotaris`;
ALTER TABLE `mkeltan` ADD `Latitude` VARCHAR(50) NULL AFTER `Nm_KepalaDesa`, ADD `Longitude` VARCHAR(50) NULL AFTER `Latitude`;
ALTER TABLE `mkomoditas` ADD `Nm_Varietas` VARCHAR(50) NULL AFTER `Nm_Komoditas`;
ALTER TABLE `mkegiatan` ADD `Nm_Satuan` VARCHAR(50) NULL AFTER `Nm_Kegiatan`;
ALTER TABLE `mkeltan` ADD `Nm_JenisKelompok` VARCHAR(50) NULL AFTER `Nm_Alamat`;

DROP TABLE IF EXISTS `mkeltan_pupuk`;

CREATE TABLE `mkeltan_pupuk` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Anggota` bigint(10) NOT NULL,
  `Kd_Komoditas` bigint(10) NOT NULL,
  `Kd_Pupuk` bigint(10) NOT NULL,
  `Jlh_Pupuk` double NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `Kd_Anggota` (`Kd_Anggota`),
  KEY `Kd_Anggota_2` (`Kd_Anggota`),
  KEY `FKKeltanPupukToPupuk` (`Kd_Pupuk`),
  KEY `FKKeltanPupukToKomoditas` (`Kd_Komoditas`),
  CONSTRAINT `FKKeltanPupukToKeltanAnggota` FOREIGN KEY (`Kd_Anggota`) REFERENCES `mkeltan_anggota` (`Kd_Anggota`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKKeltanPupukToKomoditas` FOREIGN KEY (`Kd_Komoditas`) REFERENCES `mkomoditas` (`Kd_Komoditas`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FKKeltanPupukToPupuk` FOREIGN KEY (`Kd_Pupuk`) REFERENCES `mpupuk` (`Kd_Pupuk`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;