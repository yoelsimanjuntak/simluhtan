/*
SQLyog Community v12.09 (64 bit)
MySQL - 10.1.19-MariaDB : Database - db_ppl
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `mgender` */

DROP TABLE IF EXISTS `mgender`;

CREATE TABLE `mgender` (
  `Kd_JenisKelamin` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_JenisKelamin` varchar(50) NOT NULL,
  PRIMARY KEY (`Kd_JenisKelamin`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mgender` */

insert  into `mgender`(`Kd_JenisKelamin`,`Nm_JenisKelamin`) values (1,'Laki-laki'),(2,'Perempuan');

/*Table structure for table `mkategori` */

DROP TABLE IF EXISTS `mkategori`;

CREATE TABLE `mkategori` (
  `Kd_Kategori` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_Kategori` varchar(50) NOT NULL,
  PRIMARY KEY (`Kd_Kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `mkategori` */

insert  into `mkategori`(`Kd_Kategori`,`Nm_Kategori`) values (1,'Pangan'),(2,'Holtikultura'),(3,'Perkebunan'),(4,'Peternakan');

/*Table structure for table `mkecamatan` */

DROP TABLE IF EXISTS `mkecamatan`;

CREATE TABLE `mkecamatan` (
  `Kd_Kecamatan` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_Kecamatan` varchar(50) NOT NULL,
  `Nm_Pejabat` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Kecamatan`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `mkecamatan` */

insert  into `mkecamatan`(`Kd_Kecamatan`,`Nm_Kecamatan`,`Nm_Pejabat`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (2,'Baktiraja',NULL,'admin','2019-07-20 17:56:32','admin','2019-07-22 02:31:57'),(3,'Doloksanggul',NULL,'admin','2019-07-20 17:56:38',NULL,NULL),(4,'Lintongnihuta',NULL,'admin','2019-07-20 17:56:56',NULL,NULL),(5,'Pakkat',NULL,'admin','2019-07-20 17:57:03',NULL,NULL),(6,'Onan Ganjang',NULL,'admin','2019-07-20 17:57:09',NULL,NULL),(7,'Paranginan',NULL,'admin','2019-07-20 17:57:17',NULL,NULL),(8,'Parlilitan',NULL,'admin','2019-07-20 17:57:30',NULL,NULL),(9,'Pollung',NULL,'admin','2019-07-20 17:57:36',NULL,NULL),(10,'Sijamapolang',NULL,'admin','2019-07-20 17:57:44',NULL,NULL),(11,'Tarabintang',NULL,'admin','2019-07-20 17:57:50',NULL,NULL);

/*Table structure for table `mkegiatan` */

DROP TABLE IF EXISTS `mkegiatan`;

CREATE TABLE `mkegiatan` (
  `Kd_Kegiatan` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_Kegiatan` varchar(50) NOT NULL,
  `Nm_Satuan` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Kegiatan`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `mkegiatan` */

insert  into `mkegiatan`(`Kd_Kegiatan`,`Nm_Kegiatan`,`Nm_Satuan`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (1,'Irigasi - RJIT',NULL,'admin','2019-07-20 18:39:54','admin','2019-07-20 18:40:06'),(2,'Irigasi - RJIT APBN-P',NULL,'admin','2019-07-20 18:40:20',NULL,NULL),(4,'Irigasi - JITUT','HA','admin','2019-07-20 18:40:40','admin','2019-08-09 03:43:15'),(5,'Irigasi - JIDES','UNIT','admin','2019-07-22 03:12:19','admin','2019-08-09 03:43:49'),(6,'Irigasi - PJI',NULL,'admin','2019-07-22 03:12:48',NULL,NULL),(7,'Irigasi - PJI BARU',NULL,'admin','2019-07-22 03:13:20',NULL,NULL),(8,'Irigasi - PSA',NULL,'admin','2019-07-22 03:13:50',NULL,NULL),(9,'Irigasi - Embung',NULL,'admin','2019-07-22 03:14:17',NULL,NULL),(10,'Lahan - OPLA',NULL,'admin','2019-07-22 03:14:33',NULL,NULL),(11,'Lahan - Cetak Sawah','HA','admin','2019-07-22 03:15:12','admin','2019-08-09 03:44:26'),(12,'Lahan - SRI',NULL,'admin','2019-07-22 03:15:26',NULL,NULL),(13,'ALSINTAN - TR2',NULL,'admin','2019-07-22 03:15:47',NULL,NULL),(14,'ALSINTAN - TR4 47,8 HP','UNIT','admin','2019-07-22 03:16:19','admin','2019-08-09 03:43:28'),(15,'ALSINTAN - TR 90','UNIT','admin','2019-07-22 03:16:43','admin','2019-08-09 03:44:06'),(16,'ALSINTAN - RICE TRANSPLANTER',NULL,'admin','2019-07-22 03:17:12',NULL,NULL),(17,'ALSINTAN - CHOPPER','UNIT','admin','2019-07-22 03:17:30','admin','2019-08-14 04:56:30'),(18,'ALSINTAN - CULTIVATOR',NULL,'admin','2019-07-22 03:17:54',NULL,NULL),(19,'ALSINTAN - POMPA AIR',NULL,'admin','2019-07-22 03:18:15',NULL,NULL);

/*Table structure for table `mkeltan` */

DROP TABLE IF EXISTS `mkeltan`;

CREATE TABLE `mkeltan` (
  `Kd_KelompokTani` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Kelurahan` bigint(10) NOT NULL,
  `Nm_KelompokTani` varchar(50) NOT NULL,
  `Nm_Ketua` varchar(50) NOT NULL,
  `Nm_Ketua_NIK` varchar(50) NOT NULL,
  `Nm_Alamat` varchar(200) DEFAULT NULL,
  `Nm_JenisKelompok` varchar(50) DEFAULT NULL,
  `Kd_TahunPembentukan` bigint(10) NOT NULL,
  `Nm_Status` varchar(10) NOT NULL,
  `Nm_NoSK` varchar(50) DEFAULT NULL,
  `Nm_NoAktaNotaris` varchar(50) DEFAULT NULL,
  `Nm_KepalaDesa` varchar(50) DEFAULT NULL,
  `Latitude` varchar(50) DEFAULT NULL,
  `Longitude` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_KelompokTani`),
  KEY `FKKelompokTaniToKelurahan` (`Kd_Kelurahan`),
  CONSTRAINT `FKKelompokTaniToKelurahan` FOREIGN KEY (`Kd_Kelurahan`) REFERENCES `mkelurahan` (`Kd_Kelurahan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `mkeltan` */

insert  into `mkeltan`(`Kd_KelompokTani`,`Kd_Kelurahan`,`Nm_KelompokTani`,`Nm_Ketua`,`Nm_Ketua_NIK`,`Nm_Alamat`,`Nm_JenisKelompok`,`Kd_TahunPembentukan`,`Nm_Status`,`Nm_NoSK`,`Nm_NoAktaNotaris`,`Nm_KepalaDesa`,`Latitude`,`Longitude`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (1,14,'Partopi Tao','Pastap Marulak-ulak','11112087','Simanampang',NULL,2018,'Aktif',NULL,NULL,NULL,NULL,NULL,'admin','2019-07-21 09:21:34','admin','2019-07-21 09:26:59'),(3,14,'Porlakta','Jorbut','123456','Porsea',NULL,2018,'Aktif',NULL,NULL,NULL,NULL,NULL,'admin','2019-07-21 09:29:29',NULL,NULL),(4,11,'Marsada','Daniel Simarmata','123455668','jl. Merdeka',NULL,2019,'Aktif',NULL,NULL,NULL,NULL,NULL,'admin','2019-07-22 02:43:08','admin','2019-07-22 02:43:25'),(5,11,'Mawar','Budi','1245678','Jln. Melanthon',NULL,2016,'Aktif',NULL,NULL,NULL,NULL,NULL,'admin','2019-07-22 04:58:30',NULL,NULL),(6,3,'Mawar','Mery','789456','Jl. Bonan Dolok No. 15',NULL,2018,'Aktif',NULL,NULL,NULL,NULL,NULL,'admin','2019-07-25 02:44:49',NULL,NULL),(7,126,'Saroha','Lastiar','789456','Pakkat',NULL,2018,'Aktif',NULL,NULL,NULL,NULL,NULL,'admin','2019-07-26 07:36:44','admin','2019-07-26 07:37:26'),(8,4,'Martabe','Riris Manik','1212075711930001','komp','Pemula',2018,'Lanjut',NULL,'','-','2.25759670353578','98.72155558237307','admin','2019-08-07 11:59:53','admin','2019-09-12 16:02:53'),(9,84,'Makmur JAYA','Binter Bintar','1212075711930002','Jl Raya X Y Z','Madya',2010,'-',NULL,'No/2018/QQQ','RY','2.3018270706268322','98.92736191097606','admin','2019-08-08 07:41:12',NULL,NULL),(10,37,'Humbahas Hebat','jacob','12121458697456','Jl. Marihat No 17 Doloksanggul','Lanjut',2018,'Akif','-','125Poktan/VIII/2019','Nandi','2.253136965130955','98.76189600595706','admin','2019-08-09 03:53:06','admin','2019-09-13 15:38:08'),(11,21,'Satahi','Jeskia Manurung','121205652232','Sosor Tambok Dolok Sanggul','Pemula',2019,'Aktif',NULL,'-','J Silaban','2.2467958123949847','98.76989062190023','admin','2019-08-14 05:23:06',NULL,NULL),(12,64,'Habeahan Bersatu','Katua I','123456','Medan','Pemula',2017,'Status','123456','123456','Kepala I','2.251318759121365','98.75743281015627','penyuluh.sw','2019-09-19 19:06:54',NULL,NULL);

/*Table structure for table `mkeltan_anggota` */

DROP TABLE IF EXISTS `mkeltan_anggota`;

CREATE TABLE `mkeltan_anggota` (
  `Kd_Anggota` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_KelompokTani` bigint(10) NOT NULL,
  `Nm_Anggota` varchar(50) NOT NULL,
  `Nm_Anggota_NIK` varchar(50) NOT NULL,
  `Nm_TanggalLahir` date DEFAULT NULL,
  `Kd_JenisKelamin` bigint(10) DEFAULT NULL,
  `Nm_Alamat` varchar(200) DEFAULT NULL,
  `Nm_StatusAnggota` varchar(10) DEFAULT NULL,
  `Nm_NoTelepon` varchar(50) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Anggota`),
  KEY `FKKeltanAnggotaToKeltan` (`Kd_KelompokTani`),
  CONSTRAINT `FKKeltanAnggotaToKeltan` FOREIGN KEY (`Kd_KelompokTani`) REFERENCES `mkeltan` (`Kd_KelompokTani`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `mkeltan_anggota` */

insert  into `mkeltan_anggota`(`Kd_Anggota`,`Kd_KelompokTani`,`Nm_Anggota`,`Nm_Anggota_NIK`,`Nm_TanggalLahir`,`Kd_JenisKelamin`,`Nm_Alamat`,`Nm_StatusAnggota`,`Nm_NoTelepon`,`Email`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (3,3,'Maureen Simanjuntak','123456','2004-12-25',NULL,'Medan','Sekretaris','','diskominfo@humbanghasundutankab.go.id','admin','2019-07-21 10:16:43','admin','2019-09-12 15:59:00'),(4,4,'qwertty','0987655432','2009-05-18',NULL,'asdfghj','Sebagai AN',NULL,NULL,'admin','2019-07-22 02:47:53','admin','2019-07-22 02:48:57'),(5,4,'Marni','124569865','2019-07-31',NULL,'Jl. Sisingamangaraja','Aktif',NULL,NULL,'admin','2019-07-22 04:59:29',NULL,NULL),(6,5,'Abet','122346','2000-03-14',NULL,'Bonan Dolok','AKtif',NULL,NULL,'admin','2019-07-25 02:47:18','admin','2019-07-25 02:47:38'),(7,7,'Polman','954312685','1999-03-24',NULL,'Pakkat','Anggota',NULL,NULL,'admin','2019-07-26 07:43:14','admin','2019-07-26 07:45:23'),(8,9,'Joice M','12109128019991290','2000-07-04',NULL,'Jl Medan Merdeka','Anggota','089232321132',NULL,'admin','2019-08-08 07:43:23',NULL,NULL),(9,10,'Budiman','1478523697894','1995-05-15',NULL,'Jl. Marihat','Anggota','123467953',NULL,'admin','2019-08-09 03:56:15',NULL,NULL),(10,10,'Humiar','7894561231478852','1991-12-24',NULL,'Jl. Marihat','Anggota','789456123',NULL,'admin','2019-08-09 03:58:47',NULL,NULL),(11,11,'Elly Siboro','121201522120021','1994-06-16',NULL,'Sosor Tambok','Anggota','0258963256630',NULL,'admin','2019-08-14 05:30:55',NULL,NULL),(12,10,'Yoel','11112087','1995-08-12',NULL,'Medan','Anggota','-','yoelrolas@gmail.com','admin','2019-09-13 18:13:40',NULL,NULL),(13,12,'Togar','123456','1995-08-12',NULL,'Medan','Anggota','123456','dev.simluhtan@gmail.com','admin','2019-09-19 20:00:41',NULL,NULL),(14,12,'Tongam','654321','1995-08-12',NULL,'Medan','Anggota','654321','dev.simluhtan@gmail.com','penyuluh.sw','2019-09-19 20:02:28',NULL,NULL);

/*Table structure for table `mkeltan_komoditas` */

DROP TABLE IF EXISTS `mkeltan_komoditas`;

CREATE TABLE `mkeltan_komoditas` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Anggota` bigint(10) NOT NULL,
  `Kd_Komoditas` bigint(10) NOT NULL,
  `Volume` double NOT NULL,
  `Musim_Tanam` double DEFAULT NULL,
  `Nm_SumberBenih` varchar(50) DEFAULT NULL,
  `Kd_Pupuk` bigint(10) DEFAULT NULL,
  `Jlh_Pupuk` double DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FKKomoditasToAnggota` (`Kd_Anggota`),
  KEY `FKKomoditasToKomoditas` (`Kd_Komoditas`),
  CONSTRAINT `FKKomoditasToAnggota` FOREIGN KEY (`Kd_Anggota`) REFERENCES `mkeltan_anggota` (`Kd_Anggota`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKKomoditasToKomoditas` FOREIGN KEY (`Kd_Komoditas`) REFERENCES `mkomoditas` (`Kd_Komoditas`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

/*Data for the table `mkeltan_komoditas` */

insert  into `mkeltan_komoditas`(`Uniq`,`Kd_Anggota`,`Kd_Komoditas`,`Volume`,`Musim_Tanam`,`Nm_SumberBenih`,`Kd_Pupuk`,`Jlh_Pupuk`) values (6,4,3,15,3,'Pemerintah',3,50),(9,6,12,100,4,'Pemerintah',1,20),(10,6,3,100,2,'Pribadi',2,50),(13,7,2,100,1,'Swadaya',3,50),(14,7,10,30,2,'Pemerintah',2,50),(15,7,12,20,2,'Pemerintah',3,0),(16,8,2,200,1,'Bantuan',NULL,0),(17,8,3,5,3,'Swadaya',NULL,0),(18,9,2,30,2,'Swadaya',NULL,0),(19,9,14,50,3,'Bantuan',NULL,0),(20,10,12,15,1,'Swadaya',NULL,0),(21,10,14,30,2,'Swadaya',NULL,0),(22,11,12,5,1,'Bantuan',NULL,0),(23,11,17,1,1,'Bantuan',NULL,0),(28,3,3,200,1,'Bantuan',NULL,0),(29,3,6,300,2,'Bantuan',NULL,0),(30,12,8,200,1,'Bantuan',NULL,0),(31,12,10,100,2,'Bantuan',NULL,0);

/*Table structure for table `mkeltan_pestisida` */

DROP TABLE IF EXISTS `mkeltan_pestisida`;

CREATE TABLE `mkeltan_pestisida` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Anggota` bigint(10) NOT NULL,
  `Kd_Komoditas` bigint(10) NOT NULL,
  `Kd_Pestisida` bigint(10) NOT NULL,
  `Jlh_Pestisida` double NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FKKeltanPestisidaToKeltanAnggota` (`Kd_Anggota`),
  KEY `FKKeltanPestisidaToKomoditas` (`Kd_Komoditas`),
  KEY `FKKeltanPestisidaToPestisida` (`Kd_Pestisida`),
  CONSTRAINT `FKKeltanPestisidaToKeltanAnggota` FOREIGN KEY (`Kd_Anggota`) REFERENCES `mkeltan_anggota` (`Kd_Anggota`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKKeltanPestisidaToKomoditas` FOREIGN KEY (`Kd_Komoditas`) REFERENCES `mkomoditas` (`Kd_Komoditas`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FKKeltanPestisidaToPestisida` FOREIGN KEY (`Kd_Pestisida`) REFERENCES `mpestisida` (`Kd_Pestisida`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mkeltan_pestisida` */

insert  into `mkeltan_pestisida`(`Uniq`,`Kd_Anggota`,`Kd_Komoditas`,`Kd_Pestisida`,`Jlh_Pestisida`) values (1,12,8,2,500),(2,12,10,2,10);

/*Table structure for table `mkeltan_pupuk` */

DROP TABLE IF EXISTS `mkeltan_pupuk`;

CREATE TABLE `mkeltan_pupuk` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Anggota` bigint(10) NOT NULL,
  `Kd_Komoditas` bigint(10) NOT NULL,
  `Kd_Pupuk` bigint(10) NOT NULL,
  `Jlh_Pupuk` double NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `Kd_Anggota` (`Kd_Anggota`),
  KEY `Kd_Anggota_2` (`Kd_Anggota`),
  KEY `FKKeltanPupukToPupuk` (`Kd_Pupuk`),
  KEY `FKKeltanPupukToKomoditas` (`Kd_Komoditas`),
  CONSTRAINT `FKKeltanPupukToKeltanAnggota` FOREIGN KEY (`Kd_Anggota`) REFERENCES `mkeltan_anggota` (`Kd_Anggota`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FKKeltanPupukToKomoditas` FOREIGN KEY (`Kd_Komoditas`) REFERENCES `mkomoditas` (`Kd_Komoditas`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FKKeltanPupukToPupuk` FOREIGN KEY (`Kd_Pupuk`) REFERENCES `mpupuk` (`Kd_Pupuk`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `mkeltan_pupuk` */

insert  into `mkeltan_pupuk`(`Uniq`,`Kd_Anggota`,`Kd_Komoditas`,`Kd_Pupuk`,`Jlh_Pupuk`) values (1,8,2,5,100),(2,8,2,5,20),(3,8,2,2,50),(4,8,3,4,20),(5,9,2,3,50),(6,9,2,2,50),(7,9,2,6,15),(8,9,14,5,50),(9,9,14,3,50),(10,9,14,2,50),(11,10,12,3,30),(12,10,12,3,50),(13,10,12,2,50),(14,10,14,3,50),(15,10,14,5,50),(16,11,12,3,10),(17,11,12,4,5),(18,11,12,6,2),(19,11,17,2,5),(20,12,8,2,100),(21,12,8,5,200),(22,12,10,4,200);

/*Table structure for table `mkelurahan` */

DROP TABLE IF EXISTS `mkelurahan`;

CREATE TABLE `mkelurahan` (
  `Kd_Kelurahan` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_Kecamatan` bigint(10) NOT NULL,
  `Nm_Kelurahan` varchar(50) NOT NULL,
  `Nm_Pejabat` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Kelurahan`),
  KEY `FK_KelurahanToKecamatan` (`Kd_Kecamatan`),
  CONSTRAINT `FK_KelurahanToKecamatan` FOREIGN KEY (`Kd_Kecamatan`) REFERENCES `mkecamatan` (`Kd_Kecamatan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=latin1;

/*Data for the table `mkelurahan` */

insert  into `mkelurahan`(`Kd_Kelurahan`,`Kd_Kecamatan`,`Nm_Kelurahan`,`Nm_Pejabat`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (2,4,'Dolok Margu',NULL,'admin','2019-07-20 18:30:34',NULL,NULL),(3,4,'Bonan Dolok',NULL,'admin','2019-07-20 18:30:57',NULL,NULL),(4,2,'Marbun Tonga',NULL,'admin','2019-07-20 18:31:12',NULL,NULL),(5,2,'Marbun Dolok',NULL,'admin','2019-07-20 18:31:22',NULL,NULL),(6,2,'Simamora',NULL,'admin','2019-07-20 18:31:30',NULL,NULL),(8,2,'Sinambela',NULL,'admin','2019-07-20 18:31:58',NULL,NULL),(9,2,'Siunong-unong Julu',NULL,'admin','2019-07-20 18:32:14',NULL,NULL),(10,2,'Tipang',NULL,'admin','2019-07-20 18:32:26',NULL,NULL),(11,3,'Aek Lung',NULL,'admin','2019-07-20 18:32:38',NULL,NULL),(12,3,'Bonani Onan',NULL,'admin','2019-07-20 18:32:51',NULL,NULL),(13,3,'Huta Bagasan',NULL,'admin','2019-07-20 18:33:02',NULL,NULL),(14,3,'Huta Gurgur',NULL,'admin','2019-07-20 18:33:17',NULL,NULL),(15,3,'Purba Manalu',NULL,'admin','2019-07-22 03:12:48',NULL,NULL),(17,3,'Simarigung',NULL,'admin','2019-07-22 03:14:46',NULL,NULL),(18,3,'Sampean',NULL,'admin','2019-07-22 03:14:59',NULL,NULL),(19,3,'Silaga Laga',NULL,'admin','2019-07-22 03:15:13',NULL,NULL),(20,3,'Sosor Gonting',NULL,'admin','2019-07-22 03:15:23',NULL,NULL),(21,3,'Sosor Tambok',NULL,'admin','2019-07-22 03:15:34',NULL,NULL),(22,3,'Purba Dolok',NULL,'admin','2019-07-22 03:15:43',NULL,NULL),(23,3,'Sihite I',NULL,'admin','2019-07-22 03:15:54',NULL,NULL),(24,3,'Lumban Tobing',NULL,'admin','2019-07-22 03:16:05',NULL,NULL),(25,3,'Matiti II',NULL,'admin','2019-07-22 03:16:18',NULL,NULL),(26,3,'Saitnihuta',NULL,'admin','2019-07-22 03:16:35',NULL,NULL),(27,3,'Hutaraja',NULL,'admin','2019-07-22 03:16:46',NULL,NULL),(28,3,'Lumban Purba',NULL,'admin','2019-07-22 03:16:57',NULL,NULL),(29,3,'Parik Sinomba',NULL,'admin','2019-07-22 03:17:09',NULL,NULL),(30,3,'Sihite II',NULL,'admin','2019-07-22 03:17:19',NULL,NULL),(31,3,'Janji',NULL,'admin','2019-07-22 03:17:29',NULL,NULL),(32,3,'Pakkat',NULL,'admin','2019-07-22 03:17:39',NULL,NULL),(33,3,'Sirisi Risi',NULL,'admin','2019-07-22 03:17:59',NULL,NULL),(34,3,'Matiti I',NULL,'admin','2019-07-22 03:18:11',NULL,NULL),(35,3,'Simangaronsang',NULL,'admin','2019-07-22 03:18:20',NULL,NULL),(36,3,'Sosor Tolong Sihite III',NULL,'admin','2019-07-22 03:18:31',NULL,NULL),(37,3,'Pasar Doloksanggul',NULL,'admin','2019-07-22 03:18:45',NULL,NULL),(38,9,'Parsingguran I',NULL,'admin','2019-07-22 03:19:26',NULL,NULL),(39,9,'Hutapaung',NULL,'admin','2019-07-22 03:19:41',NULL,NULL),(40,9,'Pollung',NULL,'admin','2019-07-22 03:19:54',NULL,NULL),(41,9,'Hutajulu',NULL,'admin','2019-07-22 03:20:06',NULL,NULL),(42,9,'Ria Ria',NULL,'admin','2019-07-22 03:20:20',NULL,NULL),(43,9,'Parsingguran II',NULL,'admin','2019-07-22 03:20:32',NULL,NULL),(44,9,'Pasurbatu',NULL,'admin','2019-07-22 03:20:45',NULL,NULL),(45,9,'Aek Nauli I',NULL,'admin','2019-07-22 03:21:00',NULL,NULL),(46,9,'Aek Nauli II',NULL,'admin','2019-07-22 03:21:11',NULL,NULL),(47,9,'Pandumaan',NULL,'admin','2019-07-22 03:21:22',NULL,NULL),(48,9,'Sipituhuta',NULL,'admin','2019-07-22 03:21:33',NULL,NULL),(49,9,'Pardomuan',NULL,'admin','2019-07-22 03:21:44',NULL,NULL),(50,9,'Hutapaung Utara',NULL,'admin','2019-07-22 03:21:56',NULL,NULL),(51,2,'Marbun Toruan',NULL,'admin','2019-07-22 03:23:48',NULL,NULL),(52,2,'Simangulampe',NULL,'admin','2019-07-22 03:23:59',NULL,NULL),(53,4,'Sibuntuan Partur',NULL,'admin','2019-07-22 03:24:32',NULL,NULL),(54,4,'Siharjulu',NULL,'admin','2019-07-22 03:24:42',NULL,NULL),(55,4,'Sitolu Bahal',NULL,'admin','2019-07-22 03:24:54',NULL,NULL),(56,4,'Tapian Nauli',NULL,'admin','2019-07-22 03:25:06',NULL,NULL),(57,4,'Pargaulan',NULL,'admin','2019-07-22 03:25:22',NULL,NULL),(58,4,'Lobutua',NULL,'admin','2019-07-22 03:25:36',NULL,NULL),(59,4,'Hutasoit',NULL,'admin','2019-07-22 03:25:47',NULL,NULL),(60,4,'Nagasaribu I',NULL,'admin','2019-07-22 03:26:01',NULL,NULL),(61,4,'Nagasaribu II',NULL,'admin','2019-07-22 03:26:11',NULL,NULL),(62,4,'Sibuntaon Parera',NULL,'admin','2019-07-22 03:26:24',NULL,NULL),(63,4,'Siponjot',NULL,'admin','2019-07-22 03:26:36',NULL,NULL),(64,4,'Habeahan',NULL,'admin','2019-07-22 03:26:48',NULL,NULL),(65,4,'Sigompul',NULL,'admin','2019-07-22 03:26:57',NULL,NULL),(68,4,'Sigumpar',NULL,'admin','2019-07-22 03:27:26',NULL,NULL),(69,4,'Parulohan',NULL,'admin','2019-07-22 03:27:35','admin','2019-07-23 03:10:54'),(70,4,'Sitio II',NULL,'admin','2019-07-22 03:27:47',NULL,NULL),(71,4,'Hutasoit II',NULL,'admin','2019-07-22 03:27:57',NULL,NULL),(72,4,'Nagasaribu III',NULL,'admin','2019-07-22 03:28:11',NULL,NULL),(73,4,'Nagasaribu IV',NULL,'admin','2019-07-22 03:28:25',NULL,NULL),(74,4,'Nagasaribu V',NULL,'admin','2019-07-22 03:28:36',NULL,NULL),(75,7,'Lumban Sialaman',NULL,'admin','2019-07-22 03:29:14',NULL,NULL),(76,7,'Lumban Barat',NULL,'admin','2019-07-22 04:03:21',NULL,NULL),(77,7,'Lobu Tolong',NULL,'admin','2019-07-22 04:03:47',NULL,NULL),(78,7,'Sihonongan',NULL,'admin','2019-07-22 04:04:05',NULL,NULL),(79,7,'Paranginan Utara',NULL,'admin','2019-07-22 04:04:16',NULL,NULL),(80,7,'Pearung',NULL,'admin','2019-07-22 04:04:28',NULL,NULL),(81,7,'Pearung Silali',NULL,'admin','2019-07-22 04:04:40',NULL,NULL),(82,7,'Lumban Sianturi',NULL,'admin','2019-07-22 04:04:54',NULL,NULL),(83,7,'Labutolung Habinsaran',NULL,'admin','2019-07-22 04:05:07',NULL,NULL),(84,7,'Paranginan Selatan',NULL,'admin','2019-07-22 04:05:18',NULL,NULL),(85,7,'Siborutorop',NULL,'admin','2019-07-22 04:05:27',NULL,NULL),(86,10,'Batunagajar',NULL,'admin','2019-07-22 04:07:16',NULL,NULL),(87,10,'Sanggaran I',NULL,'admin','2019-07-22 04:07:27',NULL,NULL),(88,10,'Sitapongan',NULL,'admin','2019-07-22 04:07:37',NULL,NULL),(89,10,'Sigulok',NULL,'admin','2019-07-22 04:07:46',NULL,NULL),(90,10,'Sutaginjang',NULL,'admin','2019-07-22 04:07:56',NULL,NULL),(91,10,'Siborboron',NULL,'admin','2019-07-22 04:08:07',NULL,NULL),(92,10,'Bonan Dolok I',NULL,'admin','2019-07-22 04:08:21',NULL,NULL),(93,10,'Bonan Dolok II',NULL,'admin','2019-07-22 04:08:33',NULL,NULL),(94,10,'Sibuntuon ',NULL,'admin','2019-07-22 04:08:43',NULL,NULL),(95,10,'Nagurguran',NULL,'admin','2019-07-22 04:08:54',NULL,NULL),(96,6,'Batu Nagodang Siatas',NULL,'admin','2019-07-22 04:09:47',NULL,NULL),(97,6,'Oanan Ganjang',NULL,'admin','2019-07-22 04:09:54',NULL,NULL),(98,6,'Parbotihan',NULL,'admin','2019-07-22 04:10:06',NULL,NULL),(99,6,'Sihikit',NULL,'admin','2019-07-22 04:11:09',NULL,NULL),(100,6,'Parnapan',NULL,'admin','2019-07-22 04:11:22',NULL,NULL),(101,6,'Janji Nagodang',NULL,'admin','2019-07-22 04:11:33',NULL,NULL),(102,6,'Sampe Tua',NULL,'admin','2019-07-22 04:11:41',NULL,NULL),(103,6,'Hutajulu',NULL,'admin','2019-07-22 04:11:50',NULL,NULL),(104,6,'Sibuluan',NULL,'admin','2019-07-22 04:11:59',NULL,NULL),(105,6,'Sigalogo',NULL,'admin','2019-07-22 04:12:12',NULL,NULL),(106,6,'Aek Godang Arbaan',NULL,'admin','2019-07-22 04:12:19',NULL,NULL),(107,6,'Sanggaran II',NULL,'admin','2019-07-22 04:12:31',NULL,NULL),(108,5,'Purba Bersatu',NULL,'admin','2019-07-22 04:12:52',NULL,NULL),(109,5,'Purba Beringin',NULL,'admin','2019-07-22 04:13:58',NULL,NULL),(110,5,'Karya',NULL,'admin','2019-07-22 04:14:28',NULL,NULL),(111,5,'Manalu',NULL,'admin','2019-07-22 04:14:39',NULL,NULL),(112,5,'Sijarango',NULL,'admin','2019-07-22 04:14:49',NULL,NULL),(113,5,'Tukka Dolok',NULL,'admin','2019-07-22 04:15:01',NULL,NULL),(114,5,'Siambataon',NULL,'admin','2019-07-22 04:15:14',NULL,NULL),(115,5,'Parmonangan',NULL,'admin','2019-07-22 04:15:23',NULL,NULL),(116,5,'Sipagabu',NULL,'admin','2019-07-22 04:15:37',NULL,NULL),(117,5,'Banuarea',NULL,'admin','2019-07-22 04:15:49',NULL,NULL),(118,5,'Rura Tanjung',NULL,'admin','2019-07-22 04:16:03',NULL,NULL),(119,5,'Rura Aek Sopang',NULL,'admin','2019-07-22 04:16:23',NULL,NULL),(120,5,'Lumban Tonga Tonga',NULL,'admin','2019-07-22 04:16:35',NULL,NULL),(121,5,'Pula Godang',NULL,'admin','2019-07-22 04:16:46',NULL,NULL),(122,5,'Pakkat Hauagong',NULL,'admin','2019-07-22 04:16:54',NULL,NULL),(123,5,'Peadungdung',NULL,'admin','2019-07-22 04:17:15',NULL,NULL),(126,5,'Ambobi Paranginan',NULL,'admin','2019-07-22 04:18:10',NULL,NULL),(128,5,'Purba Sianjur',NULL,'admin','2019-07-22 04:18:54',NULL,NULL),(129,5,'Siambaton Pahae',NULL,'admin','2019-07-22 04:19:06',NULL,NULL),(130,5,'Panggugunan',NULL,'admin','2019-07-22 04:19:21',NULL,NULL),(131,5,'Hauagong',NULL,'admin','2019-07-22 04:19:39',NULL,NULL),(132,8,'Pusuk II Simaninggir ',NULL,'admin','2019-07-22 04:20:17',NULL,NULL),(133,8,'Sionom Hudon VII',NULL,'admin','2019-07-22 04:20:29',NULL,NULL),(135,8,'Baringin',NULL,'admin','2019-07-22 04:21:58',NULL,NULL),(136,8,'Sionom Hudon Julu',NULL,'admin','2019-07-22 04:22:39',NULL,NULL),(137,8,'Sionom Hudon tonga',NULL,'admin','2019-07-22 04:23:15',NULL,NULL),(140,8,'Sionom Hudon Selatan',NULL,'admin','2019-07-22 04:24:50',NULL,NULL),(142,8,'Sionom Hudon Timur',NULL,'admin','2019-07-22 04:25:25',NULL,NULL),(143,8,'Sihotang Hasugian Tonga',NULL,'admin','2019-07-22 04:25:35',NULL,NULL),(144,8,'Pusuk I',NULL,'admin','2019-07-22 04:27:26',NULL,NULL),(145,8,'Sionom Hudon Toruan',NULL,'admin','2019-07-22 04:27:40',NULL,NULL),(146,8,'Simataniari',NULL,'admin','2019-07-22 04:27:54',NULL,NULL),(147,8,'Sionom Hudon Timur II',NULL,'admin','2019-07-22 04:28:05',NULL,NULL),(148,8,'Sihotang Hasugian Dolok II',NULL,'admin','2019-07-22 04:28:25',NULL,NULL),(149,8,'Sihotang Hasugian Habinsaran',NULL,'admin','2019-07-22 04:28:35',NULL,NULL),(150,8,'Sionom Hudon Sibulbulon',NULL,'admin','2019-07-22 04:28:45',NULL,NULL),(151,8,'Sionom Hudon Runggu',NULL,'admin','2019-07-22 04:28:57',NULL,NULL),(152,8,'Janji Hutanapa',NULL,'admin','2019-07-22 04:29:11',NULL,NULL),(153,8,'Baringin Natam',NULL,'admin','2019-07-22 04:29:24',NULL,NULL),(154,8,'Sihotang Hasugian Dolok I ',NULL,'admin','2019-07-22 04:29:40',NULL,NULL),(155,11,'Sitanduk',NULL,'admin','2019-07-22 04:30:24',NULL,NULL),(156,11,'Tarabintang',NULL,'admin','2019-07-22 04:30:42',NULL,NULL),(157,11,'Siharsugian Toruan',NULL,'admin','2019-07-22 04:31:03',NULL,NULL),(158,11,'Sihombu',NULL,'admin','2019-07-22 04:31:15',NULL,NULL),(160,11,'Sibongkare',NULL,'admin','2019-07-22 04:32:37',NULL,NULL),(161,11,'Simbara',NULL,'admin','2019-07-22 04:32:47',NULL,NULL),(162,11,'Marpadan',NULL,'admin','2019-07-22 04:32:56',NULL,NULL),(163,11,'Mungkur',NULL,'admin','2019-07-22 04:33:05',NULL,NULL),(164,11,'Sibongkare Sianju',NULL,'admin','2019-07-22 04:33:18',NULL,NULL),(165,3,'Sosor Tolong Sihite III',NULL,'admin','2019-07-23 03:15:22',NULL,NULL),(166,5,'Sijarango I',NULL,'admin','2019-07-23 03:17:57',NULL,NULL),(167,3,'Dev','Mr. Lurah','admin','2019-09-19 18:00:38',NULL,NULL);

/*Table structure for table `mkomoditas` */

DROP TABLE IF EXISTS `mkomoditas`;

CREATE TABLE `mkomoditas` (
  `Kd_Komoditas` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_Komoditas` varchar(50) NOT NULL,
  `Nm_Varietas` varchar(50) DEFAULT NULL,
  `Kd_Kategori` bigint(10) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Komoditas`),
  KEY `FKKomoditasToKategori` (`Kd_Kategori`),
  CONSTRAINT `FKKomoditasToKategori` FOREIGN KEY (`Kd_Kategori`) REFERENCES `mkategori` (`Kd_Kategori`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `mkomoditas` */

insert  into `mkomoditas`(`Kd_Komoditas`,`Nm_Komoditas`,`Nm_Varietas`,`Kd_Kategori`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (2,'Padi','Ketan',1,'admin','2019-07-20 19:12:13','admin','2019-08-08 01:57:21'),(3,'Jagung','Jagung Kampung',1,'admin','2019-07-20 19:12:22','admin','2019-08-08 01:57:37'),(4,'Kedelai','Kedelai',1,'admin','2019-07-20 19:12:32','admin','2019-08-08 01:57:59'),(5,'Tebu','Tebu Merah',2,'admin','2019-07-20 19:12:43','admin','2019-08-08 01:58:05'),(6,'Kopi','Robusta',2,'admin','2019-07-20 19:12:50','admin','2019-08-08 01:58:25'),(7,'Karet','Karet',2,'admin','2019-07-20 19:12:59','admin','2019-08-08 01:58:31'),(8,'Kakao','Coklat',2,'admin','2019-07-20 19:13:06','admin','2019-08-08 01:55:53'),(9,'Kelapa Sawit','Kelapa Sawit',2,'admin','2019-07-20 19:13:17','admin','2019-08-08 01:55:46'),(10,'Aneka Umbi','Ubi Jalar',1,'admin','2019-07-22 03:51:06','admin','2019-08-08 01:55:11'),(11,'Aneka Kacang','Kacang Tanah',1,'admin','2019-07-22 03:51:31','admin','2019-08-08 01:56:06'),(12,'Aneka Cabai','Cabai Rawit',2,'admin','2019-07-22 03:52:00','admin','2019-08-08 01:56:14'),(13,'Bawang Merah','Bawang Merah',2,'admin','2019-07-22 03:52:25','admin','2019-08-08 01:56:26'),(14,'Bawang Putih','Bawang Putih',2,'admin','2019-07-22 03:52:46','admin','2019-08-08 01:56:32'),(15,'Tanaman Rambah dan Obat','Kunyit',2,'admin','2019-07-22 03:53:15','admin','2019-08-08 01:56:38'),(16,'Tanaman Hias','Palm',2,'admin','2019-07-22 03:53:38','admin','2019-08-08 01:56:45'),(17,'Aneka Sayuran','Sawi',2,'admin','2019-07-22 03:54:20','admin','2019-08-08 01:56:50'),(18,'Buah Lainnya','Pisang',2,'admin','2019-07-22 03:54:57','admin','2019-08-08 01:56:57');

/*Table structure for table `mpendidikan` */

DROP TABLE IF EXISTS `mpendidikan`;

CREATE TABLE `mpendidikan` (
  `Kd_Pendidikan` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_Pendidikan` varchar(50) NOT NULL,
  PRIMARY KEY (`Kd_Pendidikan`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `mpendidikan` */

insert  into `mpendidikan`(`Kd_Pendidikan`,`Nm_Pendidikan`) values (1,'SD'),(2,'SMP'),(3,'SLTA'),(4,'SMA'),(5,'SMK'),(6,'SPP'),(7,'SPMA'),(8,'D1'),(9,'D2'),(10,'D3'),(11,'D4'),(12,'S1'),(13,'S2'),(14,'S3');

/*Table structure for table `mpestisida` */

DROP TABLE IF EXISTS `mpestisida`;

CREATE TABLE `mpestisida` (
  `Kd_Pestisida` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_Pestisida` varchar(50) NOT NULL,
  `Nm_Satuan` varchar(50) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pestisida`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mpestisida` */

insert  into `mpestisida`(`Kd_Pestisida`,`Nm_Pestisida`,`Nm_Satuan`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (2,'Pestisida','Liter','admin','2019-09-12 15:46:58',NULL,NULL);

/*Table structure for table `mppl` */

DROP TABLE IF EXISTS `mppl`;

CREATE TABLE `mppl` (
  `Kd_PPL` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_StatusPenyuluh` varchar(10) NOT NULL,
  `Nm_PPL` varchar(50) NOT NULL,
  `Nm_PPL_NIK` varchar(50) DEFAULT NULL,
  `Nm_PPL_Alamat` varchar(200) DEFAULT NULL,
  `Nm_PPL_TempatLahir` varchar(200) DEFAULT NULL,
  `Nm_PPL_TanggalLahir` date DEFAULT NULL,
  `Kd_JenisKelamin` bigint(10) DEFAULT NULL,
  `Kd_StatusPernikahan` bigint(10) DEFAULT NULL,
  `Kd_Agama` bigint(10) DEFAULT NULL,
  `Kd_Pendidikan` bigint(10) DEFAULT NULL,
  `Nm_PPL_SKCPNS` varchar(50) DEFAULT NULL,
  `Nm_PPL_Jabatan` varchar(50) DEFAULT NULL,
  `Nm_PPL_SKTerakhir` varchar(50) DEFAULT NULL,
  `Nm_PPL_TahunKontrak` varchar(50) DEFAULT NULL,
  `Kd_Kecamatan` bigint(10) DEFAULT NULL,
  `Nm_NoTelepon` varchar(50) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_PPL`),
  UNIQUE KEY `UNIQ_NIK` (`Nm_PPL_NIK`),
  KEY `FK_PPLToJenisKelamin` (`Kd_JenisKelamin`),
  KEY `FK_PPLToStatusPernikahan` (`Kd_StatusPernikahan`),
  KEY `FK_PPLToAgama` (`Kd_Agama`),
  KEY `FK_PPLToPendidikan` (`Kd_Pendidikan`),
  KEY `FK_PPLToKecamatan` (`Kd_Kecamatan`),
  CONSTRAINT `FK_PPLToAgama` FOREIGN KEY (`Kd_Agama`) REFERENCES `mreligion` (`Kd_Agama`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PPLToJenisKelamin` FOREIGN KEY (`Kd_JenisKelamin`) REFERENCES `mgender` (`Kd_JenisKelamin`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PPLToKecamatan` FOREIGN KEY (`Kd_Kecamatan`) REFERENCES `mkecamatan` (`Kd_Kecamatan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PPLToPendidikan` FOREIGN KEY (`Kd_Pendidikan`) REFERENCES `mpendidikan` (`Kd_Pendidikan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PPLToStatusPernikahan` FOREIGN KEY (`Kd_StatusPernikahan`) REFERENCES `mstatuspernikahan` (`Kd_Status`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `mppl` */

insert  into `mppl`(`Kd_PPL`,`Kd_StatusPenyuluh`,`Nm_PPL`,`Nm_PPL_NIK`,`Nm_PPL_Alamat`,`Nm_PPL_TempatLahir`,`Nm_PPL_TanggalLahir`,`Kd_JenisKelamin`,`Kd_StatusPernikahan`,`Kd_Agama`,`Kd_Pendidikan`,`Nm_PPL_SKCPNS`,`Nm_PPL_Jabatan`,`Nm_PPL_SKTerakhir`,`Nm_PPL_TahunKontrak`,`Kd_Kecamatan`,`Nm_NoTelepon`,`Email`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (4,'PNS','Yoel Rolas Simanjuntak','11112087','Medan','Medan','1995-08-12',1,NULL,1,10,'121212','','','',3,'','','admin','2019-07-20 21:06:30','admin','2019-09-19 18:05:48'),(5,'PNS','Elly Siboro','121208689100068','JL. Karya DolokSanggul','Ajibata','1991-06-28',2,NULL,2,10,'1234567890',NULL,NULL,NULL,3,NULL,NULL,'admin','2019-07-22 02:37:47',NULL,NULL),(6,'THL','Rianti Zega','12145xxxxx','Pasaribu','New York','2018-09-23',2,NULL,1,4,'12',NULL,NULL,NULL,11,NULL,NULL,'admin','2019-07-22 04:53:12',NULL,NULL),(7,'PNS','Butet','123456789','Jl. Siliwangi','Doloksanggul','2004-06-08',2,NULL,1,12,'12346',NULL,NULL,NULL,4,NULL,NULL,'admin','2019-07-25 02:36:09','admin','2019-07-25 02:40:23'),(8,'PNS','lastiar','963852741','Doloksanggul','Doloksanggul','1993-03-09',2,NULL,1,12,'123456789',NULL,NULL,NULL,3,NULL,NULL,'admin','2019-07-26 07:22:50','admin','2019-07-26 07:27:51'),(9,'PNS','Sartika','654897132','Matiti','Matiti','1996-06-07',2,NULL,4,10,'56479123',NULL,NULL,NULL,5,NULL,NULL,'admin','2019-07-26 07:31:15',NULL,NULL),(10,'PNS','Sandro Purba','112109000129090','Komp Tano Tubu','Medan','1997-06-10',1,NULL,1,10,'Sk/121/d002',NULL,NULL,NULL,7,'082399889902',NULL,'admin','2019-08-08 07:36:16',NULL,NULL),(11,'PNS','Deddy Situmorang','121207571198','Dolok Sanggul','Medan','1990-06-05',1,NULL,1,13,'SK/asa/asas/2012','Kabid','11112087','',3,'085422336655','diskominfo@humbanghasundutankab.go.id','admin','2019-08-14 05:04:36','admin','2019-09-13 16:01:50');

/*Table structure for table `mppl_kelurahan` */

DROP TABLE IF EXISTS `mppl_kelurahan`;

CREATE TABLE `mppl_kelurahan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_PPL` bigint(10) NOT NULL,
  `Kd_Kelurahan` bigint(10) NOT NULL,
  PRIMARY KEY (`Uniq`),
  UNIQUE KEY `UNIQ_Kelurahan` (`Kd_Kelurahan`),
  KEY `FKPPLKelurahanToPPL` (`Kd_PPL`),
  CONSTRAINT `FKPPLKelurahanToKelurahan` FOREIGN KEY (`Kd_Kelurahan`) REFERENCES `mkelurahan` (`Kd_Kelurahan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FKPPLKelurahanToPPL` FOREIGN KEY (`Kd_PPL`) REFERENCES `mppl` (`Kd_PPL`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

/*Data for the table `mppl_kelurahan` */

insert  into `mppl_kelurahan`(`Uniq`,`Kd_PPL`,`Kd_Kelurahan`) values (21,5,11),(22,6,158),(24,7,64),(25,7,3),(26,7,60),(31,8,26),(32,9,126),(33,9,117),(34,9,131),(35,9,120),(36,9,110),(37,9,111),(38,10,85),(39,10,83),(40,10,84),(61,11,20),(62,11,21),(63,11,33),(64,4,13),(65,4,14),(66,4,167);

/*Table structure for table `mppl_pelatihan` */

DROP TABLE IF EXISTS `mppl_pelatihan`;

CREATE TABLE `mppl_pelatihan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_PPL` bigint(10) NOT NULL,
  `Nm_Pelatihan` varchar(200) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_PPLPelatihanToPPL` (`Kd_PPL`),
  CONSTRAINT `FK_PPLPelatihanToPPL` FOREIGN KEY (`Kd_PPL`) REFERENCES `mppl` (`Kd_PPL`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `mppl_pelatihan` */

insert  into `mppl_pelatihan`(`Uniq`,`Kd_PPL`,`Nm_Pelatihan`,`Kd_Tahun`) values (10,5,'Workshop Bercocok tanam yang baik',2017),(11,6,'uhmmm....',5),(12,8,'Workshop Cocok Tanam',2018),(13,9,'Workshop Cocok Tanam',2018),(14,10,'Pelatihan xyt',2019),(15,10,'pelatihan sdf',2018),(23,11,'Workshop Cocok Tanam di Medan',2018),(24,4,'Diklat PIM IV',2019),(25,4,'Diklat PIM III',2020);

/*Table structure for table `mpps` */

DROP TABLE IF EXISTS `mpps`;

CREATE TABLE `mpps` (
  `Kd_PPS` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_PPL` bigint(10) NOT NULL,
  `Kd_StatusPenyuluh` bigint(10) NOT NULL,
  `Nm_PPS` varchar(50) NOT NULL,
  `Nm_PPS_NIK` varchar(50) NOT NULL,
  `Nm_PPS_Alamat` varchar(200) DEFAULT NULL,
  `Nm_PPS_TempatLahir` varchar(200) DEFAULT NULL,
  `Nm_PPS_TanggalLahir` date DEFAULT NULL,
  `Kd_JenisKelamin` bigint(10) DEFAULT NULL,
  `Kd_StatusPernikahan` bigint(10) DEFAULT NULL,
  `Kd_Agama` bigint(10) DEFAULT NULL,
  `Kd_Pendidikan` bigint(10) DEFAULT NULL,
  `Nm_PPS_SKPenetapan` varchar(50) DEFAULT NULL,
  `Kd_Kelurahan` bigint(10) DEFAULT NULL,
  `Nm_NoTelepon` varchar(50) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_PPS`),
  UNIQUE KEY `UNIQ_Kelurahan` (`Kd_Kelurahan`),
  KEY `FK_PPSToAgama` (`Kd_Agama`),
  KEY `FK_PPSToJenisKelamin` (`Kd_JenisKelamin`),
  KEY `FK_PPSToPendidikan` (`Kd_Pendidikan`),
  KEY `FK_PPSToStatusPernikahan` (`Kd_StatusPernikahan`),
  KEY `FK_PPSToPPL` (`Kd_PPL`),
  CONSTRAINT `FK_PPSToAgama` FOREIGN KEY (`Kd_Agama`) REFERENCES `mreligion` (`Kd_Agama`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PPSToJenisKelamin` FOREIGN KEY (`Kd_JenisKelamin`) REFERENCES `mgender` (`Kd_JenisKelamin`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PPSToKelurahan` FOREIGN KEY (`Kd_Kelurahan`) REFERENCES `mkelurahan` (`Kd_Kelurahan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PPSToPPL` FOREIGN KEY (`Kd_PPL`) REFERENCES `mppl` (`Kd_PPL`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PPSToPendidikan` FOREIGN KEY (`Kd_Pendidikan`) REFERENCES `mpendidikan` (`Kd_Pendidikan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PPSToStatusPernikahan` FOREIGN KEY (`Kd_StatusPernikahan`) REFERENCES `mstatuspernikahan` (`Kd_Status`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Data for the table `mpps` */

insert  into `mpps`(`Kd_PPS`,`Kd_PPL`,`Kd_StatusPenyuluh`,`Nm_PPS`,`Nm_PPS_NIK`,`Nm_PPS_Alamat`,`Nm_PPS_TempatLahir`,`Nm_PPS_TanggalLahir`,`Kd_JenisKelamin`,`Kd_StatusPernikahan`,`Kd_Agama`,`Kd_Pendidikan`,`Nm_PPS_SKPenetapan`,`Kd_Kelurahan`,`Nm_NoTelepon`,`Email`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (3,4,0,'Riris Manik','1111094','Porsea','Porsea','1993-11-17',2,NULL,2,11,'SK',14,NULL,NULL,'admin','2019-07-21 07:38:04',NULL,NULL),(4,5,0,'Daniel Simarmata','1234567890','Jl. Merdeka','DOloksanggul','2002-03-07',1,NULL,1,9,'1234567890',11,NULL,NULL,'admin','2019-07-22 02:41:00',NULL,NULL),(5,7,0,'Ucok','123456','Lintongnihut','lINTONGNIHUTA','2010-02-10',1,NULL,3,10,'12345646',3,NULL,NULL,'admin','2019-07-25 02:42:20',NULL,NULL),(6,7,0,'Tiur','12345','Lintongnihuta','Lintongnihuta','2010-03-10',2,NULL,2,10,'123456',64,NULL,NULL,'admin','2019-07-25 02:43:24',NULL,NULL),(7,8,0,'Budi Setia','852147963','Saitnihuta','Saitnihuta','1997-04-10',1,NULL,1,11,'789451632',26,NULL,NULL,'admin','2019-07-26 07:29:33',NULL,NULL),(8,9,0,'Jelitya','569871356','Pakkat','Pakkat','1993-07-15',2,NULL,6,10,'96548425',126,NULL,NULL,'admin','2019-07-26 07:32:35',NULL,NULL),(9,10,0,'Riris Manik','1212073766210001','Porsea','Porsea','1996-10-23',2,NULL,2,11,'121/sww/2019',83,'082370734334','diskominfo@humbanghasundutankab.go.id','admin','2019-08-08 07:37:58','admin','2019-09-12 15:56:04'),(10,11,0,'Riris Sinaga','1212075455863301','Dolok Sanggul','Porsea','1993-11-03',2,NULL,2,11,'SK/PPS/2015',20,'085455561112',NULL,'admin','2019-08-14 05:12:33',NULL,NULL),(11,4,0,'Togar','11112087','Medan','Medan','1995-08-12',1,NULL,6,10,'123456',167,'123456','dev.simluhtan@gmail.com','penyuluh','2019-09-19 18:07:28',NULL,NULL);

/*Table structure for table `mpps_pelatihan` */

DROP TABLE IF EXISTS `mpps_pelatihan`;

CREATE TABLE `mpps_pelatihan` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_PPS` bigint(10) NOT NULL,
  `Nm_Pelatihan` varchar(50) NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_PPSPelatihanToPPS` (`Kd_PPS`),
  CONSTRAINT `FK_PPSPelatihanToPPS` FOREIGN KEY (`Kd_PPS`) REFERENCES `mpps` (`Kd_PPS`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `mpps_pelatihan` */

insert  into `mpps_pelatihan`(`Uniq`,`Kd_PPS`,`Nm_Pelatihan`,`Kd_Tahun`) values (7,3,'Pelatihan',2012),(8,3,'Pelatihan 2',2013),(9,4,'Workshop Bercocok tanam yang baik',2018),(10,5,'Workshop Cocok Tanam',2019),(11,6,'Workshop Cocok Tanam',2019),(12,7,'Workshop Cocok Tanam',2018),(13,8,'Workshop Cocok Tanam',2019),(15,10,'Workshop Cocok Tanam di Dolok Sanggul',2018),(16,9,'pelatihan sdf nom',2017),(17,11,'Diklat Bela Negara',2002);

/*Table structure for table `mpupuk` */

DROP TABLE IF EXISTS `mpupuk`;

CREATE TABLE `mpupuk` (
  `Kd_Pupuk` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_Pupuk` varchar(50) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Pupuk`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `mpupuk` */

insert  into `mpupuk`(`Kd_Pupuk`,`Nm_Pupuk`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (1,'Pupuk Organik','admin','2019-07-20 19:34:59','admin','2019-07-20 19:35:08'),(2,'Pupuk Urea','admin','2019-07-20 19:35:15',NULL,NULL),(3,'Pupuk NPK','admin','2019-07-20 19:35:34',NULL,NULL),(4,'Pupuk ZA','admin','2019-07-20 19:35:41',NULL,NULL),(5,'Pupuk SP-36','admin','2019-07-22 03:11:11',NULL,NULL),(6,'Pupuk NPK - Mutiara','admin','2019-07-26 07:44:02',NULL,NULL);

/*Table structure for table `mreligion` */

DROP TABLE IF EXISTS `mreligion`;

CREATE TABLE `mreligion` (
  `Kd_Agama` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_Agama` varchar(50) NOT NULL,
  PRIMARY KEY (`Kd_Agama`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `mreligion` */

insert  into `mreligion`(`Kd_Agama`,`Nm_Agama`) values (1,'Protestan'),(2,'Katolik'),(3,'Islam'),(4,'Hindu'),(5,'Buddha'),(6,'Konghucu');

/*Table structure for table `mstatuspernikahan` */

DROP TABLE IF EXISTS `mstatuspernikahan`;

CREATE TABLE `mstatuspernikahan` (
  `Kd_Status` bigint(10) NOT NULL AUTO_INCREMENT,
  `Nm_Status` varchar(50) NOT NULL,
  PRIMARY KEY (`Kd_Status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mstatuspernikahan` */

insert  into `mstatuspernikahan`(`Kd_Status`,`Nm_Status`) values (1,'Menikah'),(2,'Belum Menikah');

/*Table structure for table `postcategories` */

DROP TABLE IF EXISTS `postcategories`;

CREATE TABLE `postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PostCategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `postcategories` */

insert  into `postcategories`(`PostCategoryID`,`PostCategoryName`,`PostCategoryLabel`) values (1,'Berita','#f56954'),(2,'Artikel','#00a65a'),(3,'Kegiatan','#f39c12'),(4,'Galeri','#00c0ef'),(5,'Others','#3c8dbc');

/*Table structure for table `postimages` */

DROP TABLE IF EXISTS `postimages`;

CREATE TABLE `postimages` (
  `PostImageID` bigint(10) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(10) NOT NULL,
  `FileName` varchar(250) NOT NULL,
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `postimages` */

insert  into `postimages`(`PostImageID`,`PostID`,`FileName`,`Description`) values (1,5,'my_pic.jpg',NULL),(2,6,'Sipinsur.jpg',NULL),(3,7,'pindah_datang.jpg',NULL),(4,8,'utk-website.jpeg',NULL);

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(10) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext NOT NULL,
  `PostExpiredDate` date NOT NULL,
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `posts` */

insert  into `posts`(`PostID`,`PostCategoryID`,`PostDate`,`PostTitle`,`PostSlug`,`PostContent`,`PostExpiredDate`,`TotalView`,`LastViewDate`,`IsSuspend`,`FileName`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (4,5,'2019-06-26','Hubungi Kami','hubungi-kami','<p><strong>Dinas Komunikasi Dan Informatika Kabupaten Humbang Hasundutan</strong></p>\r\n\r\n<p>Jl. SM. Raja Kompleks Perkantoran Tano Tubu, Doloksanggul 22457</p>\r\n\r\n<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Telp</td>\r\n			<td>:</td>\r\n			<td>&nbsp;(0633) 31555</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Email&nbsp;</td>\r\n			<td>:</td>\r\n			<td colspan=\"4\">&nbsp;diskominfo@humbanghasundutankab.go.id</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<hr />\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><iframe frameborder=\"0\" height=\"450\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3986.6996793830294!2d98.76777421426368!3d2.265541658581495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x302e3cb7ff0bcc73%3A0x2c1a5b16f34531dc!2sDinas+Komunikasi+dan+Informatika+Kab.+Humbang+Hasundutan!5e0!3m2!1sen!2sid!4v1561433370666!5m2!1sen!2sid\" style=\"border:0\" width=\"600\"></iframe></p>\r\n','2020-12-31',9,NULL,0,NULL,'admin','2019-06-26 08:27:46','admin','2019-06-26 08:27:46'),(5,2,'2019-07-22','Humbahas EXPO 2019','humbahas-expo-2019','<p>contoh&nbsp;</p>\r\n\r\n<p><strong>Doloksanggul</strong>&nbsp;- Dalam rangka Perayaan Hari Ulang Tahun Kabupaten Humbang Hasundutan Ke-16, Panitia Hari Ulang Tahun Kabupaten Humbang Hasundutan Ke-16 bekerjasama dengan Tim Kreatif &ldquo;DOXA STREET&rdquo; melaksanakan Pameran Pembangunan yaitu HUMBAHAS EXPO 2019 di Jalan Sisingamangaraja XII (depan SMP Negeri 2 Doloksanggul dan depan Kantor Polsek Doloksanggul), Minggu (21/7).<br />\r\n&nbsp;</p>\r\n\r\n<p>Kegiatan ini adalah sebagai upaya memberikan informasi seluas-luasnya kepada masyarakat tentang berbagai kemajuan yang telah dicapai Kabupaten Humbang Hasundutan sekaligus mempromosikan berbagai Usaha Mikro Kecil dan Menengah (UMKM) di Kabupaten Humbang Hasundutan.</p>\r\n\r\n<p>Hadir dalam pameran ini Wakil Bupati Humbang Hasundutan Saut Palindungan Simamora, Sekretaris Daerah Kabupaten Humbang Hasundutan Drs. Tonny Sihombing, M.IP, Pabung 0210/TU Mayor Infantri Holden Gultom, Para Pimpinan OPD, Pimpinan BUMN dan BUMD dan Tim Penggerak PKK Kabupaten Humbang Hasundutan.</p>\r\n\r\n<p>Dalam sambutannya, Wakil Bupati mengatakan agar UMKM harus terus didukung pertumbuhannya oleh seluruh kalangan masyarakat, khususnya generasi muda untuk memiliki keyakinan berwirausaha dan membuat lapangan kerjanya sendiri.<br />\r\nUMKM juga merupakan salah satu pilar pendukung utama perekonomian nasional. UMKM merupakan kunci keberhasilan negara menghadapi krisis ekonomi global dan telah terbukti tangguh menghadapi persaingan bahkan dari berbagai perusahaan yang telah mapan.</p>\r\n\r\n<p>Selain HUMBAHAS EXPO 2019, pada Pameran Pembangunan ini juga diselenggarakan pagelaran Marching Band (parade) dari SD, SMP, SMA dan SMK di Kabupaten Humbang Hasundutan, lomba melukis di atas kaos, lomba mewarnai untuk TK dan SD kelas 1 dan Kelas 2, lomba makan lampet dan lomba makan mie gomak.&nbsp;<strong>(Diskominfo)</strong></p>\r\n\r\n<hr />','2019-07-31',3,NULL,0,NULL,'admin','2019-07-22 03:22:56','admin','2019-07-22 03:25:19'),(6,1,'2019-07-22','Workshop Penyuluhan Pertanian','workshop-penyuluhan-pertanian','<p>Workshop Penyuluhan Pertanian Kabupaten Humbang Hasundutan dilakukan di kecamatan Pollung berjalan dengan Lancar dihadiri oleh 30 kelompok Tani.</p>\r\n\r\n<p>&nbsp;</p>\r\n','2020-07-20',5,NULL,0,NULL,'admin','2019-07-22 03:24:15','admin','2019-07-22 03:24:45'),(7,1,'2019-07-26','Sosoalisasi Penggunaan Aplikasi Simluhtan di DInas Kominfo','sosoalisasi-penggunaan-aplikasi-simluhtan-di-dinas-kominfo','<p>Sosoalisasi Penggunaan Aplikasi Simluhtan di DInas Kominfo</p>\r\n','2019-07-31',0,NULL,0,NULL,'admin','2019-07-26 07:52:07','admin','2019-07-26 07:52:07'),(8,4,'2019-08-08','Test','test','<p>Test</p>\r\n','2019-08-20',2,NULL,0,NULL,'admin','2019-08-08 08:31:49','admin','2019-08-08 08:31:49');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `roles` */

insert  into `roles`(`RoleID`,`RoleName`) values (1,'Administrator'),(2,'Penyuluh Lapangan'),(3,'Penyuluh Swadaya'),(4,'Kelompok Tani');

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `settings` */

insert  into `settings`(`SettingID`,`SettingLabel`,`SettingName`,`SettingValue`) values (1,'SETTING_WEB_NAME','SETTING_WEB_NAME','SIMLUHTAN'),(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Sistem Informasi Manajemen Penyuluh Pertanian'),(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://simluhtan.disqus.com/embed.js'),(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','Dinas Pertanian Kabupaten Humbang Hasundutan'),(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','Jl. Sidikalang Km. 3.5 Simpang Sitapongan Desa Simangaronsang, Doloksanggul'),(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','(0633) 31101, (0633) 31104'),(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','(0633) 31103, (0633) 31103'),(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','distan@humbanghasundutankab.go.id'),(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','https://diskominfo.humbanghasundutankab.go.id/index.php/api/data_footer_link'),(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader-128x/Ellipsis-3s-200px.gif'),(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0');

/*Table structure for table `tkegiatan` */

DROP TABLE IF EXISTS `tkegiatan`;

CREATE TABLE `tkegiatan` (
  `Kd_Kegiatan` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_PPL` bigint(10) DEFAULT NULL,
  `Kd_PPS` bigint(10) DEFAULT NULL,
  `Nm_Kegiatan` varchar(200) NOT NULL,
  `Nm_Lokasi` varchar(200) NOT NULL,
  `Nm_Tanggal` date NOT NULL,
  `Nm_Permasalahan` varchar(200) NOT NULL,
  `Nm_Solusi` varchar(200) NOT NULL,
  `Nm_File` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Kegiatan`),
  KEY `FK_User` (`CreatedBy`),
  CONSTRAINT `FK_User` FOREIGN KEY (`CreatedBy`) REFERENCES `users` (`UserName`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tkegiatan` */

insert  into `tkegiatan`(`Kd_Kegiatan`,`Kd_PPL`,`Kd_PPS`,`Nm_Kegiatan`,`Nm_Lokasi`,`Nm_Tanggal`,`Nm_Permasalahan`,`Nm_Solusi`,`Nm_File`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (3,7,0,'TEST','Dev','2019-09-18','Dev','Dev',NULL,'admin','2019-09-22 13:49:49','admin','2019-09-22 13:55:16');

/*Table structure for table `tkeltan_bantuan` */

DROP TABLE IF EXISTS `tkeltan_bantuan`;

CREATE TABLE `tkeltan_bantuan` (
  `Kd_Bantuan` bigint(10) NOT NULL AUTO_INCREMENT,
  `Kd_KelompokTani` bigint(10) NOT NULL,
  `Kd_Kegiatan` bigint(10) NOT NULL,
  `Volume` double NOT NULL,
  `Kd_Tahun` bigint(10) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Kd_Bantuan`),
  KEY `FKBantuanToKelompokTani` (`Kd_KelompokTani`),
  KEY `FKBantuanToKegiatan` (`Kd_Kegiatan`),
  CONSTRAINT `FKBantuanToKegiatan` FOREIGN KEY (`Kd_Kegiatan`) REFERENCES `mkegiatan` (`Kd_Kegiatan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FKBantuanToKelompokTani` FOREIGN KEY (`Kd_KelompokTani`) REFERENCES `mkeltan` (`Kd_KelompokTani`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `tkeltan_bantuan` */

insert  into `tkeltan_bantuan`(`Kd_Bantuan`,`Kd_KelompokTani`,`Kd_Kegiatan`,`Volume`,`Kd_Tahun`,`CreatedBy`,`CreatedOn`,`UpdatedBy`,`UpdatedOn`) values (2,3,4,200000,2017,'admin','2019-07-21 10:45:49','admin','2019-07-21 10:46:31'),(3,4,1,100,2019,'admin','2019-07-22 02:49:52',NULL,NULL),(4,5,17,0,2019,'admin','2019-07-22 04:59:48',NULL,NULL),(5,5,15,100,2019,'admin','2019-07-25 02:48:07','admin','2019-07-25 02:49:35'),(6,7,17,100,2019,'admin','2019-07-26 07:49:00',NULL,NULL),(7,9,17,10,2018,'admin','2019-08-08 07:45:05',NULL,NULL),(8,10,17,2,2019,'admin','2019-08-09 03:59:11',NULL,NULL),(9,10,14,2,2019,'admin','2019-08-09 03:59:29',NULL,NULL),(10,10,13,10,2015,'admin','2019-08-09 03:59:58',NULL,NULL),(11,11,19,1,2018,'admin','2019-08-14 05:31:39',NULL,NULL),(12,12,15,20,2015,'penyuluh.sw','2019-09-19 20:07:53',NULL,NULL),(13,6,15,100,2018,'penyuluh','2019-09-19 20:08:55',NULL,NULL);

/*Table structure for table `userinformation` */

DROP TABLE IF EXISTS `userinformation`;

CREATE TABLE `userinformation` (
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `CompanyID` varchar(200) DEFAULT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IdentityNo` varchar(50) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `ReligionID` int(10) DEFAULT NULL,
  `Gender` tinyint(1) DEFAULT NULL,
  `Address` text,
  `PhoneNumber` varchar(50) DEFAULT NULL,
  `EducationID` int(10) DEFAULT NULL,
  `UniversityName` varchar(50) DEFAULT NULL,
  `FacultyName` varchar(50) DEFAULT NULL,
  `MajorName` varchar(50) DEFAULT NULL,
  `IsGraduated` tinyint(1) NOT NULL DEFAULT '0',
  `GraduatedDate` date DEFAULT NULL,
  `YearOfExperience` int(10) DEFAULT NULL,
  `RecentPosition` varchar(250) DEFAULT NULL,
  `RecentSalary` double DEFAULT NULL,
  `ExpectedSalary` double DEFAULT NULL,
  `CVFilename` varchar(250) DEFAULT NULL,
  `ImageFilename` varchar(250) DEFAULT NULL,
  `RegisteredDate` date DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `userinformation` */

insert  into `userinformation`(`UserName`,`Email`,`CompanyID`,`Name`,`IdentityNo`,`BirthDate`,`ReligionID`,`Gender`,`Address`,`PhoneNumber`,`EducationID`,`UniversityName`,`FacultyName`,`MajorName`,`IsGraduated`,`GraduatedDate`,`YearOfExperience`,`RecentPosition`,`RecentSalary`,`ExpectedSalary`,`CVFilename`,`ImageFilename`,`RegisteredDate`) values ('admin','admin@sidasma.humbanghasundutankab.go.id',NULL,'Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17'),('anggota.poktan','devvv.simluhtan@gmail.com',NULL,'Anggota Poktan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-09-19'),('penyuluh','dev.simluhtan@gmail.com','7','Penyuluh Jahat',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-09-19'),('penyuluh.sw','devv.simluhtan@gmail.com','6','Penyuluh Gadungan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-09-19');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`UserName`,`Password`,`RoleID`,`IsSuspend`,`LastLogin`,`LastLoginIP`) values ('admin','e10adc3949ba59abbe56e057f20f883e',1,0,'2019-09-23 04:10:39','::1'),('anggota.poktan','e10adc3949ba59abbe56e057f20f883e',4,0,NULL,NULL),('penyuluh','e10adc3949ba59abbe56e057f20f883e',2,0,'2019-09-23 04:22:34','::1'),('penyuluh.sw','e10adc3949ba59abbe56e057f20f883e',3,0,'2019-09-19 20:01:39','::1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
